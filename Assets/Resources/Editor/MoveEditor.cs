﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class MoveEditor : EditorWindow {
    private Vector2 scrollPos = Vector2.zero;
    List<string> pressed = new List<string>();
    int selGridInt;
    int curTab = 0;
    [MenuItem("P2D Functions/Move Editor")]
    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(MoveEditor));
    }
    public void OnEnable() {
        this.minSize = new Vector2(500, 300);
        selGridInt = 0;
        if (pressed.Count == 0) {
            MoveManager.loadDataFile();
            for (int i = 0; i < MoveManager.moveList.Count; i++) {
                pressed.Add(MoveManager.moveList[i].name);
            }
        }
    }
    void OnGUI() {
        int oldValue = selGridInt;
        //curTab = GUILayout.Toolbar(curTab, new string[] { "Object", "Bake", "Layers" });
        scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Width(250), GUILayout.Height(this.position.height));
        GUILayout.Space(5);
        GUILayout.BeginVertical();
        selGridInt = GUILayout.SelectionGrid(selGridInt, pressed.ToArray(), 1);
        if (selGridInt != oldValue) {
            changeMove();
        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();
        Repaint();
    }
    void changeMove() {
        Debug.Log(selGridInt);
    }
}
