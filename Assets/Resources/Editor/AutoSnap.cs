using UnityEngine;
 using UnityEditor;
 public class AutoSnap : EditorWindow
 {
     const float SNAPVALUE = 0.32f;
     private Vector3 prevPosition;
	 private bool doSnap = true;
     private float snapValueX = SNAPVALUE;
	 private float snapValueY = SNAPVALUE;
	 private float snapValueZ = 1.0f;
     private float yOffSet = -0.14f;

	 [MenuItem( "Edit/Auto Snap %_l" )]
	 static void Init() {
		var window = (AutoSnap)EditorWindow.GetWindow( typeof( AutoSnap ) );
		window.maxSize = new Vector2( 200, 50 );
	 }

	 void OnEnable() {
		 EditorApplication.update += Update;
	 }

	 public void OnGUI() {
		doSnap = EditorGUILayout.Toggle( "Auto Snap", doSnap );
	 }

	 public void Update() {
		if ( doSnap
				&& !EditorApplication.isPlaying
				&& Selection.transforms.Length > 0
				&& Selection.transforms[0].position != prevPosition
				&& (Selection.activeGameObject.name.Equals("Player")
                    || Selection.activeGameObject.tag.Equals("Event"))) {
				    Snap();
				    prevPosition = Selection.transforms[0].position;
		}
	 }

	 private void Snap() {
		foreach ( var transform in Selection.transforms )
		{
			var t = transform.position;
			t.x = RoundToNearestMultiple( t.x, snapValueX ) ;
			t.y = RoundToNearestMultiple( t.y, snapValueY ) + yOffSet;
			t.z = RoundToNearestMultiple( t.z, snapValueZ );
			transform.position = t;
		}
	 }

	 private float RoundToNearestMultiple( float input, float snapValue, bool isY = false) {
        if (isY) {
            input = input - yOffSet;
        }
        float ret = ((input % snapValue) > snapValue / 2) ? input + snapValue - input % snapValue : input - input % snapValue;
        return ret;
     }
 }
