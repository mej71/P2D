﻿using UnityEngine;
using UnityEditor;
using System.Collections;


//Use to mass slice all pokemon icon sprites into two 64x64 sections for animating
//Issue with overwriting already sliced sprites.  Chance all sprite modes to Single before running this script
public class SliceIconSprites {

	[MenuItem("P2D Functions/Slice All Pokémon Icons")]
    public static void startSlicing() {  
        StaticCoroutine.DoCoroutine(SliceIconSprites.sliceIcons()); //Call as a coroutine to not freeze up engine for large processes
    }

    static IEnumerator sliceIcons() {
        Texture2D[] icons = Resources.LoadAll<Texture2D>("Graphics/Icons/Pokemon");
        //Texture2D texture;
        string path;
        string oldpath = "";
        int count = 0;
        UnityEditor.EditorUtility.DisplayProgressBar("Slicing Icon Sprites", "Slicing", 0);
        foreach (Texture2D icon in icons) {
            path = AssetDatabase.GetAssetPath(icon);
            path.Substring(0,path.Length - 2);
            if (oldpath.Equals(path)) { //skip duplicates
                oldpath = path;
                ++count;
                continue;
            }
            oldpath = path;
            TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;
            ti.isReadable = true;
            ti.filterMode = FilterMode.Point;
            ti.spriteImportMode = SpriteImportMode.Multiple;
            System.Collections.Generic.List<SpriteMetaData> newData = new System.Collections.Generic.List<SpriteMetaData>();

            int SliceWidth = 64;
            int SliceHeight = 64;
            for (int i = 0; i < icon.width; i += SliceWidth) {
                for (int j = icon.height; j > 0; j -= SliceHeight) {
                    SpriteMetaData smd = new SpriteMetaData();
                    smd.pivot = new Vector2(0f, 1f);
                    smd.alignment = 0;
                    smd.name = (icon.height - j) / SliceHeight + ", " + i / SliceWidth;
                    smd.rect = new Rect(i, j - SliceHeight, SliceWidth, SliceHeight);
                    newData.Add(smd);
                }
            }
            ti.spritesheet = newData.ToArray();
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            ++count;
            if (count%5 == 0) {
                UnityEditor.EditorUtility.DisplayProgressBar("Slicing Icon Sprites", "Slicing", (float)count/icons.Length);
            }
        }
        UnityEditor.EditorUtility.ClearProgressBar();
        yield break;
    }
}
