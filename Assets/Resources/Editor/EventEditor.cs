﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class EventEditor : EditorWindow {

	int eventIndex;
    Vector2 eventListPos = Vector2.zero;
    List<EventWrapper> events = new List<EventWrapper>();
    List<string> eventNames = new List<string>();
    int pageIndex = 0;
    Vector2 pageTabPos = Vector2.zero;
    string[] pageList = new string[]{"1"};
    
    [MenuItem("P2D Functions/Event Editor")]
    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(EventEditor));
    }
    public void OnEnable() {
        this.minSize = new Vector2(700, 500);
        eventIndex = 0;
        eventNames.Clear();
        events.Clear();
        events = ((EventWrapper[])Resources.FindObjectsOfTypeAll(typeof(EventWrapper))).ToList();
        foreach(EventWrapper ev in events) {
            if (ev.gameObject.transform.parent!=null) {
                eventNames.Add(ev.name + " (" + ev.gameObject.transform.parent.name + ")");
            } else {
                eventNames.Add(ev.name + "(Top Level)");
            }
        }
    }
    void OnGUI() {
        //create scroll view for event list
        int oldValue = eventIndex;
        eventListPos = GUILayout.BeginScrollView(eventListPos, GUILayout.Width(250), GUILayout.Height(this.position.height));
        GUILayout.Space(5);
        GUILayout.BeginVertical();
        eventIndex = GUILayout.SelectionGrid(eventIndex, eventNames.ToArray(), 1);
        if (eventIndex != oldValue) {
            changeEvent();
        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        //create horizontal view for page tabs
        int oldPage = pageIndex;
        GUILayout.BeginArea(new Rect(251, 0, 450, 50));
        GUILayout.BeginHorizontal();
        pageIndex = GUILayout.Toolbar(pageIndex, pageList);
        if (pageIndex != oldPage) {
            changePage();
        }
        GUILayout.EndArea();
        GUILayout.EndHorizontal();

        Repaint();
    }
    void changeEvent() {
        Debug.Log(eventIndex);
        pageList = new string[]{"1","2"};
    }

    void changePage() {

    }
}