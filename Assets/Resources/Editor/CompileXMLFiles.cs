using UnityEngine;
using System.IO;
using System.Xml.Linq;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Xml;

public class CompileXMLFiles : UnityEditor.EditorWindow {

    static List<string> abilityNameList = new List<string>();
    static List<string> typeNameList = new List<string>();
    static List<string> moveNameList = new List<string>();
    static List<string> itemNameList = new List<string>();
    static List<string> pokemonNameList = new List<string>();
    static List<string> mapNameList = new List<string>();
    static List<string> eventNameList = new List<string>();

    public static void startCompilation() {
        StaticCoroutine.DoCoroutine(CompileXMLFiles.compileAll()); //Call as a coroutine to not freeze up engine for large processes
    }

    //Some helper functions to return compile errors
    public static void printCompileErrorStringBadType(XElement xElement, string fileName, string elementValue, string desiredType) {
        Debug.Log("At line" + ((IXmlLineInfo)xElement).LineNumber + " in " + fileName + ", " + elementValue + " should be of " + desiredType + "type.\nDocument compiled, but element not included.");
    }

    public static void printCompileErrorStringMissingSelfReference(XElement xElement, string fileName, string elementValue, string fieldName) {
        Debug.Log("At line" + ((IXmlLineInfo)xElement).LineNumber + " in " + fileName + ", " + elementValue + " should be defined within this file for the " + fieldName + 
                " field.\nDocument compiled, but element not included.");
    }

    public static void printCompileErrorStringUndefinedField(XElement xElement, string fileName) {
        Debug.Log("At line" + ((IXmlLineInfo)xElement).LineNumber + " in " + fileName + ", the field" + xElement.Name + " is unexpected.\nDocument compiled, but element not included.");
    }

    public static void printCompileErrorFailSpecialClause(XElement xElement, string fileName, string specialClause) {
        Debug.Log("At line" + ((IXmlLineInfo)xElement).LineNumber + " in " + fileName + ", " + xElement.Name + " must be " + specialClause);
    }

    public static void printCompileErrorMissingField(string fileName, string field) {
        
    }

    public static IEnumerator compileAll() {
        UnityEditor.EditorUtility.DisplayProgressBar("Compiling","Starting compilation", 0);
        abilityNameList.Clear();
        abilityNameList.Add("NONE");
        typeNameList.Clear();
        typeNameList.Add("NONE");
        moveNameList.Clear();
        moveNameList.Add("NONE");
        itemNameList.Clear();
        itemNameList.Add("NONE");
        pokemonNameList.Clear();
        pokemonNameList.Add("NONE");
        mapNameList.Clear();
        eventNameList.Clear();

        UnityEditor.EditorUtility.DisplayProgressBar("Compiling", "Compiling Abilities", 0);
        CompileXMLFiles.compileAbilities();
        UnityEditor.EditorUtility.DisplayProgressBar("Compiling", "Compiling Types", 0.1f);
        CompileXMLFiles.compileTypes();
        UnityEditor.EditorUtility.DisplayProgressBar("Compiling", "Compiling Moves", 0.15f);
        CompileXMLFiles.compileMoves();
        UnityEditor.EditorUtility.DisplayProgressBar("Compiling", "Compiling Items", 0.25f);
        CompileXMLFiles.compileItems();
        UnityEditor.EditorUtility.DisplayProgressBar("Compiling", "Compiling Pokémon", 0.40f);
        CompileXMLFiles.compilePokemon();
        UnityEditor.EditorUtility.DisplayProgressBar("Compiling", "Compiling Events", 0.90f);
        UnityEditor.AssetDatabase.Refresh();
        UnityEditor.EditorUtility.ClearProgressBar();
        yield return null;
    }

    public static void compileAbilities() {

        //textwriter, for writing to abilityEnum.cs
        using (TextWriter abilityEnumTW = File.CreateText("Assets/Resources/Data/AbilityEnum.cs")) {
            //write the basics to the enum file
            abilityEnumTW.WriteLine("using UnityEngine;");
            abilityEnumTW.WriteLine();
            abilityEnumTW.WriteLine("public enum PBAbilities {");

            string curAbilityEnum = "";
            string curAbilityName = "";
            string curAbilityDesc = "";

            //clear the abilities list
            AbilityManager.clearList();

            try {
                XDocument reader = XDocument.Load("Assets/Resources/Editor/Abilities.xml");
                reader.StripNamespace();

                //read each ability
                foreach (XElement xe in reader.Descendants("Ability")) {
                    //write previously found ability because the last ability found shouldn't have a comma after it
                    if (!curAbilityEnum.Equals("")) {
                        abilityEnumTW.WriteLine("\t{0}\t,", curAbilityEnum);
                    }
                    curAbilityEnum = "";
                    curAbilityName = "";
                    curAbilityDesc = "";
                    //read each element in the ability (InternalName, Name, and Description in this case)
                    foreach (XElement childXe in xe.Elements()) {
                        switch (childXe.Name.ToString()) {
                            case "InternalName":
                                curAbilityEnum = childXe.Value;
                                //check validity of the Internal Name
                                //must be uppercase, and no non-alphanumeric characters
                                if (!System.Text.RegularExpressions.Regex.IsMatch(curAbilityEnum, "^[A-Z0-9]*$")) {
                                    Debug.Log("Invalid InternalName of " + curAbilityEnum + " must be all caps (Example:  DRIZZLE), and be all alphanumeric characters.  Ability skipped.");
                                    curAbilityEnum = "";
                                }
                                break;
                            case "Name":
                                curAbilityName = childXe.Value;
                                break;
                            case "Description":
                                curAbilityDesc = childXe.Value;
                                break;
                            default:
                                Debug.Log("Invalid elecment " + childXe.Name + " for ability " + xe.Value + ".  Document compiled, but element is not included");
                                break;
                        }
                    }
                    //invalidate ability if not containing all required elements
                    if (curAbilityName.Equals("")) {
                        Debug.Log("Ability " + curAbilityEnum + "is missing a 'Name' field.  Ability skipped");
                        curAbilityEnum = "";
                    }
                    if (curAbilityDesc.Equals("")) {
                        Debug.Log("Ability " + curAbilityEnum + "is missing a 'Description' field.  Ability skipped");
                        curAbilityEnum = "";
                    }
                    //add the found ability to AbilityManager, so it can save it.  don't add if invalid
                    if (!curAbilityEnum.Equals("")) {
                        AbilityManager.addAbility(curAbilityEnum, curAbilityName, curAbilityDesc);
                        abilityNameList.Add(curAbilityEnum);
                    }
                }
                //try to ensure (as best as possible) that there are no compilation errors in abilityEnum.cs
                //so when we fix our Abilities.xml, we can just compile compile it right away
                if (!curAbilityEnum.Equals("")) {
                    abilityEnumTW.WriteLine("\t{0}\t", curAbilityEnum);
                }
            } catch {
                if (!curAbilityEnum.Equals("")) {
                    abilityEnumTW.WriteLine("\tBADVALUE\t");
                }
            }
            abilityEnumTW.WriteLine("}");            
        }
        //save all data writen to file, the load it back
        //save all data writen to file, the load it back
        if (AbilityManager.getAbilities().Count > 0) {
            AbilityManager.saveDataFile();
            AbilityManager.loadDataFile();
        } else {
            Debug.Log("You have 0 abilities successfully defined, please check Abilities.xml to remedy this");
        }
    }


    public static void compileTypes() {
        string filePath = "Assets/Resources/Editor/Types.xml";

        //textwriter, for writing to TypesEnum.cs
        using (TextWriter typeEnumTW = File.CreateText("Assets/Resources/Data/TypeEnum.cs")) {
            //write the basics to the enum file
            typeEnumTW.WriteLine("using UnityEngine;");
            typeEnumTW.WriteLine();
            typeEnumTW.WriteLine("public enum PBTypes {");


            string curTypeEnum = "";
            string curTypeName = "";
            bool curIsSpecial = false;
            List<string> curWeaknesses = new List<string>();
            List<string> curResistances = new List<string>();
            List<string> curImmunities = new List<string>();


            //write enum file first, so we can use it for validation of other elements
            try {
                XDocument reader = XDocument.Load(filePath);
                reader.StripNamespace();

                //read each ability
                foreach (XElement xe in reader.Descendants("Types")) {
                    foreach (XElement childXE in xe.Elements("Type")) {
                        //write previously found ability because the last ability found shouldn't have a comma after it
                        if (!curTypeEnum.Equals("")) {
                            typeEnumTW.WriteLine("\t{0}\t,", curTypeEnum);
                            typeNameList.Add(curTypeEnum);
                        }
                        curTypeEnum = "";
                        if (xe.Element("Type") != null) {
                            curTypeEnum = childXE.Element("InternalName").Value;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(curTypeEnum, "^[A-Z]*$")) {
                                Debug.Log("Invalid InternalName of " + curTypeEnum + " must be all caps (Example: FIRE), and be all alphabetical characters.  Type skipped.");
                                curTypeEnum = "";
                            }
                        }
                    }
                }
                //try to ensure (as best as possible) that there are no compilation errors in abilityEnum.cs
                //so when we fix our Abilities.xml, we can just compile compile it right away
                if (!curTypeEnum.Equals("")) {
                    typeEnumTW.WriteLine("\t{0}\t", curTypeEnum);
                    typeNameList.Add(curTypeEnum);
                }
            } catch {
                if (!curTypeEnum.Equals("")) {
                    typeEnumTW.WriteLine("\tBADVALUE\t");
                }
            }
            typeEnumTW.WriteLine("}");
            typeEnumTW.Close();


            //clear the types list
            TypeManager.clearList();


            try {
                XDocument reader = XDocument.Load("Assets/Resources/Editor/Types.xml");
                reader.StripNamespace();

                //add each type to TypeManager
                foreach (XElement xe in reader.Descendants("Type")) {
                    curTypeEnum = "";
                    curTypeName = "";
                    curIsSpecial = false;
                    curWeaknesses.Clear();
                    curResistances.Clear();
                    curImmunities.Clear();


                    //read each element in the ability (InternalName, Name, and Description in this case)
                    foreach (XElement childXe in xe.Elements()) {
                        switch (childXe.Name.ToString().ToUpper()) {
                            case "INTERNALNAME":
                                curTypeEnum = childXe.Value;
                                //check validity of the Internal Name
                                //must be uppercase, and no non-alphanumeric characters
                                if (!System.Text.RegularExpressions.Regex.IsMatch(curTypeEnum, "^[A-Z]*$")) {
                                    curTypeEnum = "";
                                }
                                break;
                            case "NAME":
                                curTypeName = childXe.Value;
                                break;
                            case "ISSPECIAL":
                            case "ISSPECIALTYPE":
                            case "ISSPECIALTYPE?":
                            case "ISSPECIAL?":
                                if (bool.TryParse(childXe.Value, out curIsSpecial)) {
                                    curIsSpecial = bool.Parse(childXe.Value.ToString());
                                } else {
                                    printCompileErrorStringBadType(childXe, filePath, childXe.Value.ToString(), "bool");
                                    curTypeEnum = "";
                                }
                                break;
                            case "WEAKNESS":
                                if (typeNameList.Contains(childXe.Value)) {
                                    curWeaknesses.Add(childXe.Value.ToString());
                                } else {
                                    printCompileErrorStringMissingSelfReference(childXe, filePath, childXe.Value.ToString(), childXe.Name.ToString().ToUpper());
                                    curTypeEnum = "";
                                }
                                break;
                            case "RESISTANCE":
                                if (typeNameList.Contains(childXe.Value)) {
                                    curResistances.Add(childXe.Value.ToString());
                                } else {
                                    printCompileErrorStringMissingSelfReference(childXe, filePath, childXe.Value.ToString(), childXe.Name.ToString().ToUpper());
                                    curTypeEnum = "";
                                }
                                break;
                            case "IMMUNITY":
                                if (typeNameList.Contains(childXe.Value)) {
                                    curImmunities.Add(childXe.Value.ToString());
                                } else {
                                    printCompileErrorStringMissingSelfReference(childXe, filePath, childXe.Value.ToString(), childXe.Name.ToString().ToUpper());
                                    curTypeEnum = "";
                                }
                                break;
                            default:
                                printCompileErrorStringUndefinedField(childXe, filePath);
                                break;
                        }
                    }
                    //invalidate ability if not containing all required elements
                    if (curTypeName.Equals("")) {
                        Debug.Log("Ability " + curTypeEnum + "is missing a 'Name' field.  Ability skipped");
                        curTypeEnum = "";
                    }
                    //add the found ability to AbilityManager, so it can save it.  don't add if invalid
                    if (!curTypeEnum.Equals("")) {
                        TypeManager.addType(curTypeEnum, curTypeName, curIsSpecial, curWeaknesses, curResistances, curImmunities);
                    }
                }

            } catch (Exception e) {
                Debug.Log(e);
                return;
            }
        }
        //save all data writen to file, the load it back
        if (TypeManager.getNumTypes() > 0) {
            TypeManager.saveDataFile();
            TypeManager.loadDataFile();
        } else {
            Debug.Log("You have 0 types successfully defined, please check Types.xml to remedy this");
        }
    }


    public static void compileMoves() {

        string filePath = "Assets/Resources/Editor/Moves.xml";

        //textwriter, for writing to TypesEnum.cs
        using (TextWriter moveEnumTW = File.CreateText("Assets/Resources/Data/MoveEnum.cs")) {
            //write the basics to the enum file
            moveEnumTW.WriteLine("using UnityEngine;");
            moveEnumTW.WriteLine();
            moveEnumTW.WriteLine("public enum PBMoves {");
            moveEnumTW.WriteLine("\tNONE\t,");

            string curMoveEnum = "";
            string lastValidMove = "";
            string curMoveName, curMoveEffectType, curMoveType, curMoveCategory, curMoveTargetType, curMoveEffectTargetAbility;
            string curMoveEffectTargetType, curMoveDesc;
            List<string> curMoveAdditionalEffects, curMoveDamageModifiers, curMoveHitModifiers, curMoveAccuracyModifiers;
            List<string> curMoveSuccessModifiers, curMoveTargetModifiers, curMovePriorityModifiers, curMoveTypeModifiers;
            int[] curMoveStatChanges;
            int curAdditionalEffectChance, curBaseDamage, curMoveAccuracy, curBasePP, curPriority;
            bool curMakesContact, curBlockedByProtect, curBlockedByMagicBounce, curCanBeSnatched, curCanBeCopied;
            bool curAffectedByKingsRock, curThawsUser, curHighCritChance, curBitingMove, curPunchingMove;
            bool curSoundBasedMove, curPowderBasedMove, curPulseBasedMove, curBombBasedMove, skipMove;


            //write enum file first, so we can use it for validation of other elements
            try {
                XDocument reader = XDocument.Load(filePath);
                reader.StripNamespace();

                //read each ability
                foreach (XElement xe in reader.Descendants("Moves")) {
                    foreach (XElement childXE in xe.Elements("Move")) {
                        //write previously found ability because the last ability found shouldn't have a comma after it
                        if (!curMoveEnum.Equals("")) {
                            moveEnumTW.WriteLine("\t{0}\t,", curMoveEnum);
                            moveNameList.Add(curMoveEnum);
                        }
                        curMoveEnum = "";
                        if (xe.Element("Move") != null) {
                            curMoveEnum = childXE.Element("InternalName").Value;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(curMoveEnum, "^[0-Z]*$")) {
                                Debug.Log("Invalid InternalName of " + curMoveEnum + ", must be all caps (Example: THUNDERBOLT), and be all alphabetical or numerical characters.  Move skipped.");
                                curMoveEnum = "";
                            }
                        }
                    }
                }
                //try to ensure (as best as possible) that there are no compilation errors in MovesEnum.cs
                //so when we fix our Moves.xml, we can just compile compile it right away
                if (!curMoveEnum.Equals("")) {
                    moveEnumTW.WriteLine("\t{0}\t", curMoveEnum);
                    moveNameList.Add(curMoveEnum);
                }
            } catch (Exception e) {
                if (!curMoveEnum.Equals("")) {
                    moveEnumTW.WriteLine("\tBADVALUE\t");
                }

                Debug.Log(e.ToString());


            }
            moveEnumTW.WriteLine("}");
            moveEnumTW.Close();

            //clear the moves list
            MoveManager.clearList();

            try {
                XDocument reader = XDocument.Load(filePath);
                reader.StripNamespace();

                //add each type to MoveManager
                foreach (XElement xe in reader.Descendants("Move")) {
                    curMoveEnum = curMoveName = curMoveEffectType = curMoveType = curMoveCategory = "";
                    curMoveTargetType = curMoveDesc = curMoveEffectTargetAbility = curMoveEffectTargetType = "";
                    curBasePP = curAdditionalEffectChance = 0;
                    curMoveAccuracy = curBaseDamage = -1;
                    curPriority = -7;
                    curMakesContact = curBlockedByProtect = curBlockedByMagicBounce = curCanBeSnatched = curCanBeCopied = false;
                    curAffectedByKingsRock = curThawsUser = curHighCritChance = curBitingMove = curPunchingMove = false;
                    curSoundBasedMove = curPowderBasedMove = curPulseBasedMove = curBombBasedMove = skipMove = false;
                    curMoveAdditionalEffects = new List<string>();
                    curMoveDamageModifiers = new List<string>();
                    curMoveHitModifiers = new List<string>();
                    curMoveAccuracyModifiers = new List<string>();
                    curMoveSuccessModifiers = new List<string>();
                    curMoveTargetModifiers = new List<string>();
                    curMovePriorityModifiers = new List<string>();
                    curMoveTypeModifiers = new List<string>();
                    curMoveStatChanges = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

                    //read each element in the move
                    foreach (XElement childXe in xe.Elements()) {
                        switch (childXe.Name.ToString().ToUpper()) {
                            case "INTERNALNAME":
                                curMoveEnum = childXe.Value;
                                //check validity of the Internal Name
                                //must be uppercase, and no non-alphanumeric characters
                                if (!System.Text.RegularExpressions.Regex.IsMatch(curMoveEnum, "^[0-Z]*$")) {
                                    curMoveEnum = "";
                                } else {
                                    lastValidMove = curMoveEnum;
                                }
                                break;
                            case "NAME":
                                curMoveName = childXe.Value;
                                if (curMoveName.Length == 0) {
                                    curMoveEnum = "";
                                }
                                break;
                            case "MOVEEFFECTTYPE":
                                curMoveEffectType = childXe.Value;
                                if (curMoveEffectType.Length == 0) {
                                    curMoveEnum = "";
                                }
                                break;
                            case "BASEDAMAGE":
                                int.TryParse(childXe.Value, out curBaseDamage);
                                if (curBaseDamage < 0) {
                                    curMoveEnum = "";
                                    skipMove = true;
                                    printCompileErrorStringBadType(childXe, filePath, childXe.Value.ToString(), "bool");
                                }
                                break;
                            case "MOVETYPE":
                                if (typeNameList.Contains(childXe.Value.ToUpper())) {
                                    curMoveType = childXe.Value.ToUpper();
                                } else {
                                    printCompileErrorStringBadType(childXe, filePath, childXe.Value.ToString(), "PBTypes");
                                    curMoveEnum = "";
                                    skipMove = true;
                                }
                                break;
                            case "MOVECATEGORY":
                                if (System.Enum.IsDefined(typeof(MoveDamageCategory), childXe.Value.ToUpper())) {
                                    curMoveCategory = childXe.Value.ToUpper();
                                } else {
                                    printCompileErrorStringBadType(childXe, filePath, childXe.Value.ToString(), "MoveDamageCategory");
                                    curMoveEnum = "";
                                    skipMove = true;
                                }
                                break;
                            case "ACCURACY":
                                int.TryParse(childXe.Value, out curMoveAccuracy);
                                if (curMoveAccuracy < 0 || curMoveAccuracy > 100) {
                                    curMoveEnum = "";
                                    skipMove = true;
                                    printCompileErrorFailSpecialClause(childXe, filePath, "between 0 and 100");
                                }
                                break;
                            case "BASEPP":
                                int.TryParse(childXe.Value, out curBasePP);
                                if (curBasePP <= 0) {
                                    curMoveEnum = "";
                                    skipMove = true;
                                    printCompileErrorFailSpecialClause(childXe, filePath, "greater than 0");
                                }
                                break;
                            case "ADDITIONALEFFECTCHANCE":
                                int.TryParse(childXe.Value, out curAdditionalEffectChance);
                                if (curAdditionalEffectChance < 0 || curAdditionalEffectChance > 100) {
                                    curMoveEnum = "";
                                    skipMove = true;
                                    printCompileErrorFailSpecialClause(childXe, filePath, "between 0 and 100");

                                }
                                break;
                            case "TARGETTYPE":
                                if (System.Enum.IsDefined(typeof(MoveTargetType), childXe.Value.ToUpper())) {
                                    curMoveTargetType = childXe.Value.ToUpper();
                                } else {
                                    printCompileErrorStringBadType(childXe, filePath, childXe.Value.ToString(), "MoveTargetType");
                                    curMoveEnum = "";
                                    skipMove = true;
                                }
                                break;
                            case "PRIORITY":
                                int.TryParse(childXe.Value, out curPriority);
                                if (curPriority < -6 || curPriority > 6) {
                                    curMoveEnum = "";
                                    skipMove = true;
                                    printCompileErrorFailSpecialClause(childXe, filePath, "between -6 and 6");
                                }
                                break;
                            case "FLAGS":
                                foreach (XElement flagChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (flagChildXE.Name.ToString().ToUpper()) {
                                        case "MAKESCONTACT":
                                            if (bool.TryParse(flagChildXE.Value, out curMakesContact)) {
                                                curMakesContact = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "BLOCKEDBYPROTECT":
                                            if (bool.TryParse(flagChildXE.Value, out curBlockedByProtect)) {
                                                curBlockedByProtect = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "BLOCKEDBYMAGICBOUNCE":
                                            if (bool.TryParse(flagChildXE.Value, out curBlockedByMagicBounce)) {
                                                curBlockedByMagicBounce = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "CANBESNATCHED":
                                            if (bool.TryParse(flagChildXE.Value, out curCanBeSnatched)) {
                                                curCanBeSnatched = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "CANBECOPIED":
                                            if (bool.TryParse(flagChildXE.Value, out curCanBeCopied)) {
                                                curCanBeCopied = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "AFFECTEDBYKINGSROCK":
                                            if (bool.TryParse(flagChildXE.Value, out curAffectedByKingsRock)) {
                                                curAffectedByKingsRock = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "THAWSUSER":
                                            if (bool.TryParse(flagChildXE.Value, out curThawsUser)) {
                                                curThawsUser = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "HIGHCRITCHANCE":
                                            if (bool.TryParse(flagChildXE.Value, out curHighCritChance)) {
                                                curHighCritChance = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "BITINGMOVE":
                                            if (bool.TryParse(flagChildXE.Value, out curBitingMove)) {
                                                curBitingMove = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "PUNCHINGMOVE":
                                            if (bool.TryParse(flagChildXE.Value, out curPunchingMove)) {
                                                curPunchingMove = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "SOUNDBASEDMOVE":
                                            if (bool.TryParse(flagChildXE.Value, out curSoundBasedMove)) {
                                                curSoundBasedMove = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "POWDERBASEDMOVE":
                                            if (bool.TryParse(flagChildXE.Value, out curPowderBasedMove)) {
                                                curPowderBasedMove = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "PULSEBASEDMOVE":
                                            if (bool.TryParse(flagChildXE.Value, out curPulseBasedMove)) {
                                                curPulseBasedMove = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        case "BOMBBASEDMOVE":
                                            if (bool.TryParse(flagChildXE.Value, out curBombBasedMove)) {
                                                curBombBasedMove = bool.Parse(flagChildXE.Value);
                                            } else {
                                                printCompileErrorStringBadType(flagChildXE, filePath, flagChildXE.Value.ToString(), "bool");
                                                curMoveEnum = "";
                                                skipMove = true;
                                            }
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(flagChildXE, filePath);
                                            break;

                                    }
                                }
                                break;
                            case "MOVEDESCRIPTION":
                                curMoveDesc = childXe.Value;
                                if (!(curMoveDesc.Length > 0)) {
                                    curMoveEnum = "";
                                    skipMove = true;
                                }
                                break;
                            case "ADDITIONALEFFECTS":
                                foreach (XElement additionalEffectChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (additionalEffectChildXE.Name.ToString()) {
                                        case "ADDITIONALEFFECT":
                                            curMoveAdditionalEffects.Add(additionalEffectChildXE.Value);
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(additionalEffectChildXE, filePath);
                                            break;
                                    }
                                }
                                break;
                            case "ACCURACYMODIFIERS":
                                foreach (XElement accuracyModChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (accuracyModChildXE.Name.ToString()) {
                                        case "ACCURACYMODIFIER":
                                            curMoveAccuracyModifiers.Add(accuracyModChildXE.Value);
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(accuracyModChildXE, filePath);
                                            break;
                                    }
                                }
                                break;
                            case "DAMAGEMODIFIERS":
                                foreach (XElement damageModChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (damageModChildXE.Name.ToString()) {
                                        case "DAMAGEMODIFIER":
                                            curMoveDamageModifiers.Add(damageModChildXE.Value);
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(damageModChildXE, filePath);
                                            break;
                                    }
                                }
                                break;
                            case "SUCCESSMODIFIERS":
                                foreach (XElement successModChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (successModChildXE.Name.ToString()) {
                                        case "SUCCESSMODIFIER":
                                            curMoveSuccessModifiers.Add(successModChildXE.Value);
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(successModChildXE, filePath);
                                            break;
                                    }
                                }
                                break;
                            case "TARGETMODIFIERS":
                                foreach (XElement targetModChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (targetModChildXE.Name.ToString()) {
                                        case "TARGETMODIFIER":
                                            curMoveTargetModifiers.Add(targetModChildXE.Value);
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(targetModChildXE, filePath);
                                            break;
                                    }
                                }
                                break;
                            case "PRIORITYMODIFIERS":
                                foreach (XElement priorityModChildXE in childXe.Elements("PRIORITYMODIFIER")) {
                                    curMovePriorityModifiers.Add(priorityModChildXE.Value);
                                }
                                break;
                            case "TYPEMODIFIERS":
                                foreach (XElement typeModChildXE in childXe.Elements("TYPEMODIFIER")) {
                                    curMoveTypeModifiers.Add(typeModChildXE.Value);
                                }
                                break;
                            case "HITMODIFIERS":
                                foreach (XElement hitModChildXE in childXe.Elements("HITMODIFIER")) {
                                    curMoveHitModifiers.Add(hitModChildXE.Value);
                                }
                                break;
                            case "MOVESTATCHANGE":
                                foreach (XElement statChangeChildXE in childXe.Elements()) {
                                    if (skipMove) {
                                        break;
                                    }
                                    switch (statChangeChildXE.Name.ToString().ToUpper()) {
                                        case "ATTACK":
                                            curMoveStatChanges[0] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        case "DEFENSE":
                                            curMoveStatChanges[1] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        case "SPATK":
                                            curMoveStatChanges[2] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        case "SPDEF":
                                            curMoveStatChanges[3] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        case "SPEED":
                                            curMoveStatChanges[4] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        case "EVASION":
                                            curMoveStatChanges[5] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        case "ACCURACY":
                                            curMoveStatChanges[6] = Int32.Parse(statChangeChildXE.Value);
                                            break;
                                        default:
                                            printCompileErrorStringUndefinedField(statChangeChildXE, filePath);
                                            break;
                                    }
                                }
                                break;
                            case "MOVETARGETABILITY":
                                curMoveEffectTargetAbility = childXe.Value;
                                if (curMoveEffectTargetAbility.Length == 0) {
                                    curMoveEnum = "";
                                }
                                break;
                            case "MOVETARGETTYPE":
                                curMoveEffectTargetType = childXe.Value;
                                if (curMoveEffectTargetType.Length == 0) {
                                    curMoveEnum = "";
                                }
                                break;
                            default:
                                printCompileErrorStringUndefinedField(childXe, filePath);
                                break;
                        }
                        if (skipMove) {
                            break;
                        }
                    }
                    if (!skipMove) {
                        if (!curMoveEnum.Equals("")) {
                            //invalidate move if not containing all required elements, inform user of each missing field
                            if (curMoveName.Equals("")) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'Name' field.");
                                skipMove = true;
                            }
                            if (curMoveEffectType.Equals("")) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'MoveEffectType' field.");
                                skipMove = true;
                            }
                            if (curBaseDamage < 0) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'BaseDamage' field.");
                                skipMove = true;
                            }
                            if (curMoveType.Equals("")) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'MoveType' field.");
                                skipMove = true;
                            }
                            if (curMoveCategory.Equals("")) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'MoveCategory' field.");
                                skipMove = true;
                            }
                            if (curMoveAccuracy == -1) {
                                Debug.Log("Move " + curMoveEnum + " is missing an 'Accuracy' field.");
                                skipMove = true;
                            }
                            if (curBasePP == -1) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'BasePP' field.");
                                skipMove = true;
                            }
                            if (curAdditionalEffectChance != 0) {
                                if (curMoveAdditionalEffects.Count == 0) {
                                    Debug.Log("Move " + curMoveEnum + " requires AdditionalEffects/AdditionalEffect fields, but does not list any.");
                                    skipMove = true;
                                }
                            }
                            if (curMoveTargetType.Equals("")) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'MoveTargetType' field.");
                                skipMove = true;
                            }
                            if (curMoveDesc.Equals("")) {
                                Debug.Log("Move " + curMoveEnum + " is missing a 'MoveDescription' field.");
                                skipMove = true;
                            }

                            if (skipMove) {
                                Debug.Log("One or more fields for " + curMoveEnum + " are invalid, skipping move.\n\n");
                            } else {
                                //if all required fields are present, add move
                                MoveManager.addMove(curMoveEnum, curMoveName,
                                            curMoveEffectType, curBaseDamage, curMoveType,
                                            (MoveDamageCategory)Enum.Parse(typeof(MoveDamageCategory), curMoveCategory), curMoveAccuracy,
                                            curBasePP, curAdditionalEffectChance, (MoveTargetType)Enum.Parse(typeof(MoveTargetType), curMoveTargetType),
                                            curPriority, curMakesContact, curBlockedByProtect, curBlockedByMagicBounce, curCanBeSnatched, curCanBeCopied,
                                            curAffectedByKingsRock, curThawsUser, curHighCritChance, curBitingMove, curPunchingMove, curSoundBasedMove,
                                            curPowderBasedMove, curPulseBasedMove, curBombBasedMove, curMoveDesc, curMoveEffectTargetAbility, curMoveEffectTargetType,
                                            curMoveAdditionalEffects, curMoveDamageModifiers, curMoveHitModifiers, curMoveAccuracyModifiers,
                                            curMoveSuccessModifiers, curMoveTargetModifiers, curMovePriorityModifiers, curMoveTypeModifiers,
                                            curMoveStatChanges);
                            }
                        } else {
                            //print error report to the user to let know of missing InternalName
                            if (curMoveName.Equals("")) {
                                Debug.Log("Move following " + lastValidMove + "is missing an 'InternalName' field.  Move skipped");
                            } else {
                                Debug.Log("Move " + curMoveName + "is missing an 'InternalName' field.  Move skipped");
                            }
                        }
                    } else {
                        if (curMoveEnum.Equals("")) {
                            Debug.Log("Move following " + lastValidMove + "is missing one or more fields.  Move skipped");
                        } else {
                            Debug.Log("Move " + curMoveEnum + "is missing on or more fields.  Move skipped");
                        }
                    }
                }

            } catch (Exception e) {
                Debug.Log(e);
                return;
            }
        }
        //save all data writen to file, the load it back
        if (MoveManager.getNumMoves() > 0) {
            MoveManager.saveDataFile();
            MoveManager.loadDataFile();
            //MoveManager.printEachMoveName();
        } else {
            Debug.Log("You have 0 moves successfully defined, please check Moves.xml to remedy this");
        }
    }

    public static void compileItems() {

        List<string> ballItems = new List<string>();

        //textwriter, for writing to abilityEnum.cs
        using (TextWriter itemEnumTW = File.CreateText("Assets/Resources/Data/ItemEnum.cs")) {
            //write the basics to the enum file
            itemEnumTW.WriteLine("using UnityEngine;");
            itemEnumTW.WriteLine();
            itemEnumTW.WriteLine("public enum PBItems {");
            itemEnumTW.WriteLine("\tNONE\t,");

            string curItemEnum = "";
            string curItemName = "";
            string curItemPluralName = "";
            string curItemBagPocketType = "";
            int curItemPrice = -1;
            string curItemDesc = "";
            string curItemInFieldUseMethod = "";
            string curItemUsageTypeInField = "";
            string curItemInBattleUseMethod = "";
            string curItemUsageTypeInBattle = "";
            string curItemSpecialType = "";
            string curItemMachineMove = "";

            bool skipItem = false;
            string lastValidItem = "";

            //write enum file first, so we can use it for validation of other elements
            try {
                XDocument reader = XDocument.Load("Assets/Resources/Editor/Items.xml");
                reader.StripNamespace();

                //read each ability
                foreach (XElement xe in reader.Descendants("Items")) {
                    foreach (XElement childXE in xe.Elements("Item")) {
                        //write previously found item because the last ability found shouldn't have a comma after it
                        if (!curItemEnum.Equals("")) {
                            itemEnumTW.WriteLine("\t{0}\t,", curItemEnum);
                            itemNameList.Add(curItemEnum);
                        }
                        curItemEnum = "";
                        if (xe.Element("Item") != null) {
                            curItemEnum = childXE.Element("InternalName").Value;
                            if (!System.Text.RegularExpressions.Regex.IsMatch(curItemEnum, "^[0-Z]*$")) {
                                Debug.Log("Invalid InternalName of " + curItemEnum + " must be all caps (Example: REPEL), and be all alphabetical characters.  Item skipped.");
                                curItemEnum = "";
                            }
                        }
                    }
                }
                //try to ensure (as best as possible) that there are no compilation errors in abilityEnum.cs
                //so when we fix our Abilities.xml, we can just compile compile it right away
                if (!curItemEnum.Equals("")) {
                    itemEnumTW.WriteLine("\t{0}\t", curItemEnum);
                    itemNameList.Add(curItemEnum);
                }
            } catch {
                if (!curItemEnum.Equals("")) {
                    itemEnumTW.WriteLine("\tBADVALUE\t");
                }
            }
            itemEnumTW.WriteLine("}");
            itemEnumTW.WriteLine("public class FakeClass : MonoBehaviour {\n\n}");
            itemEnumTW.Close();


            //clear the types list
            ItemManager.clearList();


            try {
                XDocument reader = XDocument.Load("Assets/Resources/Editor/Items.xml");
                reader.StripNamespace();

                //add each type to TypeManager
                foreach (XElement xe in reader.Descendants("Item")) {

                    curItemEnum = "";
                    curItemName = "";
                    curItemPluralName = "";
                    curItemBagPocketType = "NONE";
                    curItemPrice = -1;
                    curItemDesc = "";
                    curItemInFieldUseMethod = "";
                    curItemUsageTypeInField = "NONE";
                    curItemInBattleUseMethod = "NONE";
                    curItemUsageTypeInBattle = "NONE";
                    curItemSpecialType = "NONE";
                    curItemMachineMove = "NONE";

                    skipItem = false;


                    //read each element in the ability (InternalName, Name, and Description in this case)
                    foreach (XElement childXe in xe.Elements()) {
                        switch (childXe.Name.ToString()) {
                            case "InternalName":
                                curItemEnum = childXe.Value;
                                //check validity of the Internal Name
                                //must be uppercase, and no non-alphanumeric characters
                                if (!System.Text.RegularExpressions.Regex.IsMatch(curItemEnum, "^[0-Z]*$")) {
                                    curItemEnum = "";
                                } else {
                                    lastValidItem = curItemEnum;
                                }
                                break;
                            case "Name":
                                curItemName = childXe.Value;
                                break;
                            case "PluralName":
                                curItemPluralName = childXe.Value;
                                break;
                            case "BagPocket":
                                if (System.Enum.IsDefined(typeof(PBPockets), childXe.Value)) {
                                    curItemBagPocketType = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for BagPocket , it must be all caps, and a type defined in the enum PBPockets");
                                }
                                break;
                            case "Price":
                                int.TryParse(childXe.Value, out curItemPrice);
                                if (curItemPrice < 0) {
                                    skipItem = true;
                                    Debug.Log(childXe.Value + "is not an acceptable price for " + curItemEnum + ", it must be 0 or greater");
                                }
                                break;
                            case "Description":
                                curItemDesc = childXe.Value;
                                break;
                            case "InFieldUseMethod":
                                if (itemNameList.Contains(childXe.Value)) {
                                    curItemInFieldUseMethod = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for InFieldUseMethod , it must be all caps, and a type defined in this XML");
                                }
                                break;
                            case "ItemUsageTypeInField":
                                if (System.Enum.IsDefined(typeof(ItemUsageInField), childXe.Value.ToString())) {
                                    curItemUsageTypeInField = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for ItemUsageTypeInField , it must be all caps, and a type defined in this XML");
                                }
                                break;
                            case "InBattleUseMethod":
                                if (itemNameList.Contains(childXe.Value)) {
                                    curItemInBattleUseMethod = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for InBattleUseMethod , it must be all caps, and a type defined in this XML");
                                }
                                break;
                            case "ItemUsageTypeInBattle":
                                if (System.Enum.IsDefined(typeof(ItemUsageDuringBattle), childXe.Value.ToString())) {
                                    curItemUsageTypeInBattle = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for ItemUsageTypeInBattle , it must be all caps, and a type defined in this XML");
                                }
                                break;
                            case "SpecialItemType":
                                if (System.Enum.IsDefined(typeof(ItemSpecialTypes), childXe.Value.ToString())) {
                                    curItemSpecialType = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for SpecialItemType , it must be all caps, and a type defined in this XML");
                                }
                                break;
                            case "MachineMove":
                                if (moveNameList.Contains(childXe.Value)) {
                                    curItemMachineMove = childXe.Value;
                                } else {
                                    skipItem = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for MachineMove , it must be all caps, and a type defined in this XML");
                                }
                                break;
                            default:
                                skipItem = true;
                                Debug.Log("Invalid elecment " + childXe.Name + " for item " + xe.Value + ".  This will not prevent compilation, but element is not included");
                                break;
                        }
                    }
                    if (!skipItem) {
                        if (!curItemEnum.Equals("")) {
                            //invalidate item if not containing all required elements, inform user of each missing field
                            if (curItemName.Equals("")) {
                                Debug.Log("Item " + curItemEnum + " is missing a 'Name' field.");
                                skipItem = true;
                            }
                            if (curItemPluralName.Equals("")) {
                                Debug.Log("Item " + curItemEnum + " is missing a 'NamePlural' field.");
                                skipItem = true;
                            }
                            if (curItemBagPocketType.Equals("")) {
                                Debug.Log("Item " + curItemEnum + " is missing a 'BagPocket' field.");
                                skipItem = true;
                            } else if ((curItemBagPocketType.Equals("TM") || curItemBagPocketType.Equals("HM")) && curItemMachineMove.Equals("")) {
                                Debug.Log("Item " + curItemEnum + " is missing a 'MachineMove' field, which is required for all TM & HM items.");
                                skipItem = true;
                            } else {
                                curItemMachineMove = "NONE";
                            }
                            if (curItemPrice < 0) {
                                Debug.Log("Move " + curItemEnum + " is missing a 'Price' field.");
                                skipItem = true;
                            }
                            if (curItemDesc.Equals("")) {
                                Debug.Log("Item " + curItemEnum + " is missing a 'Description' field.");
                                skipItem = true;
                            }
                            if (!curItemInFieldUseMethod.Equals("NONE")) {
                                if (curItemUsageTypeInField.Equals("")) {
                                    Debug.Log("Item " + curItemEnum + " is missing a 'ItemUsageTypeInField' field.");
                                    skipItem = true;
                                }
                            }
                            if (!curItemInBattleUseMethod.Equals("NONE")) {
                                if (curItemUsageTypeInBattle.Equals("")) {
                                    Debug.Log("Item " + curItemEnum + " is missing a 'ItemUsageTypeInBattle' field.");
                                    skipItem = true;
                                }
                            }

                            if (skipItem) {
                                Debug.Log("One or more fields for " + curItemEnum + " are invalid, skipping item.\n\n");
                            } else {
                                //if all required fields are present, add move
                                ItemManager.addItem(curItemEnum, curItemName,
                                            curItemPluralName, (PBPockets)Enum.Parse(typeof(PBPockets), curItemBagPocketType),
                                            curItemPrice, curItemDesc, curItemInFieldUseMethod,
                                            (ItemUsageInField)Enum.Parse(typeof(ItemUsageInField), curItemUsageTypeInField),
                                            curItemInBattleUseMethod,
                                            (ItemUsageDuringBattle)Enum.Parse(typeof(ItemUsageDuringBattle), curItemUsageTypeInBattle),
                                            (ItemSpecialTypes)Enum.Parse(typeof(ItemSpecialTypes), curItemSpecialType),
                                            curItemMachineMove);
                                if (curItemBagPocketType == Enum.GetName(typeof(PBPockets), PBPockets.POKEBALLS)) {
                                    ballItems.Add(curItemEnum);
                                }
                            }
                        } else {
                            //print error report to the user to let know of missing InternalName
                            if (curItemName.Equals("")) {
                                Debug.Log("Move following " + lastValidItem + " is missing an 'InternalName' field.  Item skipped");
                            } else {
                                Debug.Log("Item " + curItemName + " is missing an 'InternalName' field.  Item skipped");
                            }
                        }
                    } else {
                        if (curItemEnum.Equals("")) {
                            Debug.Log("Item following " + lastValidItem + " is missing one or more fields.  Item skipped");
                        } else {
                            Debug.Log("Item " + curItemEnum + " is missing on or more fields.  Item skipped");
                        }
                    }
                }
            } catch (Exception e) {
                Debug.Log(e);
                return;
            }
        }
        //save all data writen to file, the load it back
        if (ItemManager.getNumItems() > 0) {
            ItemManager.saveDataFile();
            ItemManager.loadDataFile();
            //Debug.Log(ItemManager.getNumItems());
            //ItemManager.printEachItemName();
        } else {
            Debug.Log("You have 0 items successfully defined, please check Items.xml to remedy this");
        }


        //Write ball items
        //textwriter, for writing to abilityEnum.cs
        using (TextWriter ballEnumTW = File.CreateText("Assets/Resources/Data/BallEnum.cs")) {
            //write the basics to the enum file
            ballEnumTW.WriteLine("using UnityEngine;");
            ballEnumTW.WriteLine();
            ballEnumTW.WriteLine("public enum PBBalls {");
            ballEnumTW.WriteLine("\tNONE\t,");

            for (int i = 0; i<ballItems.Count-1; ++i) {
                ballEnumTW.WriteLine("\t{0}\t,", ballItems[i]);
            }
            ballEnumTW.WriteLine("\t{0}\t", ballItems[ballItems.Count-1]);
            ballEnumTW.WriteLine("}");
            ballEnumTW.Close();
        }
    }


    public static void compilePokemon() {
        //textwriter, for writing to TypesEnum.cs
        using (TextWriter pokemonEnumTW = File.CreateText("Assets/Resources/Data/SpeciesEnum.cs")) {
            //write the basics to the enum file
            pokemonEnumTW.WriteLine("using UnityEngine;");
            pokemonEnumTW.WriteLine();
            pokemonEnumTW.WriteLine("public enum PBPokemon {");
            pokemonEnumTW.WriteLine("\tNONE\t,");


            string curMonEnum = "";
            string lastValidMon = "";
            string curMonName, curMonType1, curMonType2, curGenderRate, curGrowthRate, curEggType1, curEggType2;
            string curColor, curKind, curHabitat, curWildItemCommon, curWildItemUncommon, curWildItemRare, curIncenseType, curDexEntry;
            int[] curMonBaseStats, curEffortPoints;
            int curBaseExp, curRareness, curBaseHappiness, curBattlePlayerYPos, curBattleEnemyYPos, curBattlerAltitude, curStepsToHatch;
            float curHeight, curWeight;
            Dictionary<string, int> curDexNumbers;
            List<LevelUpMoves> curLevelUpMoves;
            List<string> curTeachableMoves, curEggMoves, curNaturalAbilities, curHiddenAbilities, curFormNames;
            List<EvolutionInfo> curEvolutions;
            bool skipMon;
            DexInfo tempDex = new DexInfo();
            int[] maxDexValues = new int[tempDex.dexList.Count];
            int speciesCount = 0;


            //write enum file first, so we can use it for validation of other elements
            try {
                XDocument reader = XDocument.Load("Assets/Resources/Editor/Pokemon.xml");
                reader.StripNamespace();

                //read each ability
                foreach (XElement xe in reader.Descendants("Pokemon")) {
                    foreach (XElement childXE in xe.Elements("Species")) {
                        //write previously found ability because the last Pok�mon found shouldn't have a comma after it
                        if (!curMonEnum.Equals("")) {
                            pokemonEnumTW.WriteLine("\t{0}\t,", curMonEnum);
                            pokemonNameList.Add(curMonEnum);
                            ++speciesCount;
                        }
                        curMonEnum = "";
                        if (xe.Element("Species") != null) {
                            
                            curMonEnum = childXE.Element("InternalName").Value;                            
                            if (!System.Text.RegularExpressions.Regex.IsMatch(curMonEnum, "^[0-Z]*$")) {
                                Debug.Log("Invalid InternalName of " + curMonEnum + " must be all caps (Example: PIKACHU), and be all alphabetical characters.  Pokemon skipped.");
                                curMonEnum = "";
                            }
                        }
                    }
                }
                //try to ensure (as best as possible) that there are no compilation errors in PokemonEnum.cs
                //so when we fix our Pokemon.xml, we can just compile compile it right away
                if (!curMonEnum.Equals("")) {
                    pokemonEnumTW.WriteLine("\t{0}\t", curMonEnum);
                    pokemonNameList.Add(curMonEnum);
                    ++speciesCount;
                }
            } catch {
                if (!curMonEnum.Equals("")) {
                    pokemonEnumTW.WriteLine("\tBADVALUE\t");
                }
            }
            pokemonEnumTW.WriteLine("}");
            pokemonEnumTW.Close();

            //Clear the species list
            SpeciesManager.clearList();

            //Count total number of Pokémon, use for national dex number
            int speciesCounter = 0;

            try {
                XDocument reader = XDocument.Load("Assets/Resources/Editor/Pokemon.xml");
                reader.StripNamespace();
                

                //add each Species to SpeciesManager
                foreach (XElement xe in reader.Descendants("Species")) {
                    
                    curMonEnum = curMonName = curMonType1 = curMonType2 =  curGenderRate = curGrowthRate = "";
                    curEggType1 = curEggType2 = curColor = curKind = curHabitat = "";
                    curWildItemCommon = curWildItemUncommon = curWildItemRare = curIncenseType = curDexEntry = "";
                    curMonBaseStats = curEffortPoints = new int[] {0, 0, 0, 0, 0, 0 };
                    curBaseExp = curRareness = curBaseHappiness = 0;
                    curStepsToHatch = curBattlePlayerYPos = curBattleEnemyYPos = curBattlerAltitude = 0;
                    curHeight = curWeight = 0.0f; //in meters, kilograms (respectively)
                    curDexNumbers = new Dictionary<string, int>();
                    curLevelUpMoves = new List<LevelUpMoves>();
                    curTeachableMoves = new List<string>();
                    curEggMoves = new List<string>();
                    curNaturalAbilities = new List<string>();
                    curHiddenAbilities = new List<string>();
                    curFormNames = new List<string>();
                    curEvolutions = new List<EvolutionInfo>();

                    skipMon = false;

                    //read each element for the Pok�mon
                    foreach (XElement childXe in xe.Elements()) {
                        switch (childXe.Name.ToString().ToUpper()) {
                            case "INTERNALNAME":
                                curMonEnum = childXe.Value;
                                //check validity of the Internal Name
                                //must be uppercase, and no non-alphanumeric characters
                                if (!System.Text.RegularExpressions.Regex.IsMatch(curMonEnum, "^[0-Z]*$")) {
                                    curMonEnum = "";
                                } else {
                                    lastValidMon = curMonEnum;
                                }
                                break;
                            case "NAME":
                                curMonName = childXe.Value;
                                break;
                            case "TYPE":  //use for both types
                                if (typeNameList.Contains(childXe.Value.ToUpper())) {
                                    curMonType1 = childXe.Value.ToUpper();
                                    curMonType2 = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Type , it must be a type defined in Types.xml");
                                }
                                break;
                            case "TYPE1":
                                if (typeNameList.Contains(childXe.Value.ToUpper())) {
                                    curMonType1 = childXe.Value.ToUpper();
                                    curMonType2 = childXe.Value.ToUpper(); //assume type2 until it is defined.  type 1 must be defined first anyway
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Type1 , it must be a type defined in Types.xml");
                                }
                                break;
                            case "TYPE2":
                                if (!curMonType1.Equals("") && typeNameList.Contains(childXe.Value.ToUpper())) { //type1 required to use type 2
                                    curMonType2 = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Type1 , it must be a type defined in Types.xml");
                                }
                                break;
                            case "BASESTATS":
                                foreach (XElement baseStatChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (baseStatChildXE.Name.ToString().ToUpper()) {
                                        case "HP":
                                            curMonBaseStats[0] = Int32.Parse(baseStatChildXE.Value);
                                            break;
                                        case "ATK":
                                            curMonBaseStats[1] = Int32.Parse(baseStatChildXE.Value);
                                            break;
                                        case "DEF":
                                            curMonBaseStats[2] = Int32.Parse(baseStatChildXE.Value);
                                            break;
                                        case "SPATK":
                                            curMonBaseStats[3] = Int32.Parse(baseStatChildXE.Value);
                                            break;
                                        case "SPDEF":
                                            curMonBaseStats[4] = Int32.Parse(baseStatChildXE.Value);
                                            break;
                                        case "SPEED":
                                            curMonBaseStats[5] = Int32.Parse(baseStatChildXE.Value);
                                            break;
                                        default:
                                            Debug.Log("Invalid Base Stat " + baseStatChildXE.Name + " for move " + curMonEnum+ ".  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "GENDERRATE":
                                if (System.Enum.IsDefined(typeof(GenderRate), childXe.Value.ToUpper())) {
                                    curGenderRate = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for GenderRate , it must be a type defined in the enum GenderRate");
                                }
                                break;
                            case "GROWTHRATE":
                                if (System.Enum.IsDefined(typeof(GrowthRate), childXe.Value.ToUpper())) {
                                    curGrowthRate = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for GrowthRate , it must be a type defined in the enum GrowthRate");
                                }
                                break;
                            case "BASEEXP":
                                int.TryParse(childXe.Value, out curBaseExp);
                                if (curBaseExp <= 0) {
                                    skipMon = true;
                                    Debug.Log(childXe.Value + "is not an acceptable Base EXP for " + curMonEnum + ", it must be greater than 0");
                                }
                                break;
                            case "EFFORTPOINTS":
                                foreach (XElement effortPointChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (effortPointChildXE.Name.ToString().ToUpper()) {
                                        case "HP":
                                            curEffortPoints[0] = Int32.Parse(effortPointChildXE.Value);
                                            break;
                                        case "ATK":
                                            curEffortPoints[1] = Int32.Parse(effortPointChildXE.Value);
                                            break;
                                        case "DEF":
                                            curEffortPoints[2] = Int32.Parse(effortPointChildXE.Value);
                                            break;
                                        case "SPATK":
                                            curEffortPoints[3] = Int32.Parse(effortPointChildXE.Value);
                                            break;
                                        case "SPDEF":
                                            curEffortPoints[4] = Int32.Parse(effortPointChildXE.Value);
                                            break;
                                        case "SPEED":
                                            curEffortPoints[5] = Int32.Parse(effortPointChildXE.Value);
                                            break;
                                        default:
                                            Debug.Log("Invalid Effort Value " + effortPointChildXE.Name + " for move " + curMonEnum + ".  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "RARENESS":
                                int.TryParse(childXe.Value, out curRareness);
                                if (curRareness <= 0 || curRareness>255) {
                                    skipMon = true;
                                    Debug.Log(curRareness + "is not an acceptable value for " + curMonEnum + "'s rareness, it must be greater than 0 and less than 101");
                                }
                                break;
                            case "BASEHAPPINESS":
                                int.TryParse(childXe.Value, out curBaseHappiness);
                                if (curBaseHappiness < 0 || curBaseHappiness > 255) {
                                    skipMon = true;
                                    Debug.Log(curBaseHappiness + "is not an acceptable value for " + curMonEnum + "'s base happiness, it must be at least 0 and less than 256");
                                }
                                break;
                            case "MOVESLEARNEDBYLEVELUP":
                                foreach (XElement levelUpMovesChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (levelUpMovesChildXE.Name.ToString().ToUpper()) {
                                        case "MOVE":
                                            int curMoveAddLevel = 0;
                                            string curMoveAddName = "";
                                            bool failedMove = false;
                                            foreach (XElement levelUpMoveChildXE in levelUpMovesChildXE.Elements()) {
                                                if (skipMon) {
                                                    break;
                                                }
                                                switch (levelUpMoveChildXE.Name.ToString().ToUpper()) {
                                                    case "MOVENAME":
                                                        if (moveNameList.Contains(levelUpMoveChildXE.Value.ToUpper())) {
                                                            curMoveAddName = levelUpMoveChildXE.Value.ToUpper();
                                                        } else {
                                                            skipMon = true;
                                                            failedMove = true;
                                                            Debug.Log("The move " + levelUpMoveChildXE.Value + " is not a defined move , it must be a move defined in Moves.xml");
                                                        }
                                                        break;
                                                    case "ATLEVEL":
                                                        int.TryParse(levelUpMoveChildXE.Value, out curMoveAddLevel);
                                                        if (curMoveAddLevel <= 0 || curMoveAddLevel > 100) {
                                                            skipMon = true;
                                                            failedMove = true;
                                                            Debug.Log(curMoveAddLevel + "is not an acceptable move level for " + curMoveAddName + " to be learned by " + curMonEnum + ", it must be greater than 0 and less than 101");
                                                        }
                                                        break;
                                                    default:
                                                        failedMove = true;
                                                        Debug.Log("Invalid value " + levelUpMoveChildXE.Name + " for " + curMonEnum + "'s Moves Learned By Level Up.  Pok�mon will be skipped");
                                                        skipMon = true;
                                                        break;
                                                }
                                            }
                                            if (!failedMove) {
                                                curLevelUpMoves.Add(new LevelUpMoves(curMoveAddLevel, curMoveAddName));
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid field" + levelUpMovesChildXE.Name + " for " + curMonEnum + "'s Moves Learned By Level Up.  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "MOVESLEARNEDBYTEACHING":
                                foreach (XElement teachableMovesChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (teachableMovesChildXE.Name.ToString().ToUpper()) {
                                        case "MOVE":
                                            if (moveNameList.Contains(teachableMovesChildXE.Value.ToUpper())) {
                                                curTeachableMoves.Add(teachableMovesChildXE.Value.ToUpper());
                                            } else {
                                                skipMon = true;
                                                Debug.Log("The move " + teachableMovesChildXE.Value + " is not a defined move , it must be a move defined in Moves.xml");
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid teachable move field" + teachableMovesChildXE.Name + " for " + curMonEnum + "'s Moves Learned By Teaching.  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "MOVESLEARNEDBYBREEDING":
                                foreach (XElement breedableMovesChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (breedableMovesChildXE.Name.ToString().ToUpper()) {
                                        case "MOVE":
                                            if (moveNameList.Contains(breedableMovesChildXE.Value.ToUpper())) {
                                                curEggMoves.Add(breedableMovesChildXE.Value.ToUpper());
                                            } else {
                                                skipMon = true;
                                                Debug.Log("The move " + breedableMovesChildXE.Value + " is not a defined move , it must be a move defined in Moves.xml.  Pok�mon will be skipped");
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid teachable move field" + breedableMovesChildXE.Name + " for " + curMonEnum + "'s Moves Learned By Breeding.  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "EGGTYPE":  //use for both egg types
                                if (System.Enum.IsDefined(typeof(EggGroups), childXe.Value.ToUpper())) {
                                    curEggType1 = childXe.Value.ToUpper();
                                    curEggType2 = curEggType1;
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for EggType , it must be a type defined in the enum EggGroups");
                                }
                                break;
                            case "EGGTYPE1":
                                if (System.Enum.IsDefined(typeof(EggGroups), childXe.Value.ToUpper())) {
                                    curEggType1 = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for EggType1 , it must be a type defined in the enum EggGroups");
                                }
                                break;
                            case "EGGTYPE2":
                                if (System.Enum.IsDefined(typeof(EggGroups), childXe.Value.ToUpper())) {
                                    curEggType2 = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for EggType2 , it must be a type defined in the enum EggGroups");
                                }
                                break;
                            case "STEPSTOHATCH":
                                int.TryParse(childXe.Value, out curStepsToHatch);
                                if (curStepsToHatch <= 0) {
                                    skipMon = true;
                                    Debug.Log(curStepsToHatch + "is not an acceptable value for " + curMonEnum + "'s egg steps, it must be greater than 0");
                                }
                                break;
                            case "HEIGHT":
                                float.TryParse(childXe.Value, out curHeight);
                                if (curHeight <= 0) {
                                    skipMon = true;
                                    Debug.Log(curHeight + "is not an acceptable value for " + curMonEnum + "'s height, it must be greater than 0");
                                }
                                break;
                            case "WEIGHT":
                                float.TryParse(childXe.Value, out curWeight);
                                if (curWeight <= 0) {
                                    skipMon = true;
                                    Debug.Log(curWeight + "is not an acceptable value for " + curMonEnum + "'s weight, it must be greater than 0");
                                }
                                break;
                            case "COLOR":
                                if (System.Enum.IsDefined(typeof(PBColors), childXe.Value.ToUpper())) {
                                    curColor = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Color , it must be a type defined in the enum PBColors");
                                }
                                break;
                            case "HABITAT":
                                if (childXe.Value.Equals("")) {
                                    curHabitat = "NONE";
                                } else {
                                    if (System.Enum.IsDefined(typeof(PokemonHabitats), childXe.Value.ToUpper())) {
                                        curHabitat = childXe.Value.ToUpper();
                                    } else {
                                        skipMon = true;
                                        Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Habitat , it must be a type defined in the enum PokemonHabitats");
                                    }
                                }
                                break;
                            case "KIND":
                                curKind = childXe.Value;
                                break;
                            case "DEXENTRY":
                                curDexEntry = childXe.Value;
                                break;
                            case "NATURALABILITIES":
                                foreach (XElement natAbilitiesChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (natAbilitiesChildXE.Name.ToString().ToUpper()) {
                                        case "ABILITY":
                                            if (abilityNameList.Contains(natAbilitiesChildXE.Value.ToUpper())) {
                                                curNaturalAbilities.Add(natAbilitiesChildXE.Value.ToUpper());
                                            } else {
                                                skipMon = true;
                                                Debug.Log("The move " + natAbilitiesChildXE.Value + " is not a defined ability , it must be a move defined in Abilities.xml.  Pok�mon will be skipped");
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid natural ability field" + natAbilitiesChildXE.Name + " for " + curMonEnum + ".  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "HIDDENABILITIES":
                                foreach (XElement hiddenAbilitiesChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (hiddenAbilitiesChildXE.Name.ToString().ToUpper()) {
                                        case "ABILITY":
                                            if (abilityNameList.Contains(hiddenAbilitiesChildXE.Value.ToUpper())) {
                                                curHiddenAbilities.Add(hiddenAbilitiesChildXE.Value.ToUpper());
                                            } else {
                                                skipMon = true;
                                                Debug.Log("The move " + hiddenAbilitiesChildXE.Value + " is not a defined ability , it must be a move defined in Abilities.xml.  Pok�mon will be skipped");
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid hidden ability field" + hiddenAbilitiesChildXE.Name + " for " + curMonEnum + ".  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "DEXNUMBERS":
                                bool skipDexNum;
                                foreach (XElement dexNumsChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (dexNumsChildXE.Name.ToString().ToUpper()) {
                                        case "DEXNUMBER":
                                            skipDexNum = false;
                                            string curDexNameToAdd = "";
                                            int curDexNumberToAdd = 0;
                                            foreach (XElement dexNumChildXE in dexNumsChildXE.Elements()) {
                                                if (skipMon) {
                                                    break;
                                                }
                                                switch (dexNumChildXE.Name.ToString().ToUpper()) {
                                                case "DEXTYPE":
                                                    curDexNameToAdd = dexNumChildXE.Value.ToUpper();
                                                    break;
                                                case "DEXNUM":
                                                    curDexNumberToAdd = int.Parse(dexNumChildXE.Value);
                                                    break;
                                                }
                                                //ensure dex name is valid
                                                if (tempDex.dexList.FindIndex(d => d.dexInternalName == curDexNameToAdd) < 0) {
                                                    skipDexNum = true;
                                                    Debug.Log("Invalid dex name" + curDexNameToAdd + " for " + curMonEnum + ".  Dex number will be skipped");
                                                } else {
                                                    /*pdate max dex value for later checking
                                                    if (curDexNumberToAdd > maxDexValues[tempDex.dexList.FindIndex(d => d.dexInternalName == curDexNameToAdd)]) {
                                                        maxDexValues[tempDex.dexList.FindIndex(d => d.dexInternalName == curDexNameToAdd)] = curDexNumberToAdd;
                                                    } else if (curDexNumberToAdd == maxDexValues[tempDex.dexList.FindIndex(d => d.dexInternalName == curDexNameToAdd)]) {
                                                        skipDexNum = true;
                                                        Debug.Log("Invalid dex number" + curDexNumberToAdd + " for " + curMonEnum + " in the dex " + curDexNameToAdd + ", already used for another mon.  Dex number will be skipped");
                                                    }*/
                                                }
                                            }
                                            if (!skipDexNum) {
                                                curDexNumbers.Add(curDexNameToAdd, curDexNumberToAdd);
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid field" + dexNumsChildXE.Name + " for " + curMonEnum + "'s dex numbers.  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "EVOLUTIONS":
                                bool skipEvo;
                                string curEvoType;
                                string curEvoTarget;
                                string curEvoRequirement;
                                int tempInt;
                                foreach (XElement evolutionsChildXE in childXe.Elements()) {
                                    if (skipMon) {
                                        break;
                                    }
                                    switch (evolutionsChildXE.Name.ToString().ToUpper()) {
                                        case "EVOLUTION":
                                            skipEvo = false;
                                            curEvoType = "";
                                            curEvoTarget = "";
                                            curEvoRequirement = "";
                                            foreach (XElement evolutionChildXE in evolutionsChildXE.Elements()) {
                                                if (skipMon) {
                                                    break;
                                                }
                                                switch (evolutionChildXE.Name.ToString().ToUpper()) {
                                                    case "EVOTYPE":
                                                        if (System.Enum.IsDefined(typeof(PokemonEvolutionTypes), evolutionChildXE.Value.ToUpper())) {
                                                            curEvoType = evolutionChildXE.Value.ToUpper();
                                                        } else {
                                                            skipEvo = true;
                                                            Debug.Log("The value " + evolutionChildXE.Value.ToString().ToUpper() + " is not accepted as an Evolution Type for " + curMonEnum + ", it must be a type defined in the enum PokemonEvolutionTypes.  Evolution will be skipped");
                                                        }
                                                        break;
                                                    case "EVOLVEINTO":
                                                        if (pokemonNameList.Contains(evolutionChildXE.Value.ToUpper())) {
                                                            curEvoTarget = evolutionChildXE.Value.ToUpper();
                                                        } else {
                                                            skipEvo = true;
                                                            Debug.Log("The move " + evolutionChildXE.Value + " is not a defined Pokemon to evolve into , it must be a Pok�mon defined in Pokemon.xml.  Evolution will be skipped");
                                                        }
                                                        break;
                                                    case "EVOREQUIREMENT":
                                                        switch (curEvoType) {
                                                            case "LEVEL":
                                                            case "LEVELMALE":
                                                            case "LEVELFEMALE":
                                                            case "LEVELDAY":
                                                            case "LEVELNIGHT":
                                                            case "LEVELDARKINPARTY":
                                                            case "LEVELRAIN":
                                                                if (int.TryParse(evolutionChildXE.Value, out tempInt)) {
                                                                    curEvoRequirement = int.TryParse(evolutionChildXE.Value, out tempInt).ToString();
                                                                }
                                                                if (tempInt <= 0 || tempInt > 100) {
                                                                    skipEvo = true;
                                                                    Debug.Log(tempInt + "is not an acceptable level for " + curMonEnum + "'s evolution type, it must be greater than 0 and less than 101");
                                                                }
                                                                break;
                                                            case "ITEM":
                                                            case "TRADEITEM":
                                                            case "ITEMMALE":
                                                            case "ITEMFEMALE":
                                                                if (itemNameList.Contains(evolutionChildXE.Value.ToUpper())) {
                                                                    curEvoRequirement = evolutionChildXE.Value.ToUpper();
                                                                } else {
                                                                    skipEvo = true;
                                                                    Debug.Log("The move " + evolutionChildXE.Value + " is not a defined Item for evo requirement, it must be an Item defined in Items.xml.  Evolution will be skipped");
                                                                }
                                                                break;
                                                            case "HASMOVE":
                                                                if (moveNameList.Contains(evolutionChildXE.Value.ToUpper())) {
                                                                    curEvoRequirement = evolutionChildXE.Value.ToUpper();
                                                                } else {
                                                                    skipEvo = true;
                                                                    Debug.Log("The move " + evolutionChildXE.Value + " is not a defined Move for evo requirement, it must be a Move defined in Moves.xml.  Evolution will be skipped");
                                                                }
                                                                break;
                                                            case "HAPPINESSMOVETYPE":
                                                                if (typeNameList.Contains(evolutionChildXE.Value.ToUpper())) {
                                                                    curEvoRequirement = evolutionChildXE.Value.ToUpper();
                                                                } else {
                                                                    skipEvo = true;
                                                                    Debug.Log("The move " + evolutionChildXE.Value + " is not a defined Type for evo requirement, it must be a Type defined in Types.xml.  Evolution will be skipped");
                                                                }
                                                                break;
                                                            case "TRADESPECIES":
                                                            case "HASINPARTY":
                                                                if (pokemonNameList.Contains(evolutionChildXE.Value.ToUpper())) {
                                                                    curEvoRequirement = evolutionChildXE.Value.ToUpper();
                                                                } else {
                                                                    skipEvo = true;
                                                                    Debug.Log("The Pok�mon " + evolutionChildXE.Value + " is not a defined Pok�mon for evo requirement, it must be a Pok�mon defined in Pok�mon.xml.  Evolution will be skipped");
                                                                }
                                                                break;
                                                            //add location later, not sure how maps are defined yet
                                                            default:

                                                                break;
                                                        }
                                                        break;
                                                    default:
                                                        Debug.Log("Invalid evolution info" + evolutionChildXE.Name + " for " + curMonEnum + ".  Evolution will be skipped");
                                                        skipEvo = true;
                                                        break;
                                                }
                                                if (!skipEvo) {
                                                   curEvolutions.Add(new EvolutionInfo(curEvoTarget, curEvoType, curEvoRequirement));
                                                }
                                            }
                                            break;
                                        default:
                                            Debug.Log("Invalid evolutions field" + evolutionsChildXE.Name + " for " + curMonEnum + ".  Pok�mon will be skipped");
                                            skipMon = true;
                                            break;
                                    }
                                }
                                break;
                            case "BATTLEENEMYYPOS":
                                int.TryParse(childXe.Value, out curBattleEnemyYPos);
                                break;
                            case "BATTLEPLAYERYPOS":
                                int.TryParse(childXe.Value, out curBattlePlayerYPos);
                                break;
                            case "BATTLEALTITUDE":
                                int.TryParse(childXe.Value, out curBattlerAltitude);
                                break;
                            case "WILDITEMCOMMON":
                                if (itemNameList.Contains(childXe.Value.ToUpper())) {
                                    curWildItemCommon = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Wild Item Common , it must be a type defined in Items.xml");
                                }
                                break;
                            case "WILDITEMUNCOMMON":
                                if (itemNameList.Contains(childXe.Value.ToUpper())) {
                                    curWildItemUncommon = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Wild Item Uncommon , it must be a type defined in Items.xml");
                                }
                                break;
                            case "WILDITEMRARE":
                                if (itemNameList.Contains(childXe.Value.ToUpper())) {
                                    curWildItemRare = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Wild Item Rare , it must be a type defined in Items.xml");
                                }
                                break;
                            case "INCENSE":
                                if (itemNameList.Contains(childXe.Value.ToUpper())) {
                                    curIncenseType = childXe.Value.ToUpper();
                                } else {
                                    skipMon = true;
                                    Debug.Log("The value " + childXe.Value.ToString() + " is not accepted for Incense , it must be a type defined in Items.xml");
                                }
                                break;
                            default:
                                Debug.Log("Invalid elecment " + childXe.Name + " for Pok�mon " + curMonEnum + ".  Document compiled, but element is not included");
                                break;
                        }
                    }
                    //invalidate Pok�mon if not containing all required elements
                    if (!skipMon) {
                        if (!curMonEnum.Equals("")) {
                            //invalidate item if not containing all required elements, inform user of each missing field
                            if (curMonName.Equals("")) {
                                Debug.Log("Item " + curMonEnum + " is missing a 'Name' field.");
                                skipMon = true;
                            }
                            if (skipMon) {
                                Debug.Log("One or more fields for " + curMonEnum + " are invalid, skipping item.\n\n");
                            } else {
                                //if all required fields are present, add move
                                SpeciesManager.addSpecies(curMonEnum, curMonName, curMonType1, curMonType2, (GenderRate)Enum.Parse(typeof(GenderRate), curGenderRate),
                                                            (GrowthRate)Enum.Parse(typeof(GrowthRate), curGrowthRate), (EggGroups)Enum.Parse(typeof(EggGroups), curEggType1),
                                                            (EggGroups)Enum.Parse(typeof(EggGroups), curEggType2), (PBColors)Enum.Parse(typeof(PBColors), curColor), curKind,
                                                            (PokemonHabitats)Enum.Parse(typeof(PokemonHabitats), curHabitat), curWildItemCommon, curWildItemUncommon, curWildItemRare,
                                                            curIncenseType, curDexEntry, curMonBaseStats, curEffortPoints, curBaseExp, curRareness, curBaseHappiness, curBattlePlayerYPos,
                                                            curBattleEnemyYPos, curBattlerAltitude, curStepsToHatch, curHeight, curWeight, curLevelUpMoves, curDexNumbers, curTeachableMoves,
                                                            curEggMoves, curNaturalAbilities, curHiddenAbilities, curFormNames, curEvolutions);
                                ++speciesCounter;
                            }
                        } else {
                            //print error report to the user to let know of missing InternalName
                            if (curMonName.Equals("")) {
                                Debug.Log("Pokémon following " + lastValidMon + " is missing an 'InternalName' field.  Item skipped");
                            } else {
                                Debug.Log("Pokémon " + curMonName + " is missing an 'InternalName' field.  Pok�mon skipped");
                            }
                        }
                    } else {
                        if (curMonEnum.Equals("")) {
                            Debug.Log("Pokémon following " + lastValidMon + " is missing one or more fields.  Pok�mon skipped");
                        } else {
                            Debug.Log("Pokémon " + curMonEnum + " is missing on or more fields.  Pok�mon skipped");
                        }
                    }
            }

            } catch (Exception e) {
                Debug.Log(e);
                return;
            }
        }
        //save all data writen to file, the load it back
        if (SpeciesManager.getNumSpecies() > 0) {
            SpeciesManager.saveDataFile();
            SpeciesManager.loadDataFile();
        } else {
            Debug.Log("You have 0 species successfully defined, please check Pokemon.xml to remedy this");
        }
    }

    public static void compileEvents() {

        //variables per event
        string curMapName;
        string curEventID;
        //variables per page
        EventTriggerType curPageTriggerType;
        bool curPageCanPassThrough;
        bool curPageMoveAnimation;
        bool curPageStopAnimation;
        bool curPageDirectionFix;
        bool curPageAlwaysOnTop;
        List<EventCommand> curPageEventCommands;
        List<EventCondition> curPageEventConditions;
        List<EventPage> curEventPageList;
        bool skipEvent;
        //variables per event command
        EventCommandType tempCommandType;
        List<MoveRoute> tempMRouteList;
        List<int> tempIntParams;
        List<float> tempFloatParams;
        List<string> tempStringParams;
        List<PBPokemon> tempPokeParams; 
        List<PBItems> tempItemParams; 
        List<PBMoves> tempMoveParams;
        List<bool> tempBoolParams;    
        int tempInt;
        float tempFloat;
        bool tempBool;   
        //variables per move route
        MoveRouteType tempMoveRouteType;
        int tempMoveRouteInt;
        //variables per event condition
        EventCondition tempCondition;

        List<string> eventIDList = new List<string>();
        List<string> mapNameList = new List<string>();

        //catalog a list of event ids and map names to ensure commands can refer to other events successfully
        try {
            XDocument reader = XDocument.Load("Assets/Resources/Editor/Events.xml");
            reader.StripNamespace();

            //read each ability
            foreach (XElement xe in reader.Descendants("EVENTS")) {
                foreach (XElement childXE in xe.Elements("EVENT")) {
                    if (xe.Element("EVENT") != null) {                      
                        eventNameList.Add(childXE.Element("EVENTIDSTRING").Value);  
                        mapNameList.Add(childXE.Element("MAPNAME").Value);
                    }
                }
            }
        } catch {}

        //clear the event list
        EventManager.clearList();

        try {
            XDocument reader = XDocument.Load("Assets/Resources/Editor/Events.xml");
            reader.StripNamespace();

            //read each ability
            foreach (XElement xe in reader.Descendants("MAP")) {
                curMapName = xe.Element("MAPNAME").Value;
                foreach (XElement eventXe in xe.Elements("EVENT")) {

                    curEventID = "";
                    skipEvent = false;
                    curEventPageList = new List<EventPage>();

                    //read each element in the ability (InternalName, Name, and Description in this case)
                    foreach (XElement childXe in xe.Elements()) {
                        switch (childXe.Name.ToString().ToUpper()) {
                            case "EVENTIDSTRING":
                                curEventID = childXe.Value.ToString();
                                break;
                            case "PAGES":
                                foreach (XElement pagesXe in childXe.Elements("PAGE")){
                                    curPageTriggerType = EventTriggerType.ACTIONBUTTON;
                                    curPageCanPassThrough = false;
                                    curPageMoveAnimation = true;
                                    curPageStopAnimation = false;
                                    curPageDirectionFix = false;
                                    curPageAlwaysOnTop = false;
                                    curPageEventCommands = new List<EventCommand>();
                                    curPageEventConditions = new List<EventCondition>();
                                    foreach (XElement pageXe in pagesXe.Elements()) {
                                        switch (pageXe.Name.ToString().ToUpper()){
                                            case "TRIGGERTYPE":
                                                if (System.Enum.IsDefined(typeof(EventTriggerType), childXe.Value.ToUpper())) {
                                                    curPageTriggerType = (EventTriggerType)Enum.Parse(typeof(EventTriggerType), childXe.Value.ToUpper());
                                                } else {
                                                    skipEvent = true;
                                                    Debug.Log("The value " + childXe.Value.ToString().ToUpper() + " is not accepted as an TriggerType for " + curEventID + ", it must be a type defined in the enum EventTriggerType.  Event will be skipped");
                                                }
                                                break;
                                            case "PAGECONDITIONS":
                                                tempIntParams = new List<int>();
                                                tempFloatParams = new List<float>();
                                                tempStringParams =  new List<string>();
                                                tempPokeParams = new List<PBPokemon>(); 
                                                tempItemParams = new List<PBItems>(); 
                                                tempMoveParams = new List<PBMoves>();
                                                tempBoolParams = new List<bool>();  
                                                tempBool = false;
                                                foreach (XElement conditionXe in pageXe.Elements()) {
                                                    switch (conditionXe.Name.ToString().ToUpper()) {
                                                         case "GLOBALSWITCHES":
                                                            foreach(XElement gSwitchXe in conditionXe.Elements("SWITCHNAME")) {
                                                                foreach (XElement gSwitchValueXe in gSwitchXe.Elements()) {
                                                                    if (bool.TryParse(gSwitchValueXe.Value, out tempBool)) {
                                                                        curPageEventConditions.Add(new EventCondition(EventConditionType.GLOBALSWITCH, 0,0,0,0,gSwitchXe.Value, tempBool, false));
                                                                    } else {
                                                                        Debug.Log("The value " + pageXe.Value + " is not accepted for the switch " + gSwitchXe.Value + " , it must be 'True' or 'False'");
                                                                        skipEvent = true;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                    }
                                                }
                                                break;
                                            case "CANPASSTHROUGH":
                                                if (bool.TryParse(childXe.Value, out curPageCanPassThrough)) {
                                                    curPageCanPassThrough = bool.Parse(pageXe.Value);
                                                } else {
                                                    Debug.Log("The value " + pageXe.Value + " is not accepted for CanPassThrough , it must be 'True' or 'False'");
                                                    skipEvent = true;
                                                }
                                                break;
                                            case "MOVEANIMATION":
                                                if (bool.TryParse(childXe.Value, out curPageMoveAnimation)) {
                                                    curPageMoveAnimation = bool.Parse(pageXe.Value);
                                                } else {
                                                    Debug.Log("The value " + pageXe.Value + " is not accepted for MoveAnimation , it must be 'True' or 'False'");
                                                    skipEvent = true;
                                                }
                                                break;
                                            case "STOPANIMATION":
                                                if (bool.TryParse(childXe.Value, out curPageStopAnimation)) {
                                                    curPageStopAnimation = bool.Parse(pageXe.Value);
                                                } else {
                                                    Debug.Log("The value " + pageXe.Value + " is not accepted for StopAnimation , it must be 'True' or 'False'");
                                                    skipEvent = true;
                                                }
                                                break;
                                            case "DIRECTIONFIX":
                                                if (bool.TryParse(childXe.Value, out curPageDirectionFix)) {
                                                    curPageDirectionFix = bool.Parse(pageXe.Value);
                                                } else {
                                                    Debug.Log("The value " + pageXe.Value + " is not accepted for DirectionFix , it must be 'True' or 'False'");
                                                    skipEvent = true;
                                                }
                                                break;
                                            case "ALWAYSONTOP":
                                                if (bool.TryParse(childXe.Value, out curPageAlwaysOnTop)) {
                                                    curPageAlwaysOnTop = bool.Parse(pageXe.Value);
                                                } else {
                                                    Debug.Log("The value " + pageXe.Value + " is not accepted for DirectionFix , it must be 'True' or 'False'");
                                                    skipEvent = true;
                                                }
                                                break;
                                            case "EVENTCOMMANDS":
                                                foreach (XElement commandListXe in pageXe.Elements("COMMAND")) {
                                                    tempCommandType = EventCommandType.NONE;
                                                    tempMRouteList = new List<MoveRoute>();
                                                    tempMoveRouteType = MoveRouteType.NONE;
                                                    tempMoveRouteInt = 0;
                                                    tempIntParams = new List<int>();
                                                    tempFloatParams = new List<float>();
                                                    tempStringParams =  new List<string>();
                                                    tempPokeParams = new List<PBPokemon>(); 
                                                    tempItemParams = new List<PBItems>(); 
                                                    tempMoveParams = new List<PBMoves>();
                                                    tempBoolParams = new List<bool>();  
                                                    tempInt = 0;
                                                    tempFloat = 0;
                                                    tempBool = false;

                                                    foreach (XElement commandXe in commandListXe.Elements()) {
                                                        switch (commandXe.Name.ToString().ToUpper()) {
                                                            case "COMMANDTYPE":
                                                                if (System.Enum.IsDefined(typeof(EventCommandType), commandXe.Value.ToUpper())) {
                                                                    tempCommandType = (EventCommandType)Enum.Parse(typeof(EventCommandType), commandXe.Value.ToUpper());
                                                                } else {
                                                                    skipEvent = true;
                                                                    Debug.Log("The value " + commandXe.Value.ToString().ToUpper() + " is not accepted as an TriggerType for " + curEventID + ".  Event will be skipped");
                                                                }
                                                                break;
                                                            case "INTEGERS":
                                                                foreach(XElement intXe in commandXe.Elements("INT")) {
                                                                    if (int.TryParse(intXe.Value, out tempInt)) {
                                                                        tempIntParams.Add(int.Parse(intXe.Value));
                                                                    } else {
                                                                        Debug.Log("The value " + intXe.Value + " must be an integer");
                                                                        skipEvent = true;
                                                                    }
                                                                }
                                                                break;
                                                            case "FLOATS":
                                                                foreach(XElement floatXe in commandXe.Elements("FLOAT")) {
                                                                    if (float.TryParse(floatXe.Value, out tempFloat)) {
                                                                        tempFloatParams.Add(float.Parse(floatXe.Value));
                                                                    } else {
                                                                        Debug.Log("The value " + floatXe.Value + " must be a float");
                                                                        skipEvent = true;
                                                                    }
                                                                }
                                                                break;
                                                            case "STRINGS":
                                                                foreach(XElement stringXe in commandXe.Elements("STRING")) {
                                                                    tempStringParams.Add(stringXe.Value.ToString());
                                                                }
                                                                break;
                                                            case "POKES":
                                                                foreach (XElement pokeXe in commandXe.Elements("POKES")) {
                                                                    if (System.Enum.IsDefined(typeof(PBPokemon), pokeXe.Value.ToUpper())) {
                                                                        tempPokeParams.Add((PBPokemon)Enum.Parse(typeof(PBPokemon), pokeXe.Value.ToUpper()));
                                                                    } else {
                                                                        skipEvent = true;
                                                                        Debug.Log("The value " + pokeXe.Value.ToString().ToUpper() + " is not accepted as an PBPokemon for " + curEventID + ".  Event will be skipped");
                                                                    }
                                                                }
                                                                break;
                                                            case "ITEMS":
                                                                foreach (XElement itemXe in commandXe.Elements("ITEM")) {
                                                                    if (System.Enum.IsDefined(typeof(PBItems), itemXe.Value.ToUpper())) {
                                                                        tempItemParams.Add((PBItems)Enum.Parse(typeof(PBItems), itemXe.Value.ToUpper()));
                                                                    } else {
                                                                        skipEvent = true;
                                                                        Debug.Log("The value " + itemXe.Value.ToString().ToUpper() + " is not accepted as an PBItems for " + curEventID + ".  Event will be skipped");
                                                                    }
                                                                }
                                                                break;
                                                            case "MOVES":
                                                                foreach (XElement moveXe in commandXe.Elements("MOVE")) {
                                                                    if (System.Enum.IsDefined(typeof(PBMoves), moveXe.Value.ToUpper())) {
                                                                        tempMoveParams.Add((PBMoves)Enum.Parse(typeof(PBMoves), moveXe.Value.ToUpper()));
                                                                    } else {
                                                                        skipEvent = true;
                                                                        Debug.Log("The value " + moveXe.Value.ToString().ToUpper() + " is not accepted as an PBMoves for " + curEventID + ".  Event will be skipped");
                                                                    }
                                                                }
                                                                break;
                                                            case "MOVEROUTELIST":
                                                                foreach (XElement moveRouteListXe in commandXe.Elements("MOVEROUTE")) {
                                                                    foreach (XElement moveRouteXe in moveRouteListXe.Elements()) {
                                                                        switch (moveRouteXe.Name.ToString().ToUpper()) {
                                                                            case "MOVEROUTETYPE":
                                                                                if (System.Enum.IsDefined(typeof(MoveRouteType), moveRouteXe.Value.ToUpper())) {
                                                                                    tempMoveRouteType = (MoveRouteType)Enum.Parse(typeof(MoveRouteType), moveRouteXe.Value.ToUpper());
                                                                                } else {
                                                                                    skipEvent = true;
                                                                                    Debug.Log("The value " + moveRouteXe.Value.ToString().ToUpper() + " is not accepted as an MoveRouteType for " + curEventID + ".  Event will be skipped");
                                                                                }
                                                                                break;
                                                                            case "NUMBEROFTIMES":
                                                                                if (int.TryParse(moveRouteXe.Value, out tempInt)) {
                                                                                    tempMoveRouteInt = int.Parse(moveRouteXe.Value);
                                                                                } else {
                                                                                    Debug.Log("The value " + moveRouteXe.Value + " must be an integer");
                                                                                    skipEvent = true;
                                                                                }
                                                                                break;
                                                                            default:
                                                                                Debug.Log("Invalid elecment " + moveRouteXe.Name + " for event move route in " + curEventID + ".  Document compiled, but element is not included");
                                                                                skipEvent = true;
                                                                                break;
                                                                        }
                                                                    }
                                                                    tempMRouteList.Add(new MoveRoute(tempMoveRouteType, tempMoveRouteInt));
                                                                }
                                                                break;
                                                            case "BOOLS":
                                                                foreach(XElement boolXe in commandXe.Elements("BOOL")) {
                                                                    if (bool.TryParse(boolXe.Value, out tempBool)) {
                                                                        tempBoolParams.Add(bool.Parse(boolXe.Value));
                                                                    } else {
                                                                        Debug.Log("The value " + boolXe.Value + " must be a boolean");
                                                                        skipEvent = true;
                                                                    }
                                                                }
                                                                break;
                                                            default:
                                                                break;
                                                        }  
                                                    }      
                                                    curPageEventCommands.Add(new EventCommand(tempCommandType, tempMRouteList, tempIntParams, tempFloatParams, tempStringParams,
                                                            tempPokeParams, tempItemParams, tempMoveParams, tempBoolParams));                         
                                                }
                                                break;
                                            default:
                                                Debug.Log("Invalid elecment " + pageXe.Name + " for event " + curEventID + ".  Document compiled, but element is not included");
                                                skipEvent = true;
                                                break;
                                        }
                                        if (skipEvent) {
                                            break;
                                        }                                    
                                    }
                                    if (skipEvent) {
                                        continue;
                                    }
                                    curEventPageList.Add(new EventPage(curPageTriggerType, curPageCanPassThrough, curPageMoveAnimation,
                                            curPageStopAnimation, curPageDirectionFix, curPageAlwaysOnTop, curPageEventCommands, curPageEventConditions));
                                }
                                break;
                            default:
                                Debug.Log("Invalid elecment " + childXe.Name + " for event " + xe.Value + ".  Document compiled, but event is not included");
                                skipEvent = true;
                                break;
                        }
                        if (skipEvent) {
                            continue;
                        }
                    }
                    //add the found ability to AbilityManager, so it can save it.  don't add if invalid
                    if (!curEventID.Equals("")) {
                        EventManager.AddEvent(curMapName, curEventID, curEventPageList);
                    }
                }
            }
            
        } catch {}         

        //save all data writen to file, the load it back
        //save all data writen to file, the load it back
        if (AbilityManager.getAbilities().Count > 0) {
            AbilityManager.saveDataFile();
            AbilityManager.loadDataFile();
        } else {
            Debug.Log("You have 0 events successfully defined, please check Events.xml to remedy this");
        }
    }

}
