﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AudioController : MonoBehaviour {

	//paths to Audio folders folder
	public static string audioSEPath = "Audio/SE/";
	public static string audioBGMPath = "Audio/BGM/";
	public static string audioBGSPath = "Audio/BGS/";
	public static string audioCryPath = "Audio/Cries/";

	//will contain a list of all desired Audio files
    public static AudioClip[] audioSEFiles;
	public static AudioClip[] audioBGMFiles;
    public static AudioClip[] audioBGSFiles;
    public static AudioClip[] audioCryFiles;

    //specify all file types to include in the array here.
    private static string[] extensionsAllowed = { "*.wav", "*.mp3", "*.ogg" };

    //flag used to make createAudioList be called once before playing an audio file the first time
    private static bool listsCreated = false;


	//runs through all specified directories looking for the specified files types
	//serves for quicker checking for existence of audio files
	static void createAudioList() {
		listsCreated = true;
        audioSEFiles = Resources.LoadAll<AudioClip>(audioSEPath);
        audioBGMFiles = Resources.LoadAll<AudioClip>(audioBGMPath);
        audioBGSFiles = Resources.LoadAll<AudioClip>(audioBGSPath);
        audioCryFiles = Resources.LoadAll<AudioClip>(audioCryPath);

    }



	//different types of audio declared seperately so they can be played at the same time
	static AudioSource audioSourceSE;
	static GameObject tempObjectSE;
	public static AudioSource audioSourceBGM;
	static GameObject tempObjectBGM;
	static AudioSource audioSourceBGS;
	static GameObject tempObjectBGS;
	static AudioSource audioSourceCry;
	static GameObject tempObjectCry;
	//coroutine to play audio
	//path expected starting in Resources
	//audioType expected to be "se", "bgm", "bgs", or "cry"
	static IEnumerator playAudio(string filename, string audioType, float volume = 1.0f, bool loop = false, float pitch = 1.0f) {
		if (audioType.ToLower().Equals("se")) {
			tempObjectSE = new GameObject ();
			tempObjectSE.AddComponent<AudioSource> ();
			audioSourceSE  = tempObjectSE.GetComponent<AudioSource> ();
			filename = System.IO.Path.GetFileNameWithoutExtension(filename);
            audioSourceSE.clip = audioSEFiles.Where(clip => clip.name == filename).FirstOrDefault();
            audioSourceSE.volume = volume;
			audioSourceSE.loop = loop;
			audioSourceSE.pitch = pitch;

			audioSourceSE.Play();

		}
		else if (audioType.ToLower().Equals("bgm")) {
			tempObjectBGM = new GameObject ();
			tempObjectBGM.AddComponent<AudioSource> ();
			audioSourceBGM  = tempObjectBGM.GetComponent<AudioSource> ();
			filename = System.IO.Path.GetFileNameWithoutExtension(filename);
			audioSourceBGM.clip = audioBGMFiles.Where(clip => clip.name == filename).FirstOrDefault();
            audioSourceBGM.volume = volume;
			audioSourceBGM.loop = loop;
			audioSourceBGM.pitch = pitch;
			audioSourceBGM.Play();
		}
		else if (audioType.ToLower().Equals("bgs")) {
			tempObjectBGS = new GameObject ();
			tempObjectBGS.AddComponent<AudioSource> ();
			audioSourceBGS  = tempObjectBGS.GetComponent<AudioSource> ();
			filename = System.IO.Path.GetFileNameWithoutExtension(filename);
			audioSourceBGS.clip = audioBGSFiles.Where(clip => clip.name == filename).FirstOrDefault();
            audioSourceBGS.volume = volume;
			audioSourceBGS.loop = loop;
			audioSourceBGS.pitch = pitch;
			audioSourceBGS.Play();
		}
		else if (audioType.ToLower().Equals("cry")) {
			tempObjectCry = new GameObject ();
			tempObjectCry.AddComponent<AudioSource> ();
			audioSourceCry  = tempObjectCry.GetComponent<AudioSource> ();
			filename = System.IO.Path.GetFileNameWithoutExtension(filename);
			audioSourceCry.clip = audioCryFiles.Where(clip => clip.name == filename).FirstOrDefault();
            audioSourceCry.volume = volume;
			audioSourceCry.loop = loop;
			audioSourceCry.pitch = pitch;
			audioSourceCry.Play();
		}
		else {
			Debug.Log("Invalid audio type of" + audioType);
		}
		yield return null;
	}




	public static void playSE(string filename, float volume = 1.0f) {
		if (!listsCreated) {
			AudioController.createAudioList();
		}
		if (audioSourceSE!=null && audioSourceSE.isPlaying) {
			return;
		} else {
			Destroy(audioSourceSE);
			Destroy(tempObjectSE);
		}
		volume = PlayerOptions.SfxVolume/100.0f;
		if (volume<0.0f) {
			Debug.Log("Volume must be greater than or equal to 0.0");
			volume = 0.0f;
		} else if (volume>1.0f) {
			Debug.Log("Volume must be less than or equal to 1.0");
			volume = 1.0f;
		}
		//check for the exact filename (with specified extension)
		if ((audioSEFiles.Where(x => x.name==filename))!=null) {
			StaticCoroutine.DoCoroutine(AudioController.playAudio(filename, "se", volume));
		}
		//if the filename doesn't have an extension, check each extension
		else if (filename == System.IO.Path.GetFileNameWithoutExtension(filename)) {
			foreach (string ext in extensionsAllowed) {
				//Remove function used to remove the * character needed in searching directories
				if ((audioSEFiles.Where(x => x.name==(filename+(ext.Remove(0, 1)))))!=null) {

					StaticCoroutine.DoCoroutine(AudioController.playAudio(filename, "se", volume));
					return;
				}
			}
			Debug.Log("Audio file " + filename + " not found.  Ensure the file exists, and is an accepted file typed.");
		}
		else {
			Debug.Log("Audio file " + filename + " not found.  Ensure you typed the correct extension");
		}

	}

	public static void stopSE(bool fade = true, float time = 0.5f) {
		if (audioSourceSE!=null && audioSourceSE.isPlaying) {
			if (fade) {
				StaticCoroutine.DoCoroutine(AudioController.fadeOutSound(audioSourceSE, time));
			}
		}
	}

	public static void playBGM(string filename, float volume = 1.0f, float pitch = 1.0f) {
		if (!listsCreated) {
			AudioController.createAudioList();
		}
		if (audioSourceBGM!=null && audioSourceBGM.isPlaying) {
			return;
		} else {
			Destroy(audioSourceBGM);
			Destroy(tempObjectBGM);
		}
		volume = PlayerOptions.BgmVolume/100.0f;
		if (volume<0.0f) {
			Debug.Log("Volume must be greater than or equal to 0.0");
			volume = 0.0f;
		} else if (volume>1.0f) {
			Debug.Log("Volume must be less than or equal to 1.0");
			volume = 1.0f;
		}
		//check for the exact filename (with specified extension)
		if ((audioBGMFiles.Where(x => x.name == filename))!= null) {
            StaticCoroutine.DoCoroutine(AudioController.playAudio(filename, "bgm", volume, true, pitch));
		}
		//if the filename doesn't have an extension, check each extension
		else if (filename == System.IO.Path.GetFileNameWithoutExtension(filename)) {
			foreach (string ext in extensionsAllowed) {
				//Remove function used to remove the * character needed in searching directories
				if ((audioBGMFiles.Where(x => x.name == (filename + (ext.Remove(0, 1)))))!= null) {
                    StaticCoroutine.DoCoroutine(AudioController.playAudio(filename, "bgm", volume, true, pitch));
					return;
				}
			}
			Debug.Log("Audio file " + filename + " not found.  Ensure the file exists, and is an accepted file typed.");
		}
		else {
			Debug.Log("Audio file " + filename + " not found.  Ensure you typed the correct extension");
		}

	}

	//Fade out gradually
	public static IEnumerator fadeOutSound(AudioSource audioSource, float FadeTime) {
        float startVolume = audioSource.volume;
 
        while (audioSource.volume > 0) {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
 
            yield return null;
        }
 
        audioSource.Stop();
        audioSource.volume = startVolume;
    }

	//Change volume gradually
	public static IEnumerator changeVolume(AudioSource audioSource, float newVolume, float FadeTime) {
		float startVolume = audioSource.volume;
		bool raiseVolume = newVolume>startVolume;
		while (newVolume!=audioSource.volume) { //raise volume
			audioSource.volume += (newVolume - startVolume) * Time.deltaTime / FadeTime;
			if ((raiseVolume && audioSource.volume>newVolume) || (!raiseVolume && audioSource.volume<newVolume)) {
				audioSource.volume = newVolume;
				break;
			}
			yield return null;
		} 
	}

	public static void changeVolumeBGM(float newVolume, float time = 0.5f) {
		if (audioSourceBGM!=null && audioSourceBGM.isPlaying) {
			StaticCoroutine.DoCoroutine(AudioController.changeVolume(audioSourceBGM, newVolume, time));
			return;
		}
	}

	public static void stopBGM(bool fade = true, float time = 0.5f) {
		if (audioSourceBGM!=null && audioSourceBGM.isPlaying) {
			if (fade) {
				StaticCoroutine.DoCoroutine(AudioController.fadeOutSound(audioSourceBGM, time));
			}
		}
	}


}
