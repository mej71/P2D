﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ItemBag {

    Dictionary<string, Dictionary<PBItems, int>> BagContents;

    public ItemBag() {
        BagContents = new Dictionary<string, Dictionary<PBItems, int>>();
        foreach (string pocket in System.Enum.GetNames(typeof(PBPockets))) {
            BagContents.Add(pocket, new Dictionary<PBItems, int>());
        }

        //if Debug
        AddItem(PBItems.BICYCLE);
        AddItem(PBItems.POTION);
        AddItem(PBItems.HYPERPOTION);
        AddItem(PBItems.REPEL);
        AddItem(PBItems.SUPERREPEL);
        AddItem(PBItems.BLACKFLUTE);
        AddItem(PBItems.SUPERPOTION);
        AddItem(PBItems.MAXREPEL);
        AddItem(PBItems.WHITEFLUTE);
        AddItem(PBItems.HONEY);
        AddItem(PBItems.REDSHARD);
        AddItem(PBItems.YELLOWFLUTE);
        //AddItem(PBItems.YELLOWSHARD, 999);
    }

    public Dictionary<PBItems, int> GetPocket(string pocketName) {
        if (BagContents.ContainsKey(pocketName)) {
            return BagContents[pocketName];
        }
        return null;        
    }

    public Dictionary<PBItems, int> GetPocket(PBPockets pocketName) {
        string pocket = System.Enum.GetName(typeof(PBPockets), pocketName);
        return GetPocket(pocket);
    }

    //Call this when loading a saved game and Items.dat has been updated after the saved game
    //To ensure pockets get reorganized if necessary, and avoid breaking anything
    private void UpdateItemsInPockets() {
        Dictionary<string, Dictionary<PBItems, int>> TempBagContents = new Dictionary<string, Dictionary<PBItems, int>>();
        foreach (string pocket in System.Enum.GetNames(typeof(PBPockets))) {
            TempBagContents.Add(pocket, new Dictionary<PBItems, int>());
        }
        foreach (Dictionary<PBItems, int> value in BagContents.Values) {
            foreach (PBItems itemEnum in value.Keys) {
                TempBagContents[ItemManager.getItems().Find((x => x.itemEnum == itemEnum.ToString())).itemBagPocketType.ToString()].Add(itemEnum, value[itemEnum]);
            }
        }
        BagContents.Clear();
        foreach (string pocket in System.Enum.GetNames(typeof(PBPockets))) {
            BagContents.Add(pocket, new Dictionary<PBItems, int>());
        }
        foreach (string key in TempBagContents.Keys) {
            foreach (PBItems itemEnum in TempBagContents[key].Values) {
                BagContents[key].Add(itemEnum, (TempBagContents[key])[itemEnum]);
            }
        }
    }

    //Check if there are any items in the bag, for use when trying to open the bag
    public bool isEmpty() {
        foreach (string key in BagContents.Keys) {
            if (BagContents[key].Count>0) {
                return false;
            }
        }
        return true;
    }

    //Adds a quantity of an item
    public void AddItem(PBItems itemEnum, int quantity = 1) {
        if (quantity <= 0) { //Can't add negative quantities
            return;
        }
        Dictionary<PBItems, int> bag = BagContents[ItemManager.getItems().Find((x => x.itemEnum == itemEnum.ToString())).itemBagPocketType.ToString()];
        if (bag.ContainsKey(itemEnum)) {
            bag[itemEnum] += quantity;
        } else {
            bag.Add(itemEnum, quantity);
        }
    }

    //QOL function, can use these functions both by enum and name.  Enum is safer, and should always be used
    public void AddItem(string itemName, int quantity = 1) {
        PBItems item = (PBItems)System.Enum.Parse(typeof(PBItems), itemName);
        AddItem(item, quantity);
    }

    //Remove a quantity of an item
    public void RemoveItem(PBItems itemEnum, int quantity = 1) {
        if (quantity <= 0) { //Can't remove negative quantities
            return;
        }
        Dictionary<PBItems, int> bag = BagContents[ItemManager.getItems().Find((x => x.itemEnum == itemEnum.ToString())).itemBagPocketType.ToString()];
        if (bag.ContainsKey(itemEnum)) {
            bag[itemEnum] -= quantity;
            if (bag[itemEnum] <= 0) {
                bag.Remove(itemEnum);
            }
        } //else, item was not found.  Don't throw error?
    }

    public void RemoveItem(string itemName, int quantity = 1) {
        PBItems item = (PBItems)System.Enum.Parse(typeof(PBItems), itemName);
        RemoveItem(item, quantity);
    }

    //Check if item exists in the bag
    public bool hasItem(PBItems itemEnum) {
        return BagContents[ItemManager.getItems().Find((x => x.itemEnum == itemEnum.ToString())).itemBagPocketType.ToString()].ContainsKey(itemEnum);
    }

    public bool hasItem(string itemName) {
        PBItems item = (PBItems)System.Enum.Parse(typeof(PBItems), itemName);
        return hasItem(item);
    }

    public int itemQuantity(PBItems itemEnum) {
        if (!hasItem(itemEnum)) {
            return 0;
        }
        return BagContents[ItemManager.getItems().Find((x => x.itemEnum == itemEnum.ToString())).itemBagPocketType.ToString()][itemEnum];
    }

    public int itemQuantity(string itemName) {
        PBItems item = (PBItems)System.Enum.Parse(typeof(PBItems), itemName);
        return itemQuantity(item);
    }
}

