﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public struct PokemonMove {
    public PBMoves moveID;
    public int pp;
    public int ppUpCount;
    public PokemonMove(PBMoves p_id, int p_pp) {
        moveID = p_id;
        pp = p_pp;
        ppUpCount = 0;
    }
}

public class Pokemon {

    int[] iv; //each separate IV corresponds to HP,ATK,DEF,SPATK,SPDEF,SPD
    int[] ev; //each separate EV corresponds to HP,ATK,DEF,SPATK,SPDEF,SPD
    PBPokemon speciesID; //public int nationaldexNO;
    PokemonData speciesData;
    int personalID; //the ID of the Pokemon itself
    int trainerID; //the ID of the trainer who caught it
    public int level;
    public System.DateTime timeObtained;
    public static int obtainLevel;
    public string obtainMap;
    //pokerus
    public PBItems item;
    public PBItems recyclingItem;
    public PBItems mail;
    public Pokemon fused;
    public string nickname; //the nickname of the Pokemon
    public int exp; //experience of the Pokemon (level can be calculated from this)
    public static GrowthRate growthRate; //how fast the Pokemon levels
    public int happiness; //the happiness of the pokemon (max 255)
    public int eggSteps; //steps to hatch the pokemon - 0 if not an egg
    public List<PokemonMove> moves;
    public PBBalls caughtBall;
    public bool[] markings; //indicates if a pokemon has a marking set (circle, square, triangle, heart, star, diamond)
    public PBObtainType obtainType;
    private int abilityNum;
    private bool hasHiddenAbility;
    private Ability abil;
    private PBTypes type1;
    private PBTypes type2;
    public Gender gender;
    public PBNatures nature;
    private bool shiny;
    public List<PBRibbons> ribbons;
    public PBStatus status;

    const int MAXIVLIMIT = 31; //the maximum value for each individual IV value
    const int MAXHAPPINESS = 255; //max happiness of the Pokemon
    const int EVLIMIT = 510; //max total EVs
    const int EVSTATLIMIT = 252; //max EVs that a single stat can have

    private System.DateTime speciesDT; //Will be used to reduce data lookups, no need to lookup if nothing has changed
    private System.DateTime abilDT;

    public Pokemon(PBPokemon p_ID, int p_level) { //add trainer later
        speciesID = p_ID;
        updateSpecies();
        level = p_level;
        curExp = getExpForLevel(level);
        iv = new int[] { 0, 0, 0, 0, 0, 0 };
        ev = new int[] { 0, 0, 0, 0, 0, 0 };
        stats = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        calcStats();
        hp = TotalHP();
        nickname = "";
        timeObtained = TimeFunctions.getCurrentDate().ToUniversalTime(); //time is in UTC
        personalID =  (int)(UnityEngine.Random.value * 255);
        personalID |= (int)(UnityEngine.Random.value * 255) << 8;
        personalID |= (int)(UnityEngine.Random.value * 255) << 16;
        personalID |= (int)(UnityEngine.Random.value * 255) << 24;
        personalID = Mathf.Abs(personalID);
        for (int i = 0; i <= 5; ++i) {
            iv[i] = (int)(UnityEngine.Random.value * 31);
        }
        nature = (PBNatures)(UnityEngine.Random.Range(0, Enum.GetNames(typeof(PBNatures)).Length)); //Random nature
        happiness = speciesData.monBaseHappiness;
        eggSteps = 0;
        status = PBStatus.NONE;
        markings = new bool[6];
        //give moves
        moves = new List<PokemonMove>();
        generateInitialMoveSet();
        obtainLevel = level;
        obtainType = PBObtainType.MET;
        obtainMap = "Unknown";
        fused = null;
        abilityNum = (int)(UnityEngine.Random.value * speciesData.monNaturalAbilities.Count);
        updateAbility(true);
        hasHiddenAbility = false;
        item = recyclingItem = mail = PBItems.NONE;
        caughtBall = PBBalls.NONE;
    }

    public PBPokemon getSpeciesID() {
        return speciesID;
    }

    public void updateSpecies(bool forceUpdate = false) {
        System.DateTime tempDt = DataFileChecking.UpdatePokemonDataDate();
        if (forceUpdate || speciesDT == null || !speciesDT.Equals(tempDt)) {
            speciesData = SpeciesManager.getSpeciesList().Find((x => x.monEnum == speciesID.ToString()));
            speciesDT = tempDt;
        }
    }

    public PokemonData getSpeciesData() {
        updateSpecies();
        return speciesData;
    }

    public PBNatures getNature() {
        return nature;
    }

    public string getTraitText() {
        int max = iv[0];
        int mInd = 0;
        for (int i = 1; i<6; ++i) {
            if (iv[i]>max) {
                max = iv[i];
                mInd = i;
            } else if (iv[i]==max && i==5 && mInd>2) { 
                max = iv[i]; //IV order was edited for readability.  Speed should supercede spatk/spdef in case of a tie
                mInd = i;
            }
        }
        switch (mInd) {
            case 0: //HP
                return (new string[] { "Loves to eat", "Takes plenty of siestas", "Nods off a lot", "Scatters things often", "Likes to relax"})[max % 5];
            case 1: //Attack
                return (new string[] { "Proud of its power", "Likes to thrash about", "A little quick tempered", "Likes to fight", "Quick tempered" })[max % 5];
            case 2: //Defense
                return (new string[] { "Sturdy body", "Capable of taking hits", "Highly persistent", "Good endurance", "Good perseverance" })[max % 5];
            case 3: //Spatk
                return (new string[] { "Highly curious", "Mischievous", "Thoroughly cunning", "Often lost in thought", "Very finicky" })[max % 5];
            case 4: //Spdef
                return (new string[] { "Strong willed", "Somewhat vain", "Strongly defiant", "Hates to lose", "Somewhat stubborn" })[max % 5];
            default: //SPeed
                return (new string[] {"Likes to run", "Alert to sounds", "Impetuous and silly", "Somewhat of a clown", "Quick to flee" })[max % 5];
        }
    }

    private void generateInitialMoveSet() {
        List<LevelUpMoves> tempMoveList = SpeciesManager.getSpeciesList().Find((x => x.monEnum == speciesID.ToString())).monLevelUpMoves;
        int pos = 0;
        foreach (LevelUpMoves move in tempMoveList) {
            if (move.moveLevel<=level) {
                if (pos>3) {
                    pos = 0;
                }
                if (moves.Count>pos) {
                    moves[pos] = new PokemonMove((PBMoves)System.Enum.Parse(typeof(PBMoves), move.moveName), MoveManager.moveList.Find(x => x.internalName.ToString() == move.moveName).basePP);
                } else {
                    moves.Add(new PokemonMove((PBMoves)System.Enum.Parse(typeof(PBMoves), move.moveName), MoveManager.moveList.Find(x => x.internalName.ToString() == move.moveName).basePP));
                }
                ++pos;
            }
        }
    }

    public Ability updateAbility(bool forceUpdate = false) {
        System.DateTime tempSpeciesDt = DataFileChecking.UpdatePokemonDataDate();
        System.DateTime tempAbilDt = DataFileChecking.UpdateAbilityDataDate();
        if (forceUpdate || speciesDT == null || abilDT==null || !speciesDT.Equals(tempSpeciesDt) || !abilDT.Equals(tempAbilDt)) {
            List<string> abilities;
            updateSpecies();
            if (hasHiddenAbility) {
                abilities = speciesData.monHiddenAbilities;
                if (abilities.Count == 0) {  //Allow hidden flag to remain without throwing an error for no hidden abilities defined
                                             //Just return a natural ability
                    abilities = speciesData.monNaturalAbilities;
                }
            } else {
                abilities = speciesData.monNaturalAbilities;
            }
            if (abilityNum >= abilities.Count) {
                abilityNum = 0;
            }
            abil = AbilityManager.getAbilities().Find(x => x.internalName == ((PBAbilities)System.Enum.Parse(typeof(PBAbilities), abilities[abilityNum])).ToString());
            speciesDT = tempSpeciesDt;
            abilDT = tempAbilDt;
        }
        return abil;
    }

    public string getAbilityDesc() {
        updateAbility();
        return abil.description;
    }

    public string getAbilityName() {
        updateAbility();
        return abil.name;
    }

    public string getType1() {
        updateSpecies();
        return speciesData.monType1;
    }

    public string getType2() {
        updateSpecies();
        return speciesData.monType2;
    }

    public int[] getIvs() {
        return iv;
    }

    public int[] getEvs() {
        return ev;
    }

    public void setNickname(string nickname) {
        this.nickname = nickname; 
    }

    public string name() {
        if (nickname.Equals("")) {
            updateSpecies();
            return speciesData.monName;
        } else {
            return nickname;
        }
    }

    public int getTrainerID() {
        return trainerID;
    }

    public int getMonID() {
        return personalID;
    }

    public int getLevel() {
        return level;
    }

    private int curExp;
    public static int getExpForLevel(int toLevel) {
        switch (Pokemon.growthRate) { 
            case GrowthRate.FAST: {
                return (int)((4/5.0) * Mathf.Pow(toLevel, 3));
            }
            case GrowthRate.MEDIUM: { // medium fast
                return (int)Mathf.Pow(toLevel, 3);
             }
             case GrowthRate.SLOW: { 
                return (int)((5/4.0) * Mathf.Pow(toLevel, 3));
            }
            case GrowthRate.PARABOLIC: {//medium slow
                return (int)((6 / 5.0) * Mathf.Pow(toLevel, 3) - 15 * Mathf.Pow(toLevel, 2) + 100 * toLevel - 140);
            }
            case GrowthRate.ERRATIC: { 
                if (toLevel <= 50) {
                    return (int)(Mathf.Pow(toLevel, 3)*(100-toLevel)/50);
                } else if (toLevel <=68) {
                    return (int)(Mathf.Pow(toLevel, 3)*(150-toLevel)/100);
                } else if (toLevel <= 68) {
                    return (int)(Mathf.Pow(toLevel, 3)*(1911-(10*toLevel))/3);
                } else {
                    return (int)(Mathf.Pow(toLevel, 3)*(160-toLevel)/100);
                }
            }
            case GrowthRate.FLUCTUATING: { 
                if (toLevel <= 15) {
                    return (int)(Mathf.Pow(toLevel, 3) * ((toLevel+1)/3.0 +24) / 50);
                } else if (toLevel <= 36) {
                    return (int)(Mathf.Pow(toLevel, 3) * (toLevel+14)/50);
                } else {
                    return (int)(Mathf.Pow(toLevel, 3) * (toLevel/2.0 + 32) / 50);
                }
            }
        }
        return 1;
    }

    private int[] stats;
    public int TotalHP(){ return stats[0]; }
    public int Attack() {return stats[1];}
    public int Defense(){ return stats[2]; }
    public int SpAtk()  { return stats[3]; }
    public int SpDef()  { return stats[4]; }
    public int Speed()  { return stats[5]; }
    public int hp;

    //Recalculates stats (called whenever level of pokemon is changed)
    public void calcStats() {
        int[] pvalues = new int[] { 100, 100, 100, 100, 100 };
        updateSpecies();
        int[] baseStats = speciesData.monBaseStats;
        int nd5 = (int)(((int)nature+1) / 5);
        int nm5 = (int)(((int)nature+1) % 5);
        int curTotalHp = TotalHP();
        if (nd5 != nm5) {
            pvalues[nd5] = 110;
            pvalues[nm5] = 90;
        }
        //calculate hp
        if (baseStats[0] == 1) {
            stats[0] = 1;
        } else {
            stats[0] = (((baseStats[0]*2 + iv[0]+(ev[0]>>2)*level/100) +level + 10));
        }
        //calculate all other stats
        for (int i = 1; i<=5; ++i) {
            stats[i] = (int)(( ((int)(baseStats[i]*2 + iv[i] + (ev[i] >> 2) * level / 100))+ 5) * pvalues[i-1]/100);
        }
        hp = stats[0] - (curTotalHp - stats[0]);
        if (hp <= 0) { hp = 1; }
    }
}
