using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventPage{

    public EventTriggerType TriggerType;
    public bool CanPassThrough = false;
    public bool MoveAnimation = true;
    public bool StopAnimation = false;
    public bool DirectionFix = false;
    public bool AlwaysOnTop = false;
    //add list of game switches to check when we make them
    public List<EventCommand> EventCommands = new List<EventCommand>();
    public List<EventCondition> EventConditions = new List<EventCondition>();
    public string SpritePath = "";
    public bool IsBeingRun = false;
    public EventWrapper ParentWrapper;
    private AnimatedTextureExtendedUV _animatedTexture;
    private MapPositionWatcher _positionWatcher;
    private Rigidbody2D _rgdBody;
    private SpriteRenderer _spRend;
    int[] dirCoords = new int[]{0,1,3,2};
    Vector3[] movingVectors;
    private float _speed = 1.0f;                   	// Speed of movement
	private float _distance = 0.32f;
    private Transform _transform;

    public EventPage(EventTriggerType eventTrigger, bool canPass, bool moveAnim, bool stopAnim, bool dirFix, bool alwaysTop,
        List<EventCommand> ecommList, List<EventCondition> econList) {
            TriggerType = eventTrigger;
            CanPassThrough = canPass;
            MoveAnimation = moveAnim;
            StopAnimation = stopAnim;
            DirectionFix = dirFix;
            AlwaysOnTop = alwaysTop;
            EventCommands = ecommList;
            EventConditions = econList;
    }

    //Check if all conditions are satisfied
    public bool PassesConditions() {
        foreach (EventCondition e in EventConditions) {
            switch(e.Condition) {
                case EventConditionType.SELFSWITCH:
                    if (!CheckSelfSwitch(e.StringParam, e.BoolParamOne)) {
                        return false;
                    }
                    break;
                case EventConditionType.TIMEAFTER:
                    if (TimeFunctions.getHour() < e.IntParamOne || (TimeFunctions.getHour() == e.IntParamOne && TimeFunctions.getHour() < e.IntParamTwo)) {
                        return false;
                    }
                    break;
                case EventConditionType.TIMEBEFORE:
                    if (TimeFunctions.getHour() > e.IntParamOne || (TimeFunctions.getHour() == e.IntParamOne && TimeFunctions.getHour() > e.IntParamTwo)) {
                        return false;
                    }
                    break;
                case EventConditionType.TIMEBETWEEN:
                    if (TimeFunctions.getHour() < e.IntParamOne || (TimeFunctions.getHour() == e.IntParamOne && TimeFunctions.getHour() < e.IntParamTwo)) {
                        return false;
                    } else if (TimeFunctions.getHour() > e.IntParamThree || (TimeFunctions.getHour() == e.IntParamThree && TimeFunctions.getHour() > e.IntParamFour)) {
                        return false;
                    }
                    break;
                case EventConditionType.GLOBALSWITCH:
                    if (!GameSwitchManager.checkSwitch(e.StringParam)) {
                        return false;
                    }
                    break;
                case EventConditionType.GLOBALVARIABLE:
                    switch (e.GameVariableTypeChoice) {
                        case GameVariableTypeChoice.INT:
                            if (!GameVariablesManager.IsEquivalent(e.StringParam, e.IntParamOne)) {
                                return false;
                            }
                            break;
                        case GameVariableTypeChoice.FLOAT:
                            if (!GameVariablesManager.IsEquivalent(e.StringParam, e.FloatParamOne)) {
                                return false;
                            }
                            break;
                        case GameVariableTypeChoice.STRING:
                            if (!GameVariablesManager.IsEquivalent(e.StringParam, e.StringParam)) {
                                return false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        return true;
    }

    public bool CheckSelfSwitch(string switchName, bool checkForTrue = true) {
        if (ParentWrapper.SelfSwitches.ContainsKey(switchName)) {
            return ParentWrapper.SelfSwitches[switchName] == checkForTrue;
        } else {
            ParentWrapper.SelfSwitches.Add(switchName, false);
            return false;
        }
    }

    //Runs event commands
    public IEnumerator TriggerEvent() {
        IsBeingRun = true;
        if (_animatedTexture==null) {
            SetFields();
        }
        foreach (EventCommand ec in EventCommands) {
            switch (ec.CommandType) {
                case EventCommandType.DISPLAYTEXT:
                    UIManager.displayText(ec.StringParams[0]); 
                    yield return new WaitUntil(() => UIManager.isTextDisplaying() == true);      
                    break;
                case EventCommandType.SETSELFSWITCH:
                    ParentWrapper.SelfSwitches[ec.StringParams[0]] = ec.BoolParams[0];
                    break; 
                case EventCommandType.EXECUTEMOVEROUTE:
                    foreach(MoveRoute mr in ec.MRouteList) {
                        yield return new WaitUntil(() => _animatedTexture.isMoving==false);
                        switch (mr.RouteType) {
                            case MoveRouteType.NONE:
                                break;
                            case MoveRouteType.TURNDOWN:
                                _animatedTexture.setFacing(0);
                                yield return new WaitForSeconds(0.5f);
                                break;
                            case MoveRouteType.TURNUP:
                                _animatedTexture.setFacing(3);
                                yield return new WaitForSeconds(0.5f);
                                break;
                            case MoveRouteType.TURNLEFT:
                                _animatedTexture.setFacing(1);
                                yield return new WaitForSeconds(0.5f);
                                break;
                            case MoveRouteType.TURNRIGHT:
                                _animatedTexture.setFacing(2);
                                yield return new WaitForSeconds(0.5f);
                                break;
                            case MoveRouteType.TURNAROUND:
                                for (int i = 0; i < mr.NumTimes; ++i) {
                                    for (int j = 0; j < 4; ++j) {                                    
                                        if (dirCoords[j]==_animatedTexture.vIndex) {
                                            _animatedTexture.setFacing(dirCoords[(j+2)%4]);
                                            break;
                                        }  
                                    }
                                    yield return new WaitForSeconds(0.5f);
                                }
                                break;
                            case MoveRouteType.TURN90CLOCKWISE:
                                for (int i = 0; i < mr.NumTimes; ++i) {
                                    for (int j = 0; j < 4; ++j) {                                    
                                        if (dirCoords[j]==_animatedTexture.vIndex) {
                                            if (j!=3) {
                                                _animatedTexture.setFacing(dirCoords[j+1]);
                                            } else {
                                                _animatedTexture.setFacing(dirCoords[0]);
                                            }
                                            break;
                                        }  
                                    }
                                    yield return new WaitForSeconds(0.5f);
                                }
                                break;
                            case MoveRouteType.TURN90COUNTERCLOCKWISE:
                                for (int i = 0; i < mr.NumTimes; ++i) {
                                     for (int j = 0; j < 4; ++j) {                                    
                                        if (dirCoords[j]==_animatedTexture.vIndex) {
                                            if (j!=0) {
                                                _animatedTexture.setFacing(dirCoords[j-1]);
                                            } else {
                                                _animatedTexture.setFacing(dirCoords[3]);
                                            }
                                            break;
                                        }  
                                    }
                                    yield return new WaitForSeconds(0.5f);
                                }
                                break;
                            case MoveRouteType.MOVEDOWN:
                                ParentWrapper.StartCoroutine(StartMoveTo(mr.NumTimes, 0));
                                break;
                            case MoveRouteType.MOVEUP:
                                ParentWrapper.StartCoroutine(StartMoveTo(mr.NumTimes, 3));
                                break;
                            case MoveRouteType.MOVERIGHT:
                                ParentWrapper.StartCoroutine(StartMoveTo(mr.NumTimes, 1));
                                break;
                            case MoveRouteType.MOVELEFT:
                                ParentWrapper.StartCoroutine(StartMoveTo(mr.NumTimes, 2));
                                break;
                        }                        
                    }
                    break;
                case EventCommandType.WAITFORSECONDS:
                    yield return new WaitForSeconds(ec.FloatParams[0]);
                    break;
                default:
                    break;
            }
        }
        IsBeingRun = false;
        ParentWrapper.UpdatePageSelection();
        yield break;
    }


    public IEnumerator StartMoveTo(int numTimes, int direction) {
        _animatedTexture.isMoving = true;
        _animatedTexture.setFacing(direction);
        Vector3 destinationPos;
        for (int i = 0; i<numTimes; ++i) {
            destinationPos = _transform.position+movingVectors[direction];
            if (PassabilityCheck.canPass(_rgdBody, movingVectors[direction], _distance)) {
                ParentWrapper.StartCoroutine(_animatedTexture.UpdateSpriteAnimation());
                while(Vector3.Distance(_transform.position, destinationPos) != 0.0f) {
                    _transform.position = Vector3.MoveTowards(
                    _transform.position, destinationPos, _speed * Time.deltaTime);
                    yield return null;
                }
            } else {
                break;
            }
        }                   
        _positionWatcher.updatePosition();
        _spRend.sortingOrder = Mathf.Abs((int)( (_transform.position.y - _positionWatcher.highestYValue) / 0.32f));
        yield return new WaitForSeconds(0.5f);
        _animatedTexture.isMoving = false;
        yield break;
    }

    public void SetFields() {
        movingVectors = new Vector3[4] {
			new Vector3(0.0f, -1*_distance, 0.0f),
			new Vector3(-1*_distance, 0.0f, 0.0f),
			new Vector3(_distance, 0.0f, 0.0f),
			new Vector3(0.0f, _distance, 0.0f)
		};
        _transform = ParentWrapper.gameObject.transform;
        _animatedTexture = ParentWrapper.gameObject.GetComponent<AnimatedTextureExtendedUV>();
        _positionWatcher = ParentWrapper.gameObject.GetComponent<MapPositionWatcher>();
        _rgdBody = ParentWrapper.gameObject.GetComponent<Rigidbody2D>();
        _spRend = ParentWrapper.gameObject.GetComponent<SpriteRenderer>();
    }

}
