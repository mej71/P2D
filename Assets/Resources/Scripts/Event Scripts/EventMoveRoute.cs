public enum MoveRouteType {
    NONE,
    MOVELEFT,
    MOVERIGHT,
    MOVEUP,
    MOVEDOWN,
    JUMPINPLACE,
    JUMPLEFT,
    JUMPRIGHT,
    JUMPUP,
    JUMPDOWN,
    MOVETOWARDPLAYER,
    MOVEAWAYFROMPLAYER,
    TURNRIGHT,
    TURNLEFT,
    TURNUP,
    TURNDOWN, //for what?
    TURNAROUND,
    TURN90COUNTERCLOCKWISE,
    TURN90CLOCKWISE,
    SETRUNON, //Only usable with characters that have running sprites
    SETRUNOFF, //Only usable with characters that have running sprites
}

public class MoveRoute {
    public MoveRouteType RouteType;
    public int NumTimes;  //Can set a number of times to avoid repeating move routes

    public MoveRoute(MoveRouteType mrt, int n) {
        RouteType = mrt;
        NumTimes = n;
    }

}