public enum EventTriggerType {
    ACTIONBUTTON,
    PLAYERTOUCH,
    EVENTTOUCH,
    AUTORUN
}

//Used as conditionals for pages, and for if/else event commands
public enum EventConditionType {
    GLOBALSWITCH,
    GLOBALVARIABLE,
    SELFSWITCH,
    TIMEBEFORE,
    TIMEAFTER,
    TIMEBETWEEN,
    HASPOKEMON,
    HASITEM,
    HASBADGE,
    PARTYKNOWSMOVE,
    SCRIPT,
    NONE
}

//Holds info for each condition
public class EventCondition {
    public EventConditionType Condition;
    public int IntParamOne, IntParamTwo, IntParamThree, IntParamFour;
    public string StringParam;
    public bool BoolParamOne, BoolParamTwo;
    public GameVariableTypeChoice GameVariableTypeChoice;
    public float FloatParamOne, FloatParamTwo;
    public EventCondition(EventConditionType c, int i1, int i2, int i3, int i4, string s, bool b1, 
            bool b2 = true, GameVariableTypeChoice gvt = GameVariableTypeChoice.STRING, float f1 = 0, float f2 = 0) {
        Condition = c;
        IntParamOne = i1;
        IntParamTwo = i2;
        IntParamThree = i3;
        IntParamFour = i4;
        StringParam = s;
        BoolParamOne = b1;
        BoolParamTwo = b2;
        GameVariableTypeChoice = gvt;
        FloatParamOne = f1;
        FloatParamTwo = f2;
    }

}