﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




//Holds the event pages
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif

public class EventWrapper : UniqueID {

    [HideInInspector]
    public MapPositionWatcher PositionWatcher;
    [HideInInspector]
    public SpriteRenderer SpRend;
    [HideInInspector]
    public List<EventPage> Pages = new List<EventPage>(); //ensure at least one page is created by default
    [HideInInspector]
    public Dictionary<string, bool> SelfSwitches = new Dictionary<string, bool>();
    public int _curPageInt;
    private int _oldPageInt;

	// Use this for initialization
	void Start () {
        _curPageInt = 0;
        _oldPageInt = -1;
        PositionWatcher = GetComponent<MapPositionWatcher>();
        if (PositionWatcher==null) {
            PositionWatcher = gameObject.AddComponent<MapPositionWatcher>();
        }
        SpRend = gameObject.GetComponent<SpriteRenderer>();
        if (SpRend == null) {
            SpRend = gameObject.AddComponent<SpriteRenderer>();
        }
        SpRend.sortingOrder = Mathf.Abs((int)((gameObject.transform.position.y - PositionWatcher.highestYValue) / 0.32f));

        //Load pages from event manager
        Pages = EventManager.GetEventPages(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, uniqueId);

        //Load self switches from game file?

        UpdatePageSelection();
        //update page selection every minute
        InvokeRepeating("UpdatePageSelection", 60.0f, 60.0f);
    }

    //Choose starting page (pages are check in reverse order, i.e. last page that meets conditions should be selected)
    public void UpdatePageSelection() {
        if (Pages[_curPageInt].IsBeingRun) {
            return;
        }
        for (int i = Pages.Count-1; i >= 0; --i) {   
            if (Pages[i].PassesConditions()) {
                _curPageInt = i;
                if (_oldPageInt==_curPageInt) {
                    break;
                }
                _oldPageInt = _curPageInt;
                //Update the sprite for the event
                if (Pages[i].SpritePath!="") {
                    SpRend.sprite = Resources.Load("Graphics/Characters/" + Pages[i].SpritePath, typeof(Sprite)) as Sprite;
                    SpRend.enabled = true;
                    gameObject.GetComponent<AnimatedTextureExtendedUV>().changeSpriteSheetName(Pages[i].SpritePath);
                } else {
                    SpRend.enabled = false;
                }
                break;
            }   
        }
    }

    //Not sure about the structure of this class
    //Event pages themselves should possibly have the update method, and this container should merely exhange the currently playing one?
    protected override void Update() {
        if (Application.isPlaying) {
            return;
        }
        #if UNITY_EDITOR
            //Don't call base update while playing
            base.Update();
        #endif
    }

    public EventPage GetCurrentPage() {
        return Pages[_curPageInt];
    }

    public void TriggerEventPage() {
        AudioController.stopSE(true, 0.25f);  //Stop sound effects
        StartCoroutine(Pages[_curPageInt].TriggerEvent());
    }

}
