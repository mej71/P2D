using System.Collections.Generic;
using UnityEngine;

public enum EventCommandType {
    NONE,
    [System.ComponentModel.Description("Display Text")]
    DISPLAYTEXT,
    [System.ComponentModel.Description("Display Choices")]
    DISPLAYCHOICES,
    [System.ComponentModel.Description("Get Number Input")]
    GETNUMBERINPUT,
    [System.ComponentModel.Description("Get Text Input")]
    GETTEXTINPUT,
    [System.ComponentModel.Description("Wait For Seconds")]
    WAITFORSECONDS,
    [System.ComponentModel.Description("Transfer Player")]
    TRANSFERPLAYER,
    [System.ComponentModel.Description("Move Event")]
    MOVEEVENT,
    [System.ComponentModel.Description("Move Camera")]
    MOVECAMERA,
    [System.ComponentModel.Description("Change Map Metadata")]
    CHANGEMAPMETADATA,
    [System.ComponentModel.Description("Select Pokémon From Party")]
    SELECTPOKEMONFROMPARTY,
    [System.ComponentModel.Description("Select Item From Bag")]
    SELECTITEMFROMBAG,
    [System.ComponentModel.Description("Display Animation Above Event")]
    DISPLAYANIMATIONABOVEEVENT,
    [System.ComponentModel.Description("Change Screen Tone")]
    CHANGESCREENTONE,
    [System.ComponentModel.Description("Screen Flash")]
    SCREENFLASH,
    [System.ComponentModel.Description("Screen Shake")]
    SCREENSHAKE,
    [System.ComponentModel.Description("Show Picture")]
    SHOWPICTURE,
    [System.ComponentModel.Description("Move Picture")]
    MOVEPICTURE,
    [System.ComponentModel.Description("Remove Picture")]
    REMOVEPICTURE,
    [System.ComponentModel.Description("Play BGM")]
    PLAYBGM,
    [System.ComponentModel.Description("Stop BGM")]
    STOPBGM,
    [System.ComponentModel.Description("Play BGS")]
    PLAYBGS,
    [System.ComponentModel.Description("Stop BGS")]
    STOPBGS,
    [System.ComponentModel.Description("Remember BGM/BGS")]
    REMEMBERBGM,
    [System.ComponentModel.Description("Restore BGM/BGS")]
    RESTOREBGM,
    [System.ComponentModel.Description("Play ME")]
    PLAYME,
    [System.ComponentModel.Description("Stop ME")]
    STOPME,
    [System.ComponentModel.Description("Play SE")]
    PLAYSE,
    [System.ComponentModel.Description("Stop SE")]
    STOPSE,
    [System.ComponentModel.Description("Execute Move Route")]
    EXECUTEMOVEROUTE,
    [System.ComponentModel.Description("Wild Battle")]
    WILDBATTLE,
    [System.ComponentModel.Description("Wild Double Battle")]
    WILDDOUBLEBATTLE,
    [System.ComponentModel.Description("Wild Natural Encounter")]
    WILDNATURALENCOUNTER,
    [System.ComponentModel.Description("Trainer Battle")]
    TRAINERBATTLE,
    [System.ComponentModel.Description("Trainer Double Battle")]
    TRAINERDOUBLEBATTLE,
    [System.ComponentModel.Description("Teach Move To Selected Pokémon")]
    TEACHMOVETOSELECTEDPOKEMON,
    [System.ComponentModel.Description("Give Pokémon")]
    GIVEPOKEMON,
    [System.ComponentModel.Description("Remove Pokémon")]
    REMOVEPOKEMON,
    [System.ComponentModel.Description("Give Item(s)")]
    GIVEITEM,
    [System.ComponentModel.Description("Set Global Switch")]
    SETGLOBALSWITCH,
    [System.ComponentModel.Description("Set Global Variable")]
    SETVARIABLE,
    [System.ComponentModel.Description("Set Self Switch")]
    SETSELFSWITCH,
    [System.ComponentModel.Description("Run Script")]
    RUNSCRIPT
}


//Every event command has a type and optional parameters that are filled out, and used based on command type
public class EventCommand {
    public EventCommandType CommandType;
    public List<MoveRoute> MRouteList;
    public List<int> IntParams;
    public List<float> FloatParams;
    public List<string> StringParams;
    public List<PBPokemon> PokeParams;
    public List<PBItems> ItemParams;
    public List<PBMoves> MoveParams;
    //add trainer, for battling parameters
    //add store-to switch and variable, so inputs store to a gloabal switch or variable
    public List<bool> BoolParams;
    
    public EventCommand(EventCommandType ect, System.Collections.Generic.List<MoveRoute> mr, List<int> ints, 
        List<float> floats, List<string> strings, List<PBPokemon> pokes, List<PBItems> items, 
        List<PBMoves> moves, List<bool> bools) {
        CommandType = ect;
        IntParams = ints;
        MRouteList = mr;
        FloatParams = floats;
        StringParams = strings;
        PokeParams = pokes;
        ItemParams = items;
        MoveParams = moves;
        BoolParams = bools;
    }
}