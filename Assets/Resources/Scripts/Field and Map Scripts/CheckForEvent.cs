using UnityEngine;
using System.Collections;
public class CheckForEvents : MonoBehaviour {

    public static EventWrapper GetEventInFront(Rigidbody2D player, Vector2 target, float distance) {
		BoxCollider2D playerCollider = player.GetComponent<BoxCollider2D>();
		Vector2 playerColliderVector = new Vector2();
		playerColliderVector.Set(  player.transform.position.x+playerCollider.offset.x, player.transform.position.y+playerCollider.offset.y );
		RaycastHit2D hit = Physics2D.Raycast(playerColliderVector, target);
        if (hit.distance <= distance) {
            if (hit.rigidbody != null) {
                EventWrapper ew = hit.rigidbody.gameObject.GetComponent<EventWrapper>();
                if (ew != null) {
                    return ew;
                }
            }
        }
		return null;
    }

    public static void TriggerEventInFrontActionButton(Rigidbody2D player, Vector2 target, float distance) {
        EventWrapper ew = GetEventInFront(player, target, distance);
        if (ew != null && ew.GetCurrentPage().TriggerType==EventTriggerType.ACTIONBUTTON) {
            ew.TriggerEventPage();
        }
    }

}