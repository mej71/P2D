﻿using UnityEngine;
using System.Collections;
public class DayNightSpriteShader : MonoBehaviour {

	private MapInfo curMapName;

	private void Awake() { 
		// If supported in the shader set our opacity
		// (Keep opacity at 1.0 to avoid copying the material)
		curMapName = CheckCurrentMetadata.getCurMap();
        this.gameObject.GetComponent<SpriteRenderer>().material.SetColor("_Color", DayNightShading.getCurrentShading());
		InvokeRepeating("updateShading", 0.0f, 60.0f);
	}

	private void updateShading() {
        this.gameObject.GetComponent<SpriteRenderer>().material.SetColor("_Color", DayNightShading.getCurrentShading());
	}

	private void Update() {
		if (curMapName != CheckCurrentMetadata.getCurMap()) {
			curMapName = CheckCurrentMetadata.getCurMap();
			updateShading();
		}
	}
}
