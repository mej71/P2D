﻿using UnityEngine;
using System.Collections;
public class AnimatedTextureExtendedUV : MonoBehaviour
{
	private int colCount = 4;
	private string sheetname;
    private string oldSheetname;
    private Sprite[] sprites;
    private string[] names;
	private SpriteRenderer spRend;
	public int hIndex = 0;
	public int vIndex = 0;
	public bool isGamePlayer = false;
    private float _updateSpeed = 8f;
	public bool isMoving;

	void Start() {
		isMoving = false;
        spRend = GetComponent<SpriteRenderer>();
        //spTransform = GetComponent<Transform>();
        oldSheetname = spRend.sprite.name.Remove(spRend.sprite.name.LastIndexOf('_'));
        updateSpritesheetSet();
    }

    void updateSpritesheetSet() {
        sprites = Resources.LoadAll<Sprite>("Graphics/Characters/" + sheetname);
        names = new string[sprites.Length];
        for (int i = 0; i < names.Length; i++) {
            names[i] = sprites[i].name;
        }
    }

    public void addToSpriteSheetName(string newSpriteSheetEnd, float newupdateSpeed = 8f) {
        sheetname = oldSheetname + newSpriteSheetEnd;
        updateSpritesheetSet();
        _updateSpeed = newupdateSpeed;
    }

	public void changeSpriteSheetName(string newSpriteSheetName, float newupdateSpeed = 8f) {
		sheetname = newSpriteSheetName;
        updateSpritesheetSet();
        _updateSpeed = newupdateSpeed;
	}

    public void revertSpritesheet() {
        sheetname = oldSheetname;
        updateSpritesheetSet();
        _updateSpeed = 8f;
    }

	//SetSpriteAnimation
	public IEnumerator UpdateSpriteAnimation() {
		// split into horizontal and vertical index
		for (int i = 0; i<2; i++) {
			hIndex += 1;
			if (hIndex>3) {
				hIndex = 0;
			}
			ChangeSprite(colCount * vIndex + hIndex);
			yield return new WaitForSeconds(1f/_updateSpeed);
		}
		ChangeSprite(4*vIndex + hIndex);
	}

	void ChangeSprite( int index ) {
        spRend.sprite = sprites[index];
    }

    void ChangeSpriteByName( string name ) {
        spRend.sprite = sprites[System.Array.IndexOf(names, name)];
    }

	public void setFacing(int p_direction) {
		vIndex = p_direction;
		ChangeSprite(4*vIndex + hIndex);
	}
}
