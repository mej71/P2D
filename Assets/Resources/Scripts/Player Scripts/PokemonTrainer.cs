﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PokemonTrainer {

    public static string TrainerName = "Red";
    int TrainerMoney = 0; 
    int AILevel = 0;
    int TrainerID = 0;
    //trainer type
    Gender gender = Gender.MALE;  //0 for boy, 1 for girl, 2 for none/unknown
    List<int> BadgesEarned = new List<int>();
    List<Pokemon> party;
    ItemBag itemBag;
    //pokedex

    public PokemonTrainer(string name, int money, int aiLevel, Gender p_gender=Gender.MALE) {
        TrainerName = name;
        TrainerMoney = money;
        AILevel = aiLevel;
        gender = p_gender;
        TrainerID = (int)(Random.value * 65535);
        party = new List<Pokemon>();
        itemBag = new ItemBag();
        party.Add(new Pokemon(PBPokemon.ARCANINE, 40));
        party.Add(new Pokemon(PBPokemon.ESPEON, 56));
        party[1].hp = 0;
        party.Add(new Pokemon(PBPokemon.BLASTOISE, 90));
        party[2].hp = party[2].TotalHP() / 4;
        party.Add(new Pokemon(PBPokemon.CACTURNE, 20));
        party[3].hp = party[2].TotalHP() / 2;
        party.Add(new Pokemon(PBPokemon.MANKEY, 50));
        party.Add(new Pokemon(PBPokemon.ZEKROM, 50));
    }

    public PokemonTrainer(PokemonTrainer copyTrainer) {
        TrainerName = copyTrainer.getName();
        TrainerMoney = copyTrainer.getMoney();
        AILevel = copyTrainer.getAILevel();
        gender = copyTrainer.getGender();
        TrainerID = copyTrainer.getTrainerID();
        itemBag = copyTrainer.getBag();
    }

    public string getName() {return TrainerName;}

    public int getMoney() {return TrainerMoney;}

    public void setMoney(int m) {TrainerMoney = m;}

    public void addMoney(int m) {TrainerMoney += m;}

    public void takeMoney(int m) {TrainerMoney -= m;}

    public int getAILevel() {return AILevel;}

    public int getTrainerID() {return TrainerID;}

    public Gender getGender() {return gender;}

    public List<int> getBadgesEarned() {return BadgesEarned;}

    public bool hasPokemon() {return party.Count > 0;}

    public List<Pokemon> getParty() {return party;}

    public int numInParty() { return party.Count; }

    public ItemBag getBag() {return itemBag;}

}