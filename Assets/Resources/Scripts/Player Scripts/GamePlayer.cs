﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GamePlayer {
    // test
	static GameObject gamePlayer;
    public static PokemonTrainer gameTrainer;
    public static bool hasTransferred = false;

	public static GameObject getPlayer() {
        if (gamePlayer == null) {
			gamePlayer = GameObject.FindGameObjectWithTag("Player");
		}
		return gamePlayer;
    }

	public static float getPlayerX() {
		if (gamePlayer == null) {
			getPlayer();
		}
		return gamePlayer.transform.position.x;
	}

    public static float getPlayerY() {
        if (gamePlayer == null) {
            getPlayer();
        }
        return gamePlayer.transform.position.y;
    }

    public static PokemonTrainer getTrainer() {
        //if not loaded, load
        if (gameTrainer == null) {
            SaveGameData.loadData();
            if (gameTrainer == null) {
                //try load, create new if false
                gameTrainer = new PokemonTrainer("Jem", 100, 25, Gender.MALE);
            }
        }
        return gameTrainer;
    }

    public static void updateTrainer(PokemonTrainer loadTrainer) {
        gameTrainer = new PokemonTrainer(loadTrainer);
    }

    public static void sayMoney() {
        UIManager.displayText("$" + GamePlayer.getTrainer().getMoney());
    } 

    public static void changeTrainerMoney(int amount, int type) {
        switch (type) {
            case 0: //set to new amount
                GamePlayer.getTrainer().setMoney(amount);
                break;
            case 1: //add amount to total money
                GamePlayer.getTrainer().addMoney(amount);
                break;
            case 2: //remove money
                GamePlayer.getTrainer().takeMoney(amount);
                break;
            default:
                break;
        }
    }
}
