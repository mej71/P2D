﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovement : MonoBehaviour {

	Vector3 pos;
	Vector3 oldPos;
	private float _speed = 1.2f;                   	// Speed of movement
	private float _distance = 0.32f;				//Distance between tiles, don't change this value
	private Rigidbody2D _RgdBody;
	private AnimatedTextureExtendedUV _AnimTxtrExtnd;	//animates the player using the charset
    SpriteRenderer spRend;
	float[] movingTimes = {0.0f, 0.0f, 0.0f, 0.0f};
	Vector3[] movingVectors;
    string strDirection = "";
    int intDirection = 0;
    bool transferred = false;
    bool isRunning = false;
    bool runningToggle = false;
    bool isBiking = false;
    public int facing = 0;
    float runningSpeed = 2.0f;
    float bikingSpeed = 4.0f;
    float runningFrameSpeed = 10f;
    MapPositionWatcher positionWatcher;
	private bool _canTryPass = true;

	// Use this for initialization
	void Start () {
		pos = transform.position;          // Take the initial position
		oldPos = pos;
		_RgdBody = GetComponent<Rigidbody2D> ();
		_AnimTxtrExtnd = GetComponent<AnimatedTextureExtendedUV>();
		positionWatcher = GetComponent<MapPositionWatcher>();
        spRend = GetComponent<SpriteRenderer>();
		//used to choose the movement destination
		movingVectors = new Vector3[4] {
			new Vector3(0.0f, -1*_distance, 0.0f),
			new Vector3(-1*_distance, 0.0f, 0.0f),
			new Vector3(_distance, 0.0f, 0.0f),
			new Vector3(0.0f, _distance, 0.0f)
		};
		_AnimTxtrExtnd.setFacing(facing);
	}

    void resetMovingTimes(bool resetDirection = false, int direction = -1) {
        if (resetDirection) {
            strDirection = "";
        }
        for (int i = 0; i < movingTimes.Length; i++) {
            if (i == direction) {
                movingTimes[i] += Time.deltaTime;
            } else {
                movingTimes[i] = 0.0f;
            }
        }
    }

	// Update is called once per frame
	void Update () {
        if (UIManager.anyMenusShowing()) {
            return;
        }
        if (Input.GetKeyUp(KeyCode.Space)) {
            UIManager.showPauseMenu();
        }
        if(Input.GetKeyUp(KeyCode.LeftShift) && PlayerOptions.runningKey == PlayerOptions.RunningKey.Toggle){
            runningToggle = !runningToggle;
        }
        if(PlayerOptions.runningKey == PlayerOptions.RunningKey.Hold){
            runningToggle = false;
        }
        if (Input.GetKeyUp(KeyCode.O))
        {
            //Toggle on/off Bicycle (Temporary placehold for item)
            isBiking = !isBiking;
            //If on, it switches to the bike spritesheet and keeps the player's initial direction 
            if (isBiking)
            {
                _AnimTxtrExtnd.addToSpriteSheetName("_bike", runningFrameSpeed);
                _AnimTxtrExtnd.setFacing(intDirection);
            }
            //If off, it switches to the walking spritesheet and keeps the player's initial direction 
            else
            {
                _AnimTxtrExtnd.revertSpritesheet();
                _AnimTxtrExtnd.setFacing(intDirection);
            }
            Debug.Log(isBiking);
        }
        if (GamePlayer.hasTransferred) {
			GamePlayer.hasTransferred = false;
			pos = transform.position;
            resetMovingTimes(true);
		}
		else {
			 //prefer button player is currently holding down first, determine which button is being pushed
			 if (strDirection.Equals("") || !Input.GetButton(strDirection)) {
				 if (Input.GetButton("Up")) {
					 strDirection = "Up";
					 intDirection = 3;
				 } else if (Input.GetButton("Left")) {
					 strDirection = "Left";
					 intDirection = 1;
				 } else if (Input.GetButton("Down")) {
					 strDirection = "Down";
					 intDirection = 0;
				 } else if (Input.GetButton("Right")) {
					 strDirection = "Right";
					 intDirection = 2;
				 } else {
                     resetMovingTimes(true);
                }
			 }
      
            
             if (!strDirection.Equals("") && Input.GetButton(strDirection) && transform.position == pos) {  //move in predetermined direction
                //If holding shift and not biking, start running
                if (((Input.GetButton("Shift") && PlayerOptions.runningKey == PlayerOptions.RunningKey.Hold) || (runningToggle && PlayerOptions.runningKey == PlayerOptions.RunningKey.Toggle)) && !isBiking) {
                    _AnimTxtrExtnd.addToSpriteSheetName("_run", runningFrameSpeed);
                    isRunning = true;

                }
                resetMovingTimes(false, intDirection);
                //must be pressing down the directional button a bit to move, to distinguish between turning and walking
                if ( movingTimes[intDirection] > 0.1f) {
			        //check if the destination will have
					if (_canTryPass) {
						if (PassabilityCheck.canPass(_RgdBody, movingVectors[intDirection], _distance)) {
							StartCoroutine(_AnimTxtrExtnd.UpdateSpriteAnimation());
							pos += movingVectors[intDirection];
							_canTryPass = true;	
						} else {
							AudioController.playSE("bump.mp3");
							_canTryPass = false;
						}
					}
		        } else {
					_canTryPass = true;
				}
		        _AnimTxtrExtnd.setFacing(intDirection);
		    } 
		 }
         if (isRunning) {
            transform.position = Vector3.MoveTowards(transform.position, pos, runningSpeed * Time.deltaTime);    // Move there
        }
         else if (isBiking)
        {
            transform.position = Vector3.MoveTowards(transform.position, pos, bikingSpeed * Time.deltaTime); //Move there
        }
        else {
            transform.position = Vector3.MoveTowards(transform.position, pos, _speed * Time.deltaTime);    // Move there
        }
		if(oldPos!=pos && Vector3.Distance(transform.position, pos) == 0.0f) {
            //only update Z position if moving up or down
            positionWatcher.updatePosition();
            updatePlayerZPosition();
            oldPos = pos;
            if (!isBiking)//Only revert back to walking sprite if the player isn't biking
            {
                _AnimTxtrExtnd.revertSpritesheet();
            }
            isRunning = false;
			_canTryPass = true;
        }
		//Try to trigger event in front
		if (Input.GetKeyDown(KeyCode.C)) {
			CheckForEvents.TriggerEventInFrontActionButton(_RgdBody, movingVectors[intDirection], _distance);	
		} else if (Input.GetKeyDown(KeyCode.J)) {
			UIManager.showTextOptionMenu(new List<string>(){"test", "test2", "test4", "test5", "test3"});
		}
	}

	public void setTransferred(bool hasTransferred, int setDirection) {
		transferred = hasTransferred;
		if (setDirection>=-2 && setDirection<=2 && setDirection!=0) {
			Debug.Log(setDirection);
			facing = setDirection;
			_AnimTxtrExtnd.setFacing(facing);
		}
	}

	public void setSpeed(float p_speed) {
		_speed = p_speed;
	}

    public void updatePlayerZPosition() {
        spRend.sortingOrder = Mathf.Abs((int)( (gameObject.transform.position.y - positionWatcher.highestYValue) / 0.32f));
    }

}
