﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

//Base class for choice boxes
public class ChoiceController : MonoBehaviour {

    protected Image imageBox, imageArrow;
    protected GameObject textObject;
    protected int numMenuItems;
    protected int arrowPos;
    protected string arrowLocation, imageBoxName, imageArrowName, textObjectName;
    //Menu items have a key that is a capitalized name, and the value is the name displayed.
    protected Dictionary<string, string> menuOptions = new Dictionary<string, string>();
    //values for object positioning
    protected int xPos, yPos, setWidth, setHeight;
    protected float widthScale, heightScale;
    protected bool hasControl;
    protected int arrowWidth, arrowHeight, menuSpacing, arrowStartYPos, arrowSpacing;
    protected bool canCancel = true;

    // Use this for initialization
    protected void Start() {
        numMenuItems = 0;
        arrowPos = 0;
        Sprite[] arrowSprites = Resources.LoadAll<Sprite>(arrowLocation);
        Image[] tempImages = GetComponentsInChildren<Image>(true);
        foreach (Image image in tempImages) {
            if (image.name.Equals(imageBoxName)) {
                imageBox = image;
                imageBox.enabled = true;
            } else if (image.name.Equals(imageArrowName)) {
                image.GetComponent<UnityEngine.UI.Image>().sprite = arrowSprites[1];
                imageArrow = image;
                imageArrow.enabled = true;
            }
        }
        foreach (Transform child in transform) {
            if (child.name.Equals(textObjectName)) {
                textObject = child.gameObject;
                break;
            }
        }
        hasControl = true;
        imageBox.sprite = Resources.Load("Graphics/UI/WindowSkins/" + PlayerOptions.curMenuFrame, typeof(Sprite)) as Sprite;
        imageBox.GetComponent<RectTransform>().sizeDelta = new Vector2(setWidth, menuOptions.Count* menuSpacing - 20);
        positionChoiceBoxElements();
        updateFont();
    }

    // Update is called once per frame
    public void Update() {
        if (hasControl) {
            bool arrowPosChanged = false;
            if (Input.GetKeyDown(KeyCode.S)) { //Down
                ++arrowPos;
                if (arrowPos == menuOptions.Count) {
                    arrowPos = 0;
                }
                arrowPosChanged = true;
            } else if (Input.GetKeyDown(KeyCode.W)) { //Up
                --arrowPos;
                if (arrowPos < 0) {
                    arrowPos = menuOptions.Count - 1;
                }
                arrowPosChanged = true;
            } else if (Input.GetKeyDown(KeyCode.X)) { //Exit
                StartCoroutine(executeSelection(true));
            }
            if (arrowPosChanged) {
                imageArrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+15, arrowStartYPos - (arrowPos * arrowSpacing), 0);
            }
            if (Input.GetButtonDown("Enter")) {
                StartCoroutine(executeSelection());
            }
            if(OptionsScreenController.isDestroyed){
                updateImageBox();
                updateFont();
                OptionsScreenController.isDestroyed = false;
            }
        }
    }

    void OnDestroy() {
        imageBox.enabled = false;
        imageArrow.enabled = false;
        textObject.GetComponent<Text>().enabled = false;
    }

    protected virtual void positionChoiceBoxElements() {
        imageBox.GetComponent<RectTransform>().localScale = widthScale != 0 || heightScale != 0 ? new Vector3(widthScale, heightScale, 1) : new Vector3(1, 1, 1); 
        imageBox.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos, yPos, 0);
        imageArrow.GetComponent<RectTransform>().sizeDelta = new Vector2(arrowWidth, arrowHeight);
        imageArrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+15, arrowStartYPos, 0);
        textObject.GetComponent<RectTransform>().sizeDelta = new Vector2(setWidth, menuOptions.Count * menuSpacing);
        textObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+50, yPos-35, 0);
        //Fill menu text
        textObject.GetComponent<Text>().enabled = false;
        textObject.GetComponent<Text>().text = "";
        for (int i = 0; i<menuOptions.Count-1; ++i ) {
            textObject.GetComponent<Text>().text += menuOptions.Values.ElementAt(i) + "\n";
        }
        textObject.GetComponent<Text>().text += menuOptions.Values.ElementAt(menuOptions.Count - 1);
        textObject.GetComponent<Text>().enabled = true;
    }
    protected void updateImageBox(){
        imageBox.sprite = Resources.Load("Graphics/UI/WindowSkins/" + PlayerOptions.curMenuFrame, typeof(Sprite)) as Sprite;
    }
    protected void updateFont(){
        textObject.GetComponent<Text>().font = FontManager.getFont(PlayerOptions.getFontStyle());
        switch(PlayerOptions.curFont){
			case PlayerOptions.FontStyle.Em:
                textObject.GetComponent<Text>().lineSpacing = 2f;
				break;
			case PlayerOptions.FontStyle.RS:
			    textObject.GetComponent<Text>().lineSpacing = 1.6f;
				break;
			case PlayerOptions.FontStyle.FRLG:
			    textObject.GetComponent<Text>().lineSpacing = 2.45f;
				break;
			case PlayerOptions.FontStyle.DPPT:
			    textObject.GetComponent<Text>().lineSpacing = 2;
				break;			
		}
    }

    //Empty method, child classes override and decide what to do with this
    protected virtual IEnumerator executeSelection(bool exit = false) {yield break;}  
}
