﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SummaryScreenController : UIScreen {

    int curPage = 0;
    int curPartyIndex = 0;
    const int defaultTextSize = 27;
    const int defaultMainTextSize = 55;
    const int maxPages = 4;  //0-4
    public int neededEXP = Pokemon.getExpForLevel(Pokemon.obtainLevel + 1) - Pokemon.getExpForLevel(Pokemon.obtainLevel);
    static readonly Color defaultTextColor = new Color(0, 0, 0);
    const int lineWidth = 525;
    Sprite[] typeIcons;

    private string[] pageTitles = { "INFO", "TRAINER MEMO", "SKILLS", "MOVES", "RIBBONS" };
    //Page 1
    UIImageDef monType1 = new UIImageDef();
    UIImageDef monType2 = new UIImageDef();
    UIImageDef toNextExpBar = new UIImageDef();
    UITextDef dexNoLabel = new UITextDef(new Vector3(233, -84, 0), defaultTextSize, defaultTextColor, "Dex No.");
    UITextDef dexNoText = new UITextDef(new Vector3(370, -84, 0), defaultTextSize, defaultTextColor, "");
    UITextDef speciesLabel = new UITextDef(new Vector3(233, -114, 0), defaultTextSize, defaultTextColor, "Species");
    UITextDef speciesText = new UITextDef(new Vector3(370, -114, 0), defaultTextSize, defaultTextColor, "");
    UITextDef typeLabel = new UITextDef(new Vector3(233, -146, 0), defaultTextSize, defaultTextColor, "Type");
    UITextDef otLabel = new UITextDef(new Vector3(233, -179, 0), defaultTextSize, defaultTextColor, "OT");
    UITextDef otText = new UITextDef(new Vector3(370, -179, 0), defaultTextSize, defaultTextColor, GamePlayer.getTrainer().getName());
    UITextDef idLabel = new UITextDef(new Vector3(230, -211, 0), defaultTextSize, defaultTextColor, "ID No.");
    UITextDef idText = new UITextDef(new Vector3(370, -211, 0), defaultTextSize, defaultTextColor, "");
    UITextDef curExpLabel = new UITextDef(new Vector3(235, -242, 0), defaultTextSize, defaultTextColor, "Exp. Points");
    UITextDef curExpText = new UITextDef(new Vector3(365, -277, 0), defaultTextSize, defaultTextColor, "50/593");
    UITextDef toNextExpLabel = new UITextDef(new Vector3(233, -307, 0), defaultTextSize, defaultTextColor, "To Next Lv.");
    UITextDef toNextExpText = new UITextDef(new Vector3(490, -348, 0), defaultTextSize, defaultTextColor, "");
    //Page 2
    UITextDef natureText = new UITextDef(new Vector3(232, -86, 0), defaultTextSize, defaultTextColor, "");
    UITextDef aqDateText = new UITextDef(new Vector3(232, -118, 0), defaultTextSize, defaultTextColor, "");
    UITextDef aqLocText = new UITextDef(new Vector3(232, -150, 0), defaultTextSize, defaultTextColor, "");
    UITextDef metText = new UITextDef(new Vector3(232, -182, 0), defaultTextSize, defaultTextColor, "");
    UITextDef traitText = new UITextDef(new Vector3(232, -256, 0), defaultTextSize, defaultTextColor, "");
    //Page 3
    UIImageDef hpBar = new UIImageDef(false);
    UITextDef hpLabel = new UITextDef(new Vector3(242, -80, 0), defaultTextSize, defaultTextColor, "HP");
    UITextDef hpText = new UITextDef(new Vector3(360, -80, 0), defaultTextSize, defaultTextColor, "");
    UITextDef atkLabel = new UITextDef(new Vector3(242, -124, 0), defaultTextSize, defaultTextColor, "Attack");
    UITextDef atkText = new UITextDef(new Vector3(360, -124, 0), defaultTextSize, defaultTextColor, "");
    UITextDef defLabel = new UITextDef(new Vector3(242, -155, 0), defaultTextSize, defaultTextColor, "Defense");
    UITextDef defText = new UITextDef(new Vector3(360, -155, 0), defaultTextSize, defaultTextColor, "");
    UITextDef spatkLabel = new UITextDef(new Vector3(242, -188, 0), defaultTextSize, defaultTextColor, "Sp. Attack");
    UITextDef spatkText = new UITextDef(new Vector3(360, -188, 0), defaultTextSize, defaultTextColor, "");
    UITextDef spdefLabel = new UITextDef(new Vector3(242, -221, 0), defaultTextSize, defaultTextColor, "Sp. Def");
    UITextDef spdefText = new UITextDef(new Vector3(360, -221, 0), defaultTextSize, defaultTextColor, "");
    UITextDef speedLabel = new UITextDef(new Vector3(242, -253, 0), defaultTextSize, defaultTextColor, "Speed");
    UITextDef speedText = new UITextDef(new Vector3(360, -253, 0), defaultTextSize, defaultTextColor, "");
    UITextDef abilLabel = new UITextDef(new Vector3(249, -286, 0), defaultTextSize, defaultTextColor, "Ability");
    UITextDef abilNameText = new UITextDef(new Vector3(360, -286, 0), defaultTextSize, defaultTextColor, "");
    UITextDef abilDescText = new UITextDef(new Vector3(225, -343, 0), defaultTextSize, defaultTextColor, "");
    //Page 4F
    UIImageListDef moveTypeImages = new UIImageListDef(4);
    UIImageListDef movePosImages = new UIImageListDef(4);
    UITextListDef moveNameTexts = new UITextListDef(new Vector3(15, -12, 0), 4, defaultTextSize-3, defaultTextColor, "");
    UITextListDef ppLabels = new UITextListDef(new Vector3(15, -12, 0), 4, defaultTextSize, defaultTextColor, "PP");
    UITextListDef ppTexts = new UITextListDef(new Vector3(15, -12, 0), 4, defaultTextSize, defaultTextColor, "");
    //Page 5
    UIImageListDef ribbonImages = new UIImageListDef(12, false);
    UITextDef ribbonNoLabel = new UITextDef(new Vector3(15, -12, 0), defaultTextSize, defaultTextColor, "No. of Ribbons");
    UITextDef ribbonNoText = new UITextDef(new Vector3(15, -12, 0), defaultTextSize, defaultTextColor, "");
    //All pages (positions are a bit ugly because of local positioning.  deal with it or rewrite a bunch of stuff I guess)
    UIImageDef ballImage = new UIImageDef();
    UIImageDef speciesImage = new UIImageDef();
    UIImageDef shinyIcon = new UIImageDef();
    UIImageDef pokerusIcon = new UIImageDef();
    UIImageListDef summaryBGs = new UIImageListDef(5, false);
    UITextDef pageTitleText = new UITextDef(new Vector3(57, -42, 0), defaultMainTextSize);
    UITextDef markingsText = new UITextDef(new Vector3(-241, -420, 0), defaultMainTextSize);
    UITextDef itemLabel = new UITextDef(new Vector3(15, -650, 0), defaultMainTextSize, defaultTextColor, "Item");
    UITextDef itemText = new UITextDef(new Vector3(15, -715, 0), defaultMainTextSize);
    UITextDef nameText = new UITextDef(new Vector3(12, -130, 0), defaultMainTextSize);
    UITextDef genderText = new UITextDef(new Vector3(358, -135, 0), defaultMainTextSize);
    UITextDef levelText = new UITextDef(new Vector3(120, -195, 0), defaultMainTextSize);
    GameObject mainPage;

    //index some values to avoid lookup time
    int maxPartyIndex;
    List<Pokemon> pokes = new List<Pokemon>();
    List<PokemonData> pokeDatas = new List<PokemonData>();

    public void setPartyIndex(int index) {
        curPartyIndex = index;        
    }


    // Use this for initialization
    void Start () {
        StartCoroutine("initData");
        InitializeAllTextAndImageFields(this);
        StartCoroutine("findMainPage");        
        resizeBGs();
        //Manually adjust a couple objects that get group defined I guess.
        //Tried to do these modifications in UIManager, but some just get ignored, so we have to do them here
        traitText.t.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector3(lineWidth, 22);
        traitText.t.horizontalOverflow = HorizontalWrapMode.Wrap;
        traitText.t.lineSpacing = 1.9f;
        abilDescText.t.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(lineWidth, 45);
        abilDescText.t.horizontalOverflow = HorizontalWrapMode.Wrap;
        abilDescText.t.lineSpacing = 1.9f;
        Sprite[] movePosSprites = Resources.LoadAll<Sprite>("Graphics/UI/Summary Screen/move_positions");
        //adjust positioning for moves
        for (int i = 0; i < 4; ++i) {
            moveTypeImages.i[i].transform.localPosition = new Vector3(246, -127 - (64 * i), 1);
            moveTypeImages.i[i].rectTransform.sizeDelta = new Vector2(96, 28);
            movePosImages.i[i].transform.localPosition = new Vector3(252, -158 - (64 * i), 1);
            movePosImages.i[i].sprite = movePosSprites[i];
            movePosImages.i[i].rectTransform.sizeDelta = new Vector2(60, 26);
            moveNameTexts.t[i].transform.localPosition = new Vector3(344, -103 - (64 * i), 1);
            ppLabels.t[i].transform.localPosition = new Vector3(327, -135 - (64 * i), 1);
            ppTexts.t[i].transform.localPosition = new Vector3(384, -135 - (64 * i), 1);
        }
        if (speciesImage.i.gameObject.GetComponent<PokemonSprite>() == null) {
            speciesImage.i.gameObject.AddComponent<PokemonSprite>();
            speciesImage.i.rectTransform.localScale = new Vector3(2, 2, 1);
            speciesImage.i.rectTransform.pivot = new Vector2(0, 1);
            speciesImage.i.rectTransform.sizeDelta = new Vector2(160, 160);
            speciesImage.i.rectTransform.localPosition = new Vector3(42, -290, 0);
        }
        monType1.i.rectTransform.localPosition = new Vector3(371, -173, 1);
        monType2.i.rectTransform.localPosition = new Vector3(435, -173, 1);
        monType1.i.rectTransform.sizeDelta = new Vector2(64, 28);
        monType2.i.rectTransform.sizeDelta = new Vector2(64, 28);
        toNextExpText.t.alignment = TextAnchor.MiddleRight;
        updateMon();
        updateCurrentPage();
    }


    IEnumerator initData() {
        //index values
        pokes = GamePlayer.getTrainer().getParty();
        for (int i = 0; i < pokes.Count; ++i) {
            pokeDatas.Add(SpeciesManager.getSpeciesList().Find(x => x.monEnum == pokes[i].getSpeciesID().ToString()));
        }
        maxPartyIndex = GamePlayer.getTrainer().getParty().Count - 1;
        typeIcons = Resources.LoadAll<Sprite>("Graphics/UI/General/pokedexTypes");
        yield return null;
    }

    IEnumerator findMainPage() {
        mainPage = gameObject.transform.Find("All Pages").gameObject;
        yield return null;
    }


    void resizeBGs() {
        foreach (Image img in summaryBGs.i) {
            img.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 1);
        }
        mainPage.GetComponent<RectTransform>().sizeDelta = new Vector2(1024, 768);
    }

    void OnDestroy() {
        ChangeAllImageAndTextEnableValue(this, false);
    }

    void updateMon() {
        //ball image
        speciesImage.i.gameObject.GetComponent<PokemonSprite>().updatePokemonSprite(pokes[curPartyIndex]);
        //shiny icon
        //pokerus
        itemText.t.text = pokes[curPartyIndex].item.ToString();
        nameText.t.text = pokes[curPartyIndex].name();
        genderText.t.text = (pokes[curPartyIndex].gender == Gender.MALE) ? "♂" : (pokes[curPartyIndex].gender == Gender.FEMALE) ? "♀" : "";
        levelText.t.text = pokes[curPartyIndex].level.ToString();
        string type1, type2;
        type1 = pokes[curPartyIndex].getType1();
        type2 = pokes[curPartyIndex].getType2();
        monType1.i.sprite = typeIcons[(int)(PBTypes)System.Enum.Parse(typeof(PBTypes), type1)];
        if (type1.Equals(type2)) { //if type1 and 2 are the same, it's a single-typed mon
            monType2.i.enabled = false;
            monType1.i.rectTransform.localPosition = new Vector3(391, -173, 1);
        } else {
            monType2.i.enabled = true;
            monType2.i.sprite = typeIcons[(int)(PBTypes)System.Enum.Parse(typeof(PBTypes), type2 )];
            monType1.i.rectTransform.localPosition = new Vector3(371, -173, 1);
        }
    }

    public void updateCurrentPage() {
        for (int i = 0; i < summaryBGs.i.Count; ++i) {
            summaryBGs.i[i].gameObject.SetActive((curPage == (i)) ? true : false);
            summaryBGs.i[i].enabled = (curPage == (i)) ? true : false;
        }
        pageTitleText.t.text = pageTitles[curPage];
        switch (curPage) {
            case 0:
                dexNoText.t.text = ((int)pokes[curPartyIndex].getSpeciesID()).ToString();  //do dex lookup later maybe?  but order defined should be national dex order anyway so this works
                speciesText.t.text = pokeDatas[curPartyIndex].monName;
                otText.t.text = GamePlayer.getTrainer().getName();
                idText.t.text = pokes[curPartyIndex].getMonID().ToString();
                curExpText.t.text = Pokemon.getExpForLevel(pokes[curPartyIndex].getLevel()).ToString(); // getExpForLevel() needed and int in the parentheses, made obtainLevel a static member in Pokemon.cs, works great! - Kyle
                if (pokes[curPartyIndex].getLevel() != 100) {
                    toNextExpText.t.text = (Pokemon.getExpForLevel(pokes[curPartyIndex].getLevel() + 1) - Pokemon.getExpForLevel(pokes[curPartyIndex].getLevel())).ToString();
                } else {
                    toNextExpText.t.text = "0";
                }
                break;
            case 1:
                string nText = pokes[curPartyIndex].getNature().ToString().ToLower() + " nature";
                natureText.t.text = char.ToUpper(nText[0]) + nText.Substring(1);
                aqDateText.t.text = pokes[curPartyIndex].timeObtained.ToShortDateString();
                aqLocText.t.text = pokes[curPartyIndex].obtainMap;
                string mText = pokes[curPartyIndex].obtainType.ToString();
                metText.t.text = char.ToUpper(mText[0]) + mText.Substring(1);
                traitText.t.text = pokes[curPartyIndex].getTraitText();
                break;
            case 2:
                hpText.t.text = pokes[curPartyIndex].hp.ToString() + "/" + pokes[curPartyIndex].TotalHP();
                atkText.t.text = pokes[curPartyIndex].Attack().ToString();
                defText.t.text = pokes[curPartyIndex].Defense().ToString();
                spatkText.t.text = pokes[curPartyIndex].SpAtk().ToString();
                spdefText.t.text = pokes[curPartyIndex].SpDef().ToString();
                speedText.t.text = pokes[curPartyIndex].Speed().ToString();
                abilNameText.t.text = pokes[curPartyIndex].getAbilityName();
                abilDescText.t.text = pokes[curPartyIndex].getAbilityDesc();
                break;
            case 3:
                Moves m;
                for (int i = 0; i < 4; ++i) {
                    if (pokes[curPartyIndex].moves.Count > i) {
                        m = MoveManager.getMoves().Find(x => x.internalName == pokes[curPartyIndex].moves[i].moveID.ToString());
                        moveTypeImages.i[i].enabled = true;
                        moveTypeImages.i[i].sprite = typeIcons[(int)(PBTypes)System.Enum.Parse(typeof(PBTypes), m.moveType)];
                        movePosImages.i[i].enabled = true;
                        moveNameTexts.t[i].enabled = true;
                        moveNameTexts.t[i].text = m.name;
                        ppLabels.t[i].text = "PP";
                        ppTexts.t[i].text = pokes[curPartyIndex].moves[i].pp.ToString();
                    } else {
                        moveTypeImages.i[i].enabled = false;
                        movePosImages.i[i].enabled = false;
                    }
                }
                break;
            default: //Page 5
                break;
        }
    }


    // Update is called once per frame
    void Update () {
        if (!hasControl) {
            return;
        }
        if (Input.GetKeyDown(KeyCode.X)) {
            Destroy(this);
        } else if (Input.GetKeyDown(KeyCode.D)) { //Right
            ++curPage;
            if (curPage> maxPages) {
                curPage = 0;
            }
            updateCurrentPage();
        } else if (Input.GetKeyDown(KeyCode.A)) { //Left
            --curPage;
            if (curPage < 0) {
                curPage = maxPages;
            }
            updateCurrentPage();
        } else if (Input.GetKeyDown(KeyCode.S)) { //Down
            curPartyIndex++;
            if (curPartyIndex> maxPartyIndex) {
                curPartyIndex = 0;
            }
            updateMon();
            updateCurrentPage();
        } else if (Input.GetKeyDown(KeyCode.W)) { //Up
            curPartyIndex--;
            if (curPartyIndex<0) {
                curPartyIndex = maxPartyIndex;
            }
            updateMon();
            updateCurrentPage();
        }
    }
}
