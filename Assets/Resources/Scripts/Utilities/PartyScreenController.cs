﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class PartyScreenController : UIScreen {

    int panelBaseX = 0;
    int panelBaseYL = 0;
    int panelBaseYR = -16;
    int panelWidthSpace = 256;
    int panelHeightSpace = 96;
    int ballOffsetX = 10;
    int ballOffsetY = 2;
    int hpBarBaseOffsetX = 97;
    int hpBarBaseOffsetY = -46;
    int hpBarOffsetX = 30;
    int hpBarOffsetY = -2;
    int curSelectedIndex = 0;
    int lastSelectedIndex = 0;
    string helpText;
    static readonly Color defaultTextColor = new Color(255, 255, 255);

    UIImageDef bgImage = new UIImageDef();
    UIImageDef helpTextImg = new UIImageDef();
    UIImageListDef partyPanels = new UIImageListDef(7);
    UIImageListDef ballIcons = new UIImageListDef(6);
    UIImageListDef pokemonIcons = new UIImageListDef(6);
    UIImageListDef selectPanels = new UIImageListDef(7, false);
    UIImageListDef hpBarBases = new UIImageListDef(6);
    UIImageListDef hpBarBgs = new UIImageListDef(6);
    UIImageListDef hpBarOutlines = new UIImageListDef(6);
    UIImageListDef hpBars = new UIImageListDef(6);
    UITextDef cancelTextObj = new UITextDef(new Vector3(15, -12, 0), 31, defaultTextColor, "CANCEL");
    UITextDef helpTextObj = new UITextDef(new Vector3(25, 65, 0), 45, new Color(0,0,0));
    UITextListDef nameText = new UITextListDef(new Vector3(100, -20, 0), 6, 22, defaultTextColor);
    UITextListDef genderText = new UITextListDef(new Vector3(200, -20, 0), 6, 22, defaultTextColor);
    UITextListDef levelText = new UITextListDef(new Vector3(30, -70, 0), 6, 20, defaultTextColor);
    UITextListDef hpText = new UITextListDef(new Vector3(180, -70, 0), 6, 20, defaultTextColor);
    GameObject partyMenu;
    

    void Start () {
        //Fill each list with appropriate number of values, so we can set by index easily
        InitializeAllTextAndImageFields(this);
        partyMenu = gameObject.transform.Find("partyMenu").gameObject;
        foreach (Image i in pokemonIcons.i) {
            if (i.gameObject.GetComponent<PokemonIconSprite>() == null) {
                i.gameObject.AddComponent<PokemonIconSprite>();
            } else {
                i.gameObject.GetComponent<PokemonIconSprite>().enabled = false;
                i.gameObject.GetComponent<PokemonIconSprite>().enabled = true; //reset enable to cancel invokes
            }
        }
        helpTextImg.i.sprite = Resources.Load("Graphics/UI/WindowSkins/choice 1", typeof(Sprite)) as Sprite;
        helpTextImg.i.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(512, 96);
        helpTextImg.i.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(5, 5, 1);
        helpTextImg.i.enabled = true;
        updatePanels();
        updateSelection();
    }

    public void setHelpText(string text) {
        helpTextObj.text = text;
    }

    // Update is called once per frame
    void Update () {
        if (!hasControl) {
            return;
        }
        if (Input.GetKeyDown(KeyCode.S)) { //Down
            if (curSelectedIndex==5 && GamePlayer.getTrainer().getParty().Count==6) {
                curSelectedIndex += 1;
            } else {
                curSelectedIndex += 2;
            }
            if (curSelectedIndex>6) {
                curSelectedIndex = 0;
            } else if (curSelectedIndex > GamePlayer.getTrainer().getParty().Count) {
                curSelectedIndex = 6;
            }
            updateSelection();
            lastSelectedIndex = curSelectedIndex;
        } else if (Input.GetKeyDown(KeyCode.W)) { //Up
            curSelectedIndex-=2;
            if (curSelectedIndex < 0) {
                curSelectedIndex = 6;
            } else if (lastSelectedIndex == 6) {
                curSelectedIndex = GamePlayer.getTrainer().getParty().Count - 1;
            }
            updateSelection();
            lastSelectedIndex = curSelectedIndex;
        } else if (Input.GetKeyDown(KeyCode.A)) {
            curSelectedIndex -= 1;
            if (curSelectedIndex < 0) {
                curSelectedIndex = 6;
            } else if (lastSelectedIndex == 6) {
                curSelectedIndex = GamePlayer.getTrainer().getParty().Count - 1;
            }
            updateSelection();
            lastSelectedIndex = curSelectedIndex;
        } else if (Input.GetKeyDown(KeyCode.D)) {
            curSelectedIndex += 1;
            if (curSelectedIndex > 6) {
                curSelectedIndex = 0;
            } else if (curSelectedIndex > GamePlayer.getTrainer().getParty().Count) {
                curSelectedIndex = 6;
            }
            updateSelection();
            lastSelectedIndex = curSelectedIndex;
        } else if (Input.GetKeyDown(KeyCode.X)) {
            Destroy(this);
        }
        if (Input.GetButtonDown("Enter")) {
            if (curSelectedIndex==6) {
                Destroy(this);
            } else {
                StartCoroutine(showPokemonMenu());
            }
        }
    }

    void updateSelection() {
        if (curSelectedIndex == 6) {
            pokemonIcons.i[lastSelectedIndex].gameObject.GetComponent<PokemonIconSprite>().unselect();
            ballIcons.i[lastSelectedIndex].sprite = Resources.Load<Sprite>("Graphics/UI/Party Screen/partyBall");
        } else if (lastSelectedIndex == 6) {
            pokemonIcons.i[curSelectedIndex].gameObject.GetComponent<PokemonIconSprite>().select();
            ballIcons.i[curSelectedIndex].sprite = Resources.Load<Sprite>("Graphics/UI/Party Screen/partyBallSel");
        } else {
            pokemonIcons.i[lastSelectedIndex].gameObject.GetComponent<PokemonIconSprite>().unselect();
            pokemonIcons.i[curSelectedIndex].gameObject.GetComponent<PokemonIconSprite>().select();
            ballIcons.i[lastSelectedIndex].sprite = Resources.Load<Sprite>("Graphics/UI/Party Screen/partyBall");
            ballIcons.i[curSelectedIndex].sprite = Resources.Load<Sprite>("Graphics/UI/Party Screen/partyBallSel");
        }
        selectPanels.i[lastSelectedIndex].enabled = false;
        selectPanels.i[curSelectedIndex].enabled = true;
    }

    void OnDestroy() {
        ChangeAllImageAndTextEnableValue(this, false);
    }

    void adjustPositioning() {
        bgImage.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 1);
        for (int i = 0; i < 6; ++i) {
            if (i % 2 == 0) {
                partyPanels.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(panelBaseX, panelBaseYL - panelHeightSpace * (i / 2), 1);
            } else {
                partyPanels.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(panelBaseX+ panelWidthSpace, panelBaseYR - panelHeightSpace * ((i-1)/2), 1);
            }
            ballIcons.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(ballOffsetX, ballOffsetY, 1);
            hpBarBgs.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(hpBarBaseOffsetX, hpBarBaseOffsetY, 1);
            hpBarBases.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(hpBarBaseOffsetX, hpBarBaseOffsetY, 1);
            hpBarOutlines.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(hpBarBaseOffsetX, hpBarBaseOffsetY, 1);
            hpBars.i[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(hpBarBaseOffsetX + hpBarOffsetX, hpBarBaseOffsetY + hpBarOffsetY, 1);
        }
        partyPanels.i[6].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(400, -336, 1);
    }

    void updatePanels() {
        adjustPositioning();
        Color panelColor;
        PokemonIconSprite curSprite;
        List<Pokemon> party = GamePlayer.getTrainer().getParty();
        for (int i = 0; i < 6; ++i) {
            curSprite = pokemonIcons.i[i].gameObject.GetComponent<PokemonIconSprite>();
            if (party.Count >= i+1) { //if you have this many pokemon
                hpBars.i[i].enabled = hpBarBases.i[i].enabled = hpBarBgs.i[i].enabled = hpBarOutlines.i[i].enabled = true;
                if (party[i].hp <= 0) { //if pokemon at index is fainted
                    panelColor = Color.HSVToRGB(0.06f, 1, 1); //reddish orange
                    hpBars.i[i].color = panelColor;
                } else {
                    panelColor = Color.HSVToRGB(0.30f, 1, 1); //green
                    if (party[i].hp <= (party[i].TotalHP()/4.0f)) {
                        hpBars.i[i].color = Color.HSVToRGB(0.02f, 1, 1); //red
                    } else if (party[i].hp <= (party[i].TotalHP() / 2.0f)) {
                        hpBars.i[i].color = Color.HSVToRGB(0.10f, 1, 1); //orange
                    } else {
                        hpBars.i[i].color = panelColor; //green
                    }
                }
                hpBars.i[i].gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(100*((float)party[i].hp/ party[i].TotalHP()), 8);
                hpBarOutlines.i[i].color = panelColor;
                ballIcons.i[i].enabled = true;
                pokemonIcons.i[i].enabled = true;
                curSprite.initPokemonIconSprite(party[i]);
                pokemonIcons.i[i].gameObject.transform.localPosition = new Vector3(40, -10, 1);
                nameText.t[i].text = party[i].name();
                genderText.t[i].text = (party[i].gender == Gender.FEMALE) ? "♀" : (party[i].gender == Gender.MALE) ? "♂" : "";
                genderText.t[i].color = (party[i].gender == Gender.FEMALE) ? Color.cyan : (party[i].gender == Gender.MALE) ? Color.red : Color.black;
                levelText.t[i].text = "Lv. " + party[i].level.ToString();
                hpText.t[i].text = party[i].hp.ToString() + "/" + party[i].TotalHP();
            } else {
                panelColor = Color.HSVToRGB(0.50f, 1, 1); //blue
                ballIcons.i[i].enabled = false;
                pokemonIcons.i[i].enabled = false;
                hpBars.i[i].enabled = hpBarBases.i[i].enabled = hpBarBgs.i[i].enabled = hpBarOutlines.i[i].enabled = false;
                curSprite.enabled = false;
            }
            partyPanels.i[i].color = panelColor;
        }
    }

    IEnumerator showPokemonMenu() {
        hasControl = false;
        partyMenu.AddComponent<PartyScreenMenuController>().setPokemon(GamePlayer.getTrainer().getParty()[curSelectedIndex], curSelectedIndex);
        while (partyMenu.GetComponent<PartyScreenMenuController>() != null) {
            yield return null;
        }
        hasControl = true;  
        yield break;
    }

}


public class PartyScreenMenuController : ChoiceController {

    private Pokemon poke;
    private int index;

    new void Start() {
        //Set desired objects/sprites to use (already created, and children of the game object this is attatched to)
        arrowLocation = "Graphics/Pictures/arrows";
        imageBoxName = "Party Menu Skin";
        imageArrowName = "Party Menu Selector";
        textObjectName = "Party Menu Text";
        //Set desired positioning
        yPos = -416 - 200 * (int)((index - 1)) / 2;
        arrowStartYPos = yPos + 237;
        if (index % 2 == 0) {
            xPos = 520;
            yPos -= 96;
            arrowStartYPos -= 96;
        } else {
            xPos = 250;
        }
        setWidth = 250;
        arrowHeight = arrowWidth = 20;
        menuSpacing = 60;
        arrowSpacing = 52;
        base.Start();
    }

    public void setPokemon(Pokemon p, int p_index) {
        poke = p;
        index = p_index;
        updateMenuText();
    }

    void updateMenuText() {
        menuOptions.Clear();
        //Add each menu item
        menuOptions.Add("SUMMARY", "Summary");
        if (Debug.isDebugBuild || Application.isEditor) {
            menuOptions.Add("DEBUG", "Debug");
        }
        menuOptions.Add("SWITCH", "Switch");
        menuOptions.Add("ITEM", "Item");
        menuOptions.Add("CANCEL", "Cancel");
    }

    protected override IEnumerator executeSelection(bool exit = false) {
        if (exit) {
            Destroy(this);
            yield break;
        }
        hasControl = false;
        switch (menuOptions.Keys.ElementAt(arrowPos)) {
            case "SUMMARY":
                UIManager.showSummaryScreen(index);
                while (UIManager.isSummaryScreenShowing()) {
                    yield return null;
                }
                break;
            case "DEBUG":
                break;
            case "SWITCH":

                break;
            case "ITEM":

                break;
            case "CANCEL":
                Destroy(this);
                break;
        }
        hasControl = true;
        yield break;
    }


}
