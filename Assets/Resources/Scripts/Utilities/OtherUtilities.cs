﻿using UnityEngine;
using System.Collections;


public class ListExtensions<T> {

    //Initialize a list with a specified number of null values
    public static void initNullList(ref System.Collections.Generic.List<T> list, int num) {
        list = new System.Collections.Generic.List<T>();
        for (int i = 0; i < num; ++i) {
            list.Add(default(T));
        }
    }

    //Initialize a list with a specified number of specified values
    public static void initNullList(ref System.Collections.Generic.List<T> list, int num, T t) {
        list = new System.Collections.Generic.List<T>();
        for (int i = 0; i < num; ++i) {
            list.Add(t);
        }
    }

    public static void Swap(ref System.Collections.Generic.List<T> list, int pos1, int pos2) {
        T temp = list[pos1];
        list[pos1] = list[pos2];
        list[pos2] = temp;
    }

}

public class ReflectionExtensions{
    //Method by Ed S.: https://stackoverflow.com/questions/1196991/get-property-value-from-string-using-reflection-in-c-sharp
    public static object getPropValue(object src, string propName){
        return src.GetType().GetProperty(propName).GetValue(src, null);
    }
}
