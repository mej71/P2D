﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemScreenController : UIScreen {

    const int defaultTextSize = 27;
    const int defaultMainTextSize = 55;
    static readonly Color defaultTextColor = new Color(0, 0, 0);

    UIImageDef bagBG = new UIImageDef();
    UIImageDef bagBGRect = new UIImageDef();
    UIImageDef pocketIcon = new UIImageDef();
    UIImageDef bagIcon = new UIImageDef();
    UIImageDef bagIconHighlight = new UIImageDef();
    UIImageDef arrowLeft = new UIImageDef();
    UIImageDef arrowRight = new UIImageDef();
    UIImageDef bagBgTextRect = new UIImageDef();
    UIImageDef itemSelector = new UIImageDef();
    UIImageDef itemIcon = new UIImageDef();
    UIImageDef sliderArrow = new UIImageDef();
    UIImageDef sliderButton = new UIImageDef();
    UITextDef pocketLabel = new UITextDef(new Vector3(190, -380, 0), defaultMainTextSize, defaultTextColor, "");
    UITextDef itemList = new UITextDef(new Vector3(0, 0, 0), defaultMainTextSize, defaultTextColor, "");
    UITextDef quantityList = new UITextDef(new Vector3(0, 0, 0), defaultMainTextSize, defaultTextColor, "");
    UITextDef itemDescription = new UITextDef(new Vector3(0, 0, 0), 45, defaultTextColor, "");
    GameObject itemBagMenu;
    Dictionary<PBItems, int> curPocket;

    int pocketIndex = 0;
    int pocketSize = 0;
    int maxPocketIndex = 7;
    int arrowIndex = 0;
    int[] itemIndex = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
    int numDisplayItems = 7;
    Sprite[] bgIconSprites;
    Sprite[] bagIconHighlightSprites;
    Sprite[] arrowLeftSprites;
    Sprite[] arrowRightSprites;
    Color32[] bgColors = new Color32[8] {new Color32(216,128,168,255), new Color32(232,128,80,255), new Color32(224,160,56,255), new Color32(144,192,48,255),
        new Color32(48,184,88,255), new Color32(24,168,208,255), new Color32(72,128,240,255), new Color32(160,88,232,255) };

    string[] pocketLabelText = new string[8] { "Items", "Medicine", "Pokéballs", "TMs & HMs", "Berries", "Mail", "Battle Items", "Key Items" };

    // Use this for initialization
    void Start () {
        InitializeAllTextAndImageFields(this); 
        itemBagMenu = gameObject.transform.Find("itemBagMenu").gameObject;
        if (GamePlayer.getTrainer().getGender() == Gender.FEMALE) {
            bagIcon.i.sprite = Resources.Load("Graphics/UI/Item Screen/bagf", typeof(Sprite)) as Sprite;
            bagIconHighlightSprites = Resources.LoadAll<Sprite>("Graphics/UI/Item Screen/bagf_highlights");
        } else {
            bagIcon.i.sprite = Resources.Load("Graphics/UI/Item Screen/bag", typeof(Sprite)) as Sprite;
            bagIconHighlightSprites = Resources.LoadAll<Sprite>("Graphics/UI/Item Screen/bag_highlights");
        }
        itemList.t.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        itemList.t.GetComponent<RectTransform>().sizeDelta = new Vector2(500, 20 * 42);
        itemList.t.GetComponent<RectTransform>().anchoredPosition = new Vector3(390, -140, 0);
        quantityList.t.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        quantityList.t.alignment = TextAnchor.UpperRight;
        itemDescription.t.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        itemDescription.t.GetComponent<RectTransform>().sizeDelta = new Vector2(1500, 350);
        itemDescription.t.GetComponent<RectTransform>().anchoredPosition = new Vector3(190, -690, 0);
        itemDescription.t.horizontalOverflow = HorizontalWrapMode.Wrap;
        itemDescription.t.verticalOverflow = VerticalWrapMode.Truncate;

        if (bagBG.i.sprite == null) {
            bagBGRect.i.sprite = Resources.Load("Graphics/UI/Item Screen/bagbgcolor", typeof(Sprite)) as Sprite;
            bagBGRect.i.SetNativeSize();
            bagBGRect.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, -768, 1);
            bagBGRect.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 2);
            bagBgTextRect.i.sprite = Resources.Load("Graphics/UI/Item Screen/bagtextbox", typeof(Sprite)) as Sprite;
            bagBgTextRect.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(374, -544, 1);
            bagBgTextRect.i.SetNativeSize();
            bagBgTextRect.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 2);
            itemSelector.i.sprite = Resources.Load("Graphics/UI/Item Screen/bagSel", typeof(Sprite)) as Sprite;
            itemSelector.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(382, -105, 1);
            itemSelector.i.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(540, 88);
            bagBG.i.sprite = Resources.Load("Graphics/UI/Item Screen/bagbg", typeof(Sprite)) as Sprite;
            bagBG.i.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(512, 384);
            bagBG.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 2);
            bagBG.i.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, -868, 1);
            bagIcon.i.SetNativeSize();
            bagIcon.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 1);
            bagIcon.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(56, -340, 0);
            bagIconHighlight.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(56, -340, 0);
            bagIconHighlight.i.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector3(128, 128, 1);
            bagIconHighlight.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 2);
            itemIcon.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(48, -715, 0);
            itemIcon.i.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector3(48, 48, 1);
            itemIcon.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 2);
            arrowLeft.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-6, -260, 0);
            arrowRight.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(276, -260, 0);
            pocketLabel.t.alignment = TextAnchor.MiddleCenter;
            sliderArrow.i.sprite = Resources.Load("Graphics/UI/Item Screen/slider_arrows", typeof(Sprite)) as Sprite;
            sliderArrow.i.SetNativeSize();
            sliderArrow.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 1);
            sliderArrow.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(936, -530, 0);
            sliderButton.i.sprite = Resources.Load("Graphics/UI/Item Screen/bagSlider", typeof(Sprite)) as Sprite;
            sliderButton.i.SetNativeSize();
            sliderButton.i.gameObject.GetComponent<RectTransform>().localScale = new Vector3(2, 2, 1);
            sliderButton.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(944, -215, 0);
        }
       bgIconSprites = Resources.LoadAll<Sprite>("Graphics/UI/Item Screen/bagbg_icons");
       arrowLeftSprites = Resources.LoadAll<Sprite>("Graphics/UI/General/arrow_left");
       arrowRightSprites= Resources.LoadAll<Sprite>("Graphics/UI/General/arrow_right");
       InvokeRepeating("UpdateArrows", 0, 0.05f);
       updatePocket();
       updateList();
    }

    void updatePocket() {
        bagBGRect.i.color = bgColors[pocketIndex];
        pocketIcon.i.sprite = bgIconSprites[pocketIndex];
        pocketIcon.i.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector3(56, 56, 1);
        pocketIcon.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(7 + (pocketIndex*44), -507, 0);
        bagIconHighlight.i.sprite = bagIconHighlightSprites[pocketIndex];
        pocketLabel.t.text = pocketLabelText[pocketIndex];
        pocketSize = 0;
        itemList.t.text = "";
        quantityList.t.text = "";
        itemDescription.t.text = "";
        curPocket = GamePlayer.getTrainer().getBag().GetPocket((PBPockets)pocketIndex);
        if (curPocket.Count >= numDisplayItems) {
            sliderArrow.i.enabled = true;
            sliderButton.i.enabled = true;
        } else {
            sliderArrow.i.enabled = false;
            sliderButton.i.enabled = false;
        }
    }

    void updateList() {
        if (curPocket.Count>=numDisplayItems && itemIndex[pocketIndex]>0) {
            itemList.t.GetComponent<RectTransform>().anchoredPosition = new Vector3(390, -95, 0);
            quantityList.t.GetComponent<RectTransform>().anchoredPosition = new Vector3(900, -95, 0);
            itemSelector.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(382, -130, 1);
        } else {
            itemList.t.GetComponent<RectTransform>().anchoredPosition = new Vector3(390, -140, 0);
            quantityList.t.GetComponent<RectTransform>().anchoredPosition = new Vector3(900, -140, 0);
            itemSelector.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(382, -105, 1);
        }
        if (itemIndex[pocketIndex]+numDisplayItems > curPocket.Count + 1  && itemIndex[pocketIndex] > 0) {
            int selectPos = 0;
            int adjustPos = 0;
            if (itemIndex[pocketIndex] + numDisplayItems >= curPocket.Count && itemIndex[pocketIndex] > 0) {
                if (curPocket.Count < numDisplayItems) {
                    selectPos = itemIndex[pocketIndex]%numDisplayItems;
                    adjustPos += 25;
                } else {
                    selectPos = numDisplayItems - (curPocket.Count + 1 - itemIndex[pocketIndex]);
                }                
            } 
            itemSelector.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(382, -130 - (68* selectPos) + adjustPos, 1);
        }
        itemList.t.text = "";
        quantityList.t.text = "";
        for (int i = 0; i<curPocket.Count; ++i) {
            if (i > itemIndex[pocketIndex] + numDisplayItems) {
                break;
            }
            if (i + numDisplayItems >= curPocket.Count || i >= itemIndex[pocketIndex] - 1) {
                itemList.t.text += " " + ItemManager.getItems().Find((x => x.itemEnum == curPocket.ElementAt(i).Key.ToString())).itemName + "\n";
                quantityList.t.text += "x" + curPocket.ElementAt(i).Value+"\n";
            } 
        }
        itemList.t.text += " Close Bag";
        if (itemIndex[pocketIndex] == curPocket.Count) {
            itemIcon.i.sprite = Resources.Load("Graphics/Icons/Items/itemBack", typeof(Sprite)) as Sprite;
            itemDescription.t.text = "Exit the Bag";
        } else {
            Item item = ItemManager.getItems().Find((x => x.itemEnum == curPocket.ElementAt(itemIndex[pocketIndex]).Key.ToString()));
            itemDescription.t.text = item.itemDesc;
            itemIcon.i.sprite = Resources.Load("Graphics/Icons/Items/item" + ((int)System.Enum.Parse(typeof(PBItems), item.itemEnum.ToString())).ToString("000"), typeof(Sprite)) as Sprite;
        }
        sliderButton.i.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(944, -215 - ((itemIndex[pocketIndex]/(float)curPocket.Count) * 235), 0);
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.X)) {
            Destroy(this);
        } else if (Input.GetKeyDown(KeyCode.D)) {
            ++pocketIndex;
            if (pocketIndex> maxPocketIndex) {
                pocketIndex = 0;
            }
            updatePocket();
            updateList();
        } else if (Input.GetKeyDown(KeyCode.A)) {
            --pocketIndex;
            if (pocketIndex < 0) {
                pocketIndex = maxPocketIndex;
            }
            updatePocket();
            updateList();
        } else if (Input.GetKeyDown(KeyCode.W)) {
            --itemIndex[pocketIndex];
            if (itemIndex[pocketIndex]<0) {
                itemIndex[pocketIndex] = curPocket.Count;
            }
            updateList();
        } else if (Input.GetKeyDown(KeyCode.S)) {
            ++itemIndex[pocketIndex];
            if (itemIndex[pocketIndex] > curPocket.Count) {
                itemIndex[pocketIndex] = 0;
            }
            updateList();
        } else if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.C)) {
            useItemMenu();
        }
    }

    void OnDestroy() {
        ChangeAllImageAndTextEnableValue(this, false);
        CancelInvoke("UpdateArrows");
    }

    void useItemMenu() {
        if (itemIndex[pocketIndex]==curPocket.Count) {
            Destroy(this);
        } else {
            //show item menu
        }
    }

    private void UpdateArrows() {
        arrowLeft.i.sprite = arrowLeftSprites[arrowIndex];
        arrowRight.i.sprite = arrowRightSprites[arrowIndex];
        ++arrowIndex;
        if (arrowIndex == arrowLeftSprites.Count()) {
            arrowIndex = 0;
        }
    }

}
