using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;


//Use these for explicit checking in events
public enum GameVariableTypeChoice {
    NONE,
    STRING,
    INT,
    FLOAT,
    PBPOKEMON,
    PBMOVES,
    PBITEMS
}

public class GameVariablesManager : MonoBehaviour {

    //Define default variable values here, along with their names
    public static Dictionary<string, object> GameVariables = new Dictionary<string, object>{
        {"Temp Pokemon Choice", new Pokemon(PBPokemon.NONE, 1)},
        {"Temp Move Choice", PBMoves.NONE},
        {"Temp String Choice", ""},
        {"Temp Int Choice", -1},
        {"Starter Choice", -1}
    };
    private static bool _HasLoadedSwitches = false;

    public static void LoadVariables() {
        //Will load this from save file instead later
        TextAsset asset = Resources.Load("Data/GameVariables", typeof(TextAsset)) as TextAsset;
        byte[] bytes = asset.bytes;
        Stream s = new MemoryStream(bytes);
        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        Dictionary<string, object> tempGVars = (Dictionary<string, object>)binaryFormatter.Deserialize(s);
        //After loading from file, ensure any new switches are added along with their default values
        foreach (KeyValuePair<string, object> gvar in tempGVars) {
            if (GameVariables.ContainsKey(gvar.Key)) {
                GameVariables[gvar.Key] = gvar.Value;
            }
        }
        _HasLoadedSwitches = true;
    }

    public static void SaveVariables() {
        using (Stream stream = File.Open("Assets/Resources/Data/GameVariables.txt", FileMode.Create)) {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(stream, GameVariables);
        }
    }

    public static bool IsEquivalent<T>(string varName, T item) {
        if (!_HasLoadedSwitches) {
            LoadVariables();
        }
        if (GameVariables.ContainsKey(varName) && GameVariables[varName] is T) {
            if (EqualityComparer<T>.Default.Equals(item, (T)GameVariables[varName])) {
                return true;
            }
        } 
        return false;
    }

    public static void SetVariable<T>(string varName, T item) {
        if (!_HasLoadedSwitches) {
            LoadVariables();
        }
        if (GameVariables.ContainsKey(varName) && GameVariables[varName] is T) {
            GameVariables[varName] = item;
        } 
    }

}
