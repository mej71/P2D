﻿using UnityEngine;
using System.Collections;

public class PokemonSpriteManager : MonoBehaviour {

    static System.Collections.Generic.List<string> pokemonIcons = new System.Collections.Generic.List<string>();

    public static Sprite[] getPokemonSprite(Pokemon poke, bool back=false) {
        if (pokemonIcons.Count == 0) {
            fetchIconSprites();
        }
        string spriteName = poke.getSpeciesID().ToString() + ((poke.gender == Gender.FEMALE) ? "_f" : "");
        spriteName += (back) ? "_b" : "";
        if (pokemonIcons.Contains(spriteName)) {
            return Resources.LoadAll<Sprite>("Graphics/Battlers/" + spriteName);
        } else {
            spriteName = poke.getSpeciesID().ToString();
            return Resources.LoadAll<Sprite>("Graphics/Battlers/" + spriteName);
        }
    }

    public static Sprite[] getPokemonSprite(PBPokemon poke, bool back = false, Gender g = Gender.MALE) {
        if (pokemonIcons.Count == 0) {
            fetchIconSprites();
        }
        string spriteName = poke.ToString() + ((g == Gender.FEMALE) ? "_f" : "");
        spriteName += (back) ? "_b" : "";
        return Resources.LoadAll<Sprite>("Graphics/Battlers/" + spriteName);
    }

    public static void fetchIconSprites() {
        Object[] tempIcons = (Resources.LoadAll("Graphics/Battlers/", typeof(Sprite)));
        foreach (Object obj in tempIcons) {
            pokemonIcons.Add(obj.name);
        }
    }
}

//Animated sprites not possible due to file size and limitations of Unity
//If possible, create and move to joint based animations in the future
public class PokemonSprite : MonoBehaviour {

    public void updatePokemonSprite(Pokemon p, bool back = false) {
        gameObject.GetComponent<UnityEngine.UI.Image>().sprite = PokemonSpriteManager.getPokemonSprite(p, back)[0];
    }
    
    public void updatePokemonSprite(PBPokemon p, bool back = false, Gender g = Gender.MALE) {
        gameObject.GetComponent<UnityEngine.UI.Image>().sprite = PokemonSpriteManager.getPokemonSprite(p, back, g)[0];
    }
}
