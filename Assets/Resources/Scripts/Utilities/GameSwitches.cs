using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class GameSwitch {
    public string SwitchName;
    public bool DefaultValue;
    public bool CurrentValue;
    public GameSwitch(string sn, bool dv = false) {
        SwitchName = sn;
        DefaultValue = dv;
        CurrentValue = dv;
    }
}


public class GameSwitchManager : MonoBehaviour {

    public static List<GameSwitch> GameSwitches = new List<GameSwitch>{
          { new GameSwitch("Has Pokedex?", false)},
          { new GameSwitch("Has PokeGear?")},
          { new GameSwitch("Has Starter?")},
          { new GameSwitch("Has Running Shoes?")},
    };
    private static bool _HasLoadedSwitches = false;

    public static void loadSwitches() {
        TextAsset asset = Resources.Load("Data/GameSwitches", typeof(TextAsset)) as TextAsset;
        byte[] bytes = asset.bytes;
        Stream s = new MemoryStream(bytes);
        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        List<GameSwitch> tempSwitches = (List<GameSwitch>)binaryFormatter.Deserialize(s);
        //After loading from file, ensure any new switches are added along with their default values
        foreach (GameSwitch gswitch in tempSwitches) {
            if (GameSwitches.Exists( x => x.SwitchName == gswitch.SwitchName)) {
                GameSwitches[GameSwitches.FindIndex(x => x.SwitchName == gswitch.SwitchName)].CurrentValue = gswitch.CurrentValue;
            }
        }
        _HasLoadedSwitches = true;
    }

    public static void saveSwitches() {
        using (Stream stream = File.Open("Assets/Resources/Data/GameSwitches.txt", FileMode.Create)) {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(stream, GameSwitches);
        }
    }

    public static bool checkSwitch(string switchName) {
        if (!_HasLoadedSwitches) {
            loadSwitches();
        }
        if (GameSwitches.Exists(x => x.SwitchName == switchName)) {
            return GameSwitches.FirstOrDefault(x => x.SwitchName == switchName).CurrentValue;
        } else {
        //switch does not exist
        return false;
        }
    }

    public static void setSwitch(string switchName, bool value) {
        if (!_HasLoadedSwitches) {
            loadSwitches();
        }
        if (GameSwitches.Exists(x => x.SwitchName == switchName)) {
        GameSwitches.FirstOrDefault(x => x.SwitchName == switchName).CurrentValue = value;
        } else {
            //switch does not exist
            //don't add switch because you could just be adding a typo'd switch to the save game
            //alert the user instead 
            Debug.Log("The switch " + switchName + " has not been defined.  Make sure you define it before using it.");
        }
        saveSwitches();
        return;
    }
}
