﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class OptionsScreenController : UIScreen
{
    const int defaultTextSize = 25;
    static Color32 defaultTextColor = new Color32(0, 0, 0, 255);
    public static float xScale = (float) 8/400;
    public static float yScale = (float) 8/320;
    public static bool isDestroyed = false;
    UIImageDef optionsBG = new UIImageDef();
    UIImageDef optionsTitleBox = new UIImageDef();
    UIImageDef speechFrame = new UIImageDef();

    UITextDef optionsTitleText = new UITextDef(new Vector3(18, -20), defaultTextSize, defaultTextColor, "Options");
    GameObject optionsMenu; 

    void Start()
    {
        InitializeAllTextAndImageFields(this);
        
        optionsBG.i.GetComponent<RectTransform>().localScale = new Vector3(128, 96, 10);
        optionsTitleBox.i.GetComponent<RectTransform>().localPosition = Vector3.zero;
        optionsTitleBox.i.GetComponent<RectTransform>().localScale = new Vector3((float) 8/400, (float) 8/320, 2);
        optionsTitleBox.i.GetComponent<RectTransform>().sizeDelta = new Vector3(400, 60, 1);
        speechFrame.i.GetComponent<RectTransform>().localPosition =new Vector3 (0, -6.25f);
        speechFrame.i.GetComponent<RectTransform>().localScale = new Vector3(xScale, yScale, 2);
        speechFrame.i.GetComponent<RectTransform>().sizeDelta = new Vector3(400, 70, 1);
        optionsMenu = gameObject.transform.Find("optionsMenu").gameObject;
        StartCoroutine(showOptionsMenu());
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.X))
        {
            Destroy(this);
        }
    }

    void OnDestroy()
    {
        ChangeAllImageAndTextEnableValue(this, false);
        isDestroyed = true;
    }
    
    IEnumerator showOptionsMenu() {
        hasControl = false;
        optionsMenu.AddComponent<OptionsMenuController>().initializeMenuText(PlayerOptions.getOptionsList(), optionsMenu, optionsTitleBox, speechFrame);
        while (optionsMenu.GetComponent<OptionsMenuController>() != null) {
            yield return null;
        }
        hasControl = true;  
        yield break;
    }
}

public class OptionsMenuController : ChoiceController {

    protected new Dictionary<string, PlayerOptions> menuOptions = new Dictionary<string, PlayerOptions>();
    protected bool ScrollingEnabled;    // Is scrolling enabled for this list? (true if number of items in list > MaxLineHeight)
    protected int MaxLineHeight;        // The max number of lines to display before scrolling.
    protected int MinOptionIndex;       // The first option currently being displayed.
    protected int MaxOptionIndex;       // The last option currently being displayed.
    protected List<PlayerOptions> Options;     // List of Options in the menu.
    protected string SelectedOption;    // The option selected by the player.
    protected UIImageDef titleBox, speechFrame;
    // Use this for initialization
    new void Start()
    {
        arrowLocation = "Graphics/Pictures/arrows";
        imageBoxName = "Options Skin";
        imageArrowName = "Options Selector";
        textObjectName = "Options Text";
        MaxLineHeight = 6;
        //Set desired positioning
        base.Start();
    }
    // Call this when creating the menu to initialize the list.
    public void initializeMenuText(List<PlayerOptions> newOptions, GameObject go, UIImageDef img, UIImageDef speech)
    {
        GameObject optionsMenu = go;
        titleBox = img;
        speechFrame = speech;
        // Initialize object if it has not been initailized yet...?
        if (imageBox == null)
        {
            arrowLocation = "Graphics/Pictures/arrows";
            imageBoxName = "Options Skin";
            imageArrowName = "Options Selector";
            textObjectName = "Options Text";
            ScrollingEnabled = false;
            MaxLineHeight = 6;
            MinOptionIndex = 0;
            MaxOptionIndex = 1;
        }

        Options = newOptions;
        // Determine if scrolling needs to be enabled.
        if (Options.Count > MaxLineHeight)
        {
            ScrollingEnabled = true;
        }

        menuOptions.Clear();

        MinOptionIndex = 0;
        MaxOptionIndex = (MaxLineHeight - 1 < Options.Count() ? MaxLineHeight - 1 : Options.Count());

        // Add each menu item.
        // Adds all strings in Options[] to menuOptions. They are given a key
        // equal to their index in the Options list.
        for (int i = MinOptionIndex; i <= MaxOptionIndex; i++)
        {
            menuOptions.Add(i.ToString(), Options[i]);
        }
        OptionsGUI.initializeGUIOptions(menuOptions);
        optionsMenu.AddComponent<OptionsGUI>();
        //Initialize parent method to set the variables and assets below.
        base.Start();

        // Set desired positioning.
        arrowHeight = arrowWidth = 40;
        menuSpacing = 90;
        arrowSpacing = 65;
        setWidth = 400;
        setHeight = 340;
        widthScale = 2.56f;
        heightScale = 1.3f;
        xPos = 0;
        yPos = -700;// + height of text box at bot of screen?
                    // Need to check if the text area is higher / wider than the available screen space.
        arrowStartYPos = yPos + (5* menuSpacing) - arrowSpacing / 2 - 50;
        positionChoiceBoxElements();
    }
    new void Update() {
		if (hasControl && Options != null) {
            bool arrowPosChanged = false;
            if (Input.GetKeyDown(KeyCode.S)) { //Down
                ++arrowPos;
                if (arrowPos == Options.Count) {
                    arrowPos = 0;
                }
                arrowPosChanged = true;
                AudioController.playSE("Choose.mp3");
            } else if (Input.GetKeyDown(KeyCode.W)) { //Up
                --arrowPos;
                if (arrowPos < 0) {
                    arrowPos = Options.Count - 1;
                }
                arrowPosChanged = true;
                AudioController.playSE("Choose.mp3");
            } else if (Input.GetKeyDown(KeyCode.D)) { //Right
                menuOptions[arrowPos.ToString()].next();
                StartCoroutine(executeSelection());
                AudioController.playSE("Choose.mp3");
            } else if (Input.GetKeyDown(KeyCode.A)) { //Left
                menuOptions[arrowPos.ToString()].prev();
                StartCoroutine(executeSelection());
                AudioController.playSE("Choose.mp3");
            } else if (Input.GetKeyDown(KeyCode.X)) { //Exit
                StartCoroutine(executeSelection(true));
            } else if (Input.GetMouseButton(0)) { //Left click to drag sliders when the arrow position is on the intended option.
                StartCoroutine(executeSelection());
            }
            if (arrowPosChanged) {       
				if (ScrollingEnabled) {  
					// Increment the list Options if the arrow position exceeds the indices
					// of the currently displayed Options.
					if (arrowPos < MinOptionIndex) {
						MinOptionIndex = arrowPos;
						// Make sure MaxOptionIndex does not exceed the length of the list.
						MaxOptionIndex = (arrowPos + (MaxLineHeight - 1) < Options.Count() ? arrowPos + (MaxLineHeight - 1) : Options.Count());
						updateMenuText();
					}
					else if (arrowPos > MaxOptionIndex) {
						MaxOptionIndex = arrowPos;
						// Make sure MinOptionIndex is not negative.
						MinOptionIndex = (MaxOptionIndex - (MaxLineHeight - 1) < 0 ? 0 : MaxOptionIndex - (MaxLineHeight - 1));
						updateMenuText();
					}
				}
                imageArrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+30, arrowStartYPos - ((arrowPos - MinOptionIndex) * arrowSpacing), 0);
            }
        }
	}
    protected void updateMenuText() {
		menuOptions.Clear();
		for (int i = MinOptionIndex; i <= MaxOptionIndex; i++) { //Adjust the list when scrolling is enabled to show the new min and max indexs i.e index 0 to 6 -> index 1 to 7
			menuOptions.Add(i.ToString(), Options[i]);
		}
        OptionsGUI.initializeGUIOptions(menuOptions);	
		positionChoiceBoxElements();
	}

    protected override void positionChoiceBoxElements() {
        imageBox.GetComponent<RectTransform>().localScale = widthScale != 0 || heightScale != 0 ? new Vector3(widthScale, heightScale, 1) : new Vector3(1, 1, 1); 
        imageBox.GetComponent<RectTransform>().sizeDelta = new Vector2(setWidth, setHeight);
        imageBox.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos, yPos, 0);
        imageArrow.GetComponent<RectTransform>().sizeDelta = new Vector2(arrowWidth, arrowHeight);
        imageArrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+30, arrowStartYPos, 0);
        textObject.GetComponent<RectTransform>().sizeDelta = new Vector2(setWidth, setHeight+60);
        textObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+75, yPos+10, 0);
        ///Fill menu text
        textObject.GetComponent<Text>().enabled = false;
        textObject.GetComponent<Text>().text = "";
        textObject.GetComponent<Text>().color = new Color32(191, 121, 0, 255);
        for (int i = MinOptionIndex; i <= MaxOptionIndex - 1; i++) {
            textObject.GetComponent<Text>().text += ReflectionExtensions.getPropValue(menuOptions[i.ToString()], "Name") + "\n";
        }
        textObject.GetComponent<Text>().text += ReflectionExtensions.getPropValue(menuOptions[MaxOptionIndex.ToString()], "Name");
        textObject.GetComponent<Text>().enabled = true;
    }
    protected void updateUIImages(){
        titleBox.i.GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load("Graphics/UI/WindowSkins/" + PlayerOptions.curMenuFrame, typeof(Sprite)) as Sprite;
        speechFrame.i.GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load("Graphics/UI/WindowSkins/" + PlayerOptions.curSpeechFrame, typeof(Sprite)) as Sprite;
    }
    protected override IEnumerator executeSelection(bool exit = false)
    {
        if (exit)
        {
            Destroy(this);
            yield break;
        }
        hasControl = false;
        List<string> tempList = new List<string>();
        switch ((string) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "Name")) {
            case "BGM Volume":
                PlayerOptions.BgmVolume = (float) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                if(AudioController.audioSourceBGM != null){
                    AudioController.audioSourceBGM.volume = PlayerOptions.BgmVolume/100.0f;
                }
                break;
            case "SE Volume":
                PlayerOptions.SfxVolume = (float)ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                break;
            case "Text Speed":
                PlayerOptions.textSpeed = (int)ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                break;
            case "Battle Effects":
                PlayerOptions.battleEffects = (PlayerOptions.BattleEffects) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                break;
            case "Battle Style":
                PlayerOptions.battleStyle = (PlayerOptions.BattleStyle) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                break;
            case "Running Key":
                PlayerOptions.runningKey = (PlayerOptions.RunningKey) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                break;
            case "Speech Frame":
                tempList.Clear();
                tempList = (List<string>) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "Values");
                PlayerOptions.curSpeechFrame = tempList[(int)ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "Index")];
                updateUIImages();
                break;
            case "Menu Frame":
                tempList.Clear();
                tempList = (List<string>) ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "Values");
                PlayerOptions.curMenuFrame = tempList[(int)ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "Index")];
                updateUIImages();
                updateImageBox();
                break;
            case "Font Style":
                PlayerOptions.curFont = (PlayerOptions.FontStyle)ReflectionExtensions.getPropValue(menuOptions[arrowPos.ToString()], "CurrentValue");
                updateFont();
                break;                    
        }
        hasControl = true;
        yield break;
    }
}
public class OptionsGUI : MonoBehaviour{
	Vector2 nativeSize = new Vector2(640, 480);
	public static Dictionary<int, SliderOption> Sliders = new Dictionary<int, SliderOption>();
	public static Dictionary<int, EnumOption> Enums = new Dictionary<int, EnumOption>();
	public static Dictionary<int, NumberOption> NumsList = new Dictionary<int, NumberOption>();
	public static int guiCount;
	public static void initializeGUIOptions(Dictionary<string, PlayerOptions> optList){
		Sliders.Clear();
		Enums.Clear();
		NumsList.Clear();
		guiCount = optList.Count;
		for (int i = 0; i < optList.Count; i++)
		{
			if(optList.Values.ElementAt(i) is SliderOption){
				Sliders.Add(i, (SliderOption)optList.Values.ElementAt(i));
			}
			else if(optList.Values.ElementAt(i) is EnumOption){
				Enums.Add(i, (EnumOption)optList.Values.ElementAt(i));
			}
			else if(optList.Values.ElementAt(i) is NumberOption){
				NumsList.Add(i, (NumberOption)optList.Values.ElementAt(i));
			}
		}
	}
	void OnGUI() {
		Vector3 scale = new Vector3 (Screen.width / nativeSize.x, Screen.height / nativeSize.y, 1.0f);
        GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, scale);
		GUI.skin.horizontalSlider.fixedHeight = 15;
		GUI.skin.horizontalSliderThumb.fixedHeight = GUI.skin.horizontalSlider.fixedHeight;
		GUI.skin.horizontalSliderThumb.fixedWidth = GUI.skin.horizontalSlider.fixedHeight;
		GUIStyle style = new GUIStyle();
		style.normal.textColor = Color.black;
		style.fontSize = 35;
		style.alignment = TextAnchor.UpperLeft;
		style.font = FontManager.getFont(PlayerOptions.getFontStyle());
		// foreach(KeyValuePair<int, SliderOption> sl in Sliders){
		// 	sl.Value.CurrentValue = GUI.HorizontalSlider(new Rect(450, 80 + (sl.Key*75), 100, 1000), Mathf.Round(sl.Value.CurrentValue), sl.Value.Min, sl.Value.Max);
		// }
		for (int i = guiCount-1; i >=0; i--) //Previously, iterating forwards would allow only the first slider to be controllable, however now iterating in reverse allows all sliders to be controllable. (Raycast GUI ordering issue?)
		{
			if(Sliders.ContainsKey(i) && Sliders.Count != 0){
				Sliders[i].CurrentValue = GUI.HorizontalSlider(new Rect(450, 130 + (i*40), 100, 1000), Mathf.Round(Sliders[i].CurrentValue), Sliders[i].Min, Sliders[i].Max);
				GUI.Label(new Rect(560, 125 + (i*40), 100, 1000), Sliders[i].CurrentValue.ToString(), style);
			}
			if(Enums.ContainsKey(i) && Enums.Count != 0){			
				GUI.Label(new Rect(450, 125 + (i*40), 100, 1000), Enum.GetName(typeof(PlayerOptions).GetNestedType(Enums[i].EnumName), Enums[i].CurrentValue).ToString(), style);
			}
			if(NumsList.ContainsKey(i) && NumsList.Count != 0){			
				GUI.Label(new Rect(450, 125 + (i*40), 100, 1000), (NumsList[i].Index + 1).ToString(), style);
			}
		}
		if (Input.GetKey(KeyCode.X))
        {
            Destroy(this);
        }
	}
}