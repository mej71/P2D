﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class UIManager : MonoBehaviour {

    public static GameObject uiManager, messageSystem, pauseMenu, textOptionMenu, partyScreen;
    public static GameObject summaryScreen, itemBagScreen, trainerCardScreen, optionsScreen;
    public static bool inMenu = false;

    #region Message System
    public static void displayText(string textToDisplay) {
        if (uiManager == null) {
            setUpUIManager();
        }
        if (messageSystem == null) {
            UIManager.generateMessageSystem();
        }
        if (messageSystem.GetComponent<DisplayText>() == null) {
            messageSystem.AddComponent<DisplayText>();
        }
        messageSystem.gameObject.SetActive(true);
        messageSystem.GetComponent<DisplayText>().SetText(textToDisplay);
    }

    public static bool isTextDisplaying() {
        if (uiManager == null) {
            setUpUIManager();
        }
        if (messageSystem == null) {
            UIManager.generateMessageSystem();
        }
        return messageSystem.GetComponent<DisplayText>() == null;
    }

    //Generate Message System objects via code
    public static void generateMessageSystem() {
        //Create Message System object
        messageSystem = createTopUIObject("Message System", uiManager);
        //Create Message Skin object
        GameObject messageSkin = createLowerUIImageObject("Message Skin", messageSystem, Vector3.zero, Vector2.zero, false, "Graphics/UI/WindowSkins/speech hgss 2", UnityEngine.UI.Image.Type.Sliced);
        //Create Message Text object
        GameObject messageText = createLowerUITextObject("Message Text", messageSystem, new Vector3(40, 115, 0), new Vector2(860, 96), "Power Clear", 42, 1.7f, Vector2.zero, new Vector2(0, 0.5f), Color.black);
    }
    #endregion Message System

    #region Pause Menu
    public static void showPauseMenu() {
        if (uiManager == null) {
            setUpUIManager();
        }
        if (pauseMenu == null) {
            UIManager.generatePauseMenu();
        }
        pauseMenu.AddComponent<PauseMenuController>();
    }

    //Generate Pause Menu Objects via code
    public static void generatePauseMenu() {
        //Create Pause Menu object
        pauseMenu = createTopUIObject("Pause Menu", uiManager);
        //Create Pause Skin object
        GameObject pauseSkin = createLowerUIImageObject("Pause Skin", pauseMenu, Vector3.zero, Vector2.zero, false, "Graphics/WindowSkins/choice 1", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), Vector2.zero);
        //Create Message BG object
        GameObject pauseText = createLowerUITextObject("Pause Text", pauseMenu, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
        //Create Pause Arrow object
        GameObject pauseArrow = createLowerUIImageObject("Pause Selector", pauseMenu, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), Vector2.zero);
    }
    #endregion Pause Menu

    #region Text Option Menu
    public static void showTextOptionMenu(List<string> options) {
        if (uiManager == null) {
            setUpUIManager();
        }
        if (textOptionMenu == null) {
            UIManager.generateTextOptionMenu();
        }
        textOptionMenu.AddComponent<TextOptionMenuController>();
        textOptionMenu.GetComponent<TextOptionMenuController>().initializeMenuText(options);
    }

    // Generate Text Option Menu Objects via code. Uses same skin as Pause Menu.
    public static void generateTextOptionMenu() {
        // Create Text Option Menu parent object
        textOptionMenu = createTopUIObject("Text Option Menu", uiManager);
        // Create Text Option Menu skin object
        GameObject textOptionSkin = createLowerUIImageObject("Text Option Skin", textOptionMenu, Vector3.zero, Vector2.zero, false, "Graphics/WindowSkins/choice 1", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), Vector2.zero);
        // Create Message BG object
        GameObject textOptionText = createLowerUITextObject("Text Option Text", textOptionMenu, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
        // Create Text Option Arrow object
        GameObject textOptionArrow = createLowerUIImageObject("Text Option Selector", textOptionMenu, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), Vector2.zero);
    }
    #endregion Text Option Menu

    #region Party Screen
    public static void showPartyScreen() {
        if (uiManager == null) {
            setUpUIManager();
        }
        if (partyScreen == null) {
            UIManager.generatePartyScreen();
        }
        partyScreen.AddComponent<PartyScreenController>().setHelpText("Choose a Pokémon");
    }

    //Generate Party Screen with code
    public static void generatePartyScreen() {
        //Create Party Screen object
        partyScreen = createTopUIObject("Party Screen", uiManager);
        partyScreen.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //Create party background object
        GameObject partyBG = createLowerUIImageObject("bgImage", partyScreen, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partybg", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
        GameObject partyPanel, panelBallIcon, panelPokemonIcon, panelSel, cancelText;
        GameObject hpBarBase, hpBarBg, hpBarOutline, hpBar, nameText, genderText;
        GameObject levelText, hpText;
        for (int i = 0; i < 7; ++i) {
            //create panel
            if (i > 0 && i < 6) {
                partyPanel = createLowerUIImageObject("partyPanels_" + i, partyBG, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyPanelRect", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
            } else if (i == 6) {
                partyPanel = createLowerUIImageObject("partyPanels_" + i, partyBG, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyCancel", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
                cancelText = createLowerUITextObject("cancelTextObj", partyPanel, Vector3.zero, Vector2.zero, new Vector2(0, 1), new Vector2(0, 1), Color.white);
            } else {
                partyPanel = createLowerUIImageObject("partyPanels_" + i, partyBG, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyPanelRound", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
            }
            //create select panel
            if (i > 0 && i < 6) {
                panelSel = createLowerUIImageObject("selectPanels_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyPanelRectSel", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            } else if (i == 6) {
                panelSel = createLowerUIImageObject("selectPanels_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyCancelSel", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            } else {
                panelSel = createLowerUIImageObject("selectPanels_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyPanelRoundSel", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            }
            if (i == 6) {  //make sure to create the cancel panel, but stop before creating a 7th of anything else
                break;
            }
            //create ball icon
            panelBallIcon = createLowerUIImageObject("ballIcons_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyBall", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            //create ball icon
            panelPokemonIcon = createLowerUIImageObject("pokemonIcons_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1));
            //HP Bar bg
            hpBarBg = createLowerUIImageObject("hpBarBgs_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyHPBar_bg", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            hpBarBg.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            //HP Bar Outline
            hpBarOutline = createLowerUIImageObject("hpBarOutlines_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyHPBar_Outline", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            hpBarOutline.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            //Add actual HP bar
            hpBar = createLowerUIImageObject("hpBars_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            //HP Bar base
            hpBarBase = createLowerUIImageObject("hpBarBases_" + i, partyPanel, Vector3.zero, Vector2.zero, false, "Graphics/UI/Party Screen/partyHPBar", UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            hpBarBase.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            //Name Text
            nameText = createLowerUITextObject("nameText_" + i, partyPanel, Vector3.zero, Vector2.zero, new Vector2(0, 1), new Vector2(0, 1), Color.white);
            //Gender Text
            genderText = createLowerUITextObject("genderText_" + i, partyPanel, Vector3.zero, Vector2.zero, new Vector2(0, 1), new Vector2(0, 1), Color.white);
            //Level Text
            levelText = createLowerUITextObject("levelText_" + i, partyPanel, Vector3.zero, Vector2.zero, new Vector2(0, 1), new Vector2(0, 1), Color.white);
            //HP Text
            hpText = createLowerUITextObject("hpText_" + i, partyPanel, Vector3.zero, Vector2.zero, new Vector2(0, 1), new Vector2(0, 1), Color.white);
        }
        //Create Help Text Skin object
        GameObject helpTextSkin = createLowerUIImageObject("helpTextImg", partyScreen, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 0), new Vector2(0, 0));
        //Help Text
        GameObject helpText = createLowerUITextObject("helpTextObj", helpTextSkin, Vector3.zero, Vector2.zero, new Vector2(0, 0), new Vector2(0, 0), Color.black);
        //Create Party Menu object
        GameObject partyMenu = createTopUIObject("partyMenu", partyScreen);
        partyMenu.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        partyMenu.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        partyMenu.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        //Create Party Skin object
        GameObject partyMenuSkin = createLowerUIImageObject("Party Menu Skin", partyMenu, Vector3.zero, Vector2.zero, false, "Graphics/WindowSkins/choice 1", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 0));
        //Create Party BG object
        GameObject partyMenuText = createLowerUITextObject("Party Menu Text", partyMenu, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), new Vector2(0, 0), Color.black);
        //Create Party Arrow object
        GameObject partyMenuArrow = createLowerUIImageObject("Party Menu Selector", partyMenu, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 0));
    }
    #endregion Party Screen

    #region Summary Screen
    public static void showSummaryScreen(int index = 0) { 
        if (uiManager==null) {
            setUpUIManager();
        }
        if (summaryScreen==null) {
            UIManager.generateSummaryScreen();
        }
        summaryScreen.AddComponent<SummaryScreenController>().setPartyIndex(index);
    }

    public static void generateSummaryScreen() {
        //Create Summary Screen object
        summaryScreen = createTopUIObject("Summary Screen", uiManager);
        summaryScreen.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //All pages
        GameObject summaryPage, summaryTitleText, markingsText, itemLabel, itemText;
        GameObject ballImage, nameText, genderText, levelText, speciesImage, shinyImage;
        //Page 1
        System.Collections.Generic.List<string> page1ImageList = new System.Collections.Generic.List<string>(new string[] { "monType1", "monType2", "toNextExpBar" });
        System.Collections.Generic.List<string> page1TextList = new System.Collections.Generic.List<string>(new string[] { "dexNoLabel", "dexNoText", "speciesLabel", "speciesText", "typeLabel",
                    "otLabel", "otText", "idLabel", "idText", "curExpLabel", "curExpText", "toNextExpLabel", "toNextExpText"});
        //Page 2
        System.Collections.Generic.List<string> page2TextList = new System.Collections.Generic.List<string>(new string[] { "natureText", "aqDateText", "aqLocText", "metText", "traitText" });
        //Page 3
        System.Collections.Generic.List<string> page3ImageList = new System.Collections.Generic.List<string>(new string[] { "hpBar" });
        System.Collections.Generic.List<string> page3TextList = new System.Collections.Generic.List<string>(new string[] { "hpLabel", "hpText", "atkLabel", "atkText", "defLabel",
                                          "defText", "spatkLabel", "spatkText", "spdefLabel", "spdefText", "speedLabel", "speedText", "abilLabel", "abilNameText", "abilDescText"});
        //Page 4
        GameObject moveTypeImage, movePosImage, moveNameText, ppLabel, ppText;
        //Page 5
        GameObject ribbonImage, ribbonNoLabel, ribbonNoText;
        for (int i = 1; i < 6; ++i) {
            summaryPage = createLowerUIImageObject("summaryBGs_" + (i - 1), summaryScreen, Vector3.zero, Vector2.zero, false, "Graphics/UI/Summary Screen/summary" + i, UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), new Vector2(0, 1), true);
            switch (i) {
                case 1:
                    createGenericListOfLowerUITextObjects(page1TextList, summaryPage, "Power Clear", 42, 2.0f, Color.black, new Vector2(0, 1), Vector2.zero);
                    createGenericListOfLowerUIImageObjects(page1ImageList, summaryPage, UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), Vector2.zero);
                    break;
                case 2:
                    createGenericListOfLowerUITextObjects(page2TextList, summaryPage, "Power Clear", 42, 2.0f, Color.black, new Vector2(0, 1), Vector2.zero);
                    break;
                case 3:
                    createGenericListOfLowerUITextObjects(page3TextList, summaryPage, "Power Clear", 42, 2.0f, Color.black, new Vector2(0, 1), Vector2.zero);
                    createGenericListOfLowerUIImageObjects(page3ImageList, summaryPage, UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), Vector2.zero);
                    break;
                case 4:
                    for (int j = 0; j < 4; ++j) {
                        moveTypeImage = createLowerUIImageObject("moveTypeImages_" + j, summaryPage, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced);
                        moveTypeImage.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
                        moveTypeImage.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
                        movePosImage = createLowerUIImageObject("movePosImages_" + j, summaryPage, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced);
                        movePosImage.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
                        movePosImage.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
                        moveNameText = createLowerUITextObject("moveNameTexts_" + j, summaryPage, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
                        moveNameText.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
                        moveNameText.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
                        ppLabel = createLowerUITextObject("ppLabels_" + j, summaryPage, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
                        ppText = createLowerUITextObject("ppTexts_" + j, summaryPage, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
                    }
                    break;
                case 5:
                    for (int j = 0; j < 12; ++j) {
                        ribbonImage = createLowerUIImageObject("ribbonImages_" + j, summaryPage, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced);
                    }
                    ribbonNoLabel = createLowerUITextObject("ribbonNoLabel", summaryPage, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
                    ribbonNoText = createLowerUITextObject("ribbonNoText", summaryPage, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
                    break;
            }
        }
        //Create each main summary page
        GameObject allPages = createTopUIObject("All Pages", summaryScreen);
        allPages.GetComponent<RectTransform>().position = Vector3.zero;
        allPages.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        allPages.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        allPages.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        summaryTitleText = createLowerUITextObject("pageTitleText", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        markingsText = createLowerUITextObject("markingsText", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        itemLabel = createLowerUITextObject("itemLabel", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        itemText = createLowerUITextObject("itemText", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        ballImage = createLowerUIImageObject("ballImage", allPages, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Simple);
        nameText = createLowerUITextObject("nameText", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        genderText = createLowerUITextObject("genderText", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        levelText = createLowerUITextObject("levelText", allPages, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.white);
        speciesImage = createLowerUIImageObject("speciesImage", allPages, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced);
        shinyImage = createLowerUIImageObject("shinyIcon", allPages, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Simple);
    }
    #endregion Summary Screen


    #region Item Bag Screen
    public static void showItemBagScreen(int index = 0) {
        if (uiManager == null) {
            setUpUIManager();
        }
        if (itemBagScreen == null) {
            UIManager.generateItemBagScreen();
        }
        itemBagScreen.AddComponent<ItemScreenController>();
    }

    public static void generateItemBagScreen() {
        //Create Item Bag Screen object
        itemBagScreen = createTopUIObject("Item Bag Screen", uiManager);
        itemBagScreen.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //Create Item Bag Menu object
        GameObject itemBagMenu = createTopUIObject("itemBagMenu", itemBagScreen);
        itemBagMenu.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        itemBagMenu.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        itemBagMenu.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        GameObject bagBgTextRect = createLowerUIImageObject("bagBgTextRect", itemBagMenu, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced);
        GameObject itemList = createLowerUITextObject("itemList", itemBagMenu, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
        GameObject quantityList = createLowerUITextObject("quantityList", itemBagMenu, Vector3.zero, Vector2.zero, "Power Clear", 42, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
        System.Collections.Generic.List<string> itemImageList = new System.Collections.Generic.List<string>(new string[] { "bagBGRect", "bagBG", "bagIcon", "bagIconHighlight", "pocketIcon",
            "arrowLeft", "arrowRight", "itemSelector", "itemIcon", "sliderArrow", "sliderButton" });
        createGenericListOfLowerUIImageObjects(itemImageList, itemBagMenu, UnityEngine.UI.Image.Type.Simple, new Vector2(0, 1), Vector2.zero);
        System.Collections.Generic.List<string> itemTextList = new System.Collections.Generic.List<string>(new string[] { "pocketLabel", "itemDescription"});
        createGenericListOfLowerUITextObjects(itemTextList, itemBagMenu, "Power Clear", 42, 2.0f, Color.black, new Vector2(0, 1), Vector2.zero);
    }
    #endregion

    #region Trainer Card Screen
    public static void showTrainerCardScreen()
    {
        if (uiManager == null)
        {
            setUpUIManager();
        }
        if (trainerCardScreen == null)
        {
            UIManager.generateTrainerCardScreen();
        }
        trainerCardScreen.AddComponent<TrainerCardScreenController>();
    }

    public static void generateTrainerCardScreen()
    {
        //Create Trainer Card Screen object
        trainerCardScreen = createTopUIObject("Trainer Card Screen", uiManager);
        trainerCardScreen.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

        //Create card background, card image, and trainer image objects
        GameObject cardBG = createLowerUIImageObject("cardBG", trainerCardScreen, Vector3.zero, Vector2.zero, false, "Graphics/UI/Trainer Card Screen/cardbg" + ((GamePlayer.getTrainer().getGender() == Gender.FEMALE) ? "_f" : ""), UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
        GameObject cardImage = createLowerUIImageObject("cardImage", cardBG, Vector3.zero, Vector2.zero, false, "Graphics/UI/Trainer Card Screen/card" + ((GamePlayer.getTrainer().getGender() == Gender.FEMALE) ? "_f" : ""), UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
        GameObject trainerImage = createLowerUIImageObject("trainerImage", cardImage, Vector3.zero, Vector2.zero, false, "Graphics/Characters/trchar00" + (int)GamePlayer.getTrainer().getGender() + "_trainer", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true); //May need to change this line later on for different png names

        //Separate text objects into labelTexts and valueTexts since they require different text alignments
        System.Collections.Generic.List<string> cardLabelList = new System.Collections.Generic.List<string>(new string[] { "trainerNameLabel", "trainerMoneyLabel", "trainerPokedexLabel", "playTimeLabel", "trainerStartLabel", "trainerIDLabel" });
        System.Collections.Generic.List<string> cardTextList = new System.Collections.Generic.List<string>(new string[] { "trainerNameText", "trainerMoneyText", "trainerPokedexText", "playTimeText", "trainerIDText" });
        createGenericListOfLowerUITextObjects(cardLabelList, cardImage, "Power Clear", 42, 2.0f, Color.black, new Vector2(0, 1), new Vector2(0, 0), TextAnchor.UpperLeft);
        createGenericListOfLowerUITextObjects(cardTextList, cardImage, "Power Clear", 42, 2.0f, Color.black, new Vector2(0, 1), new Vector2(0, 0), TextAnchor.UpperRight);

    }
    #endregion

    #region Options Screen
    public static void showOptionsScreen()
    {
        if (uiManager == null)
        {
            setUpUIManager();
        }
        if (optionsScreen == null)
        {
            UIManager.generateOptionsScreen();
        }
        optionsScreen.AddComponent<OptionsScreenController>();
    }

    public static void generateOptionsScreen()
    {
        //Create Options Screen object
        optionsScreen = createTopUIObject("Options Screen", uiManager);
        optionsScreen.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

        GameObject optionsBG = createLowerUIImageObject("optionsBG", optionsScreen, Vector3.zero, Vector2.zero, false, "Graphics/UI/Options Screen/optionsbg", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
        GameObject optionsTitleBox = createLowerUIImageObject("optionsTitleBox", optionsBG, Vector3.zero, Vector2.zero, false, "Graphics/UI/WindowSkins/choice 1", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);
        GameObject speechFrame = createLowerUIImageObject("speechFrame", optionsBG, Vector3.zero, Vector2.zero, false, "Graphics/UI/WindowSkins/speech hgss 1", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), new Vector2(0, 1), true);

        GameObject optionsTitleText = createLowerUITextObject("optionsTitleText", optionsTitleBox, Vector3.zero, Vector2.zero, PlayerOptions.getFontStyle(), 42, 2.0f, new Vector2(0, 1), new Vector2(0, 0), Color.black, TextAnchor.UpperLeft);
        
        //Create Options Menu object for easier positioning of images.
        GameObject optionsMenu = createTopUIObject("optionsMenu", optionsScreen);
        optionsMenu.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        optionsMenu.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        optionsMenu.GetComponent<RectTransform>().pivot = new Vector2(0, 0);

        //Create Options Skin object
        GameObject optionsSkin = createLowerUIImageObject("Options Skin", optionsMenu, Vector3.zero, Vector2.zero, false, "Graphics/WindowSkins/choice 1", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), Vector2.zero);
        //Create Message BG object
        GameObject optionsText = createLowerUITextObject("Options Text", optionsMenu, Vector3.zero, Vector2.zero, "Power Clear", 53, 2.0f, new Vector2(0, 1), Vector2.zero, Color.black);
        //Create Options Arrow object
        GameObject optionsArrow = createLowerUIImageObject("Options Selector", optionsMenu, Vector3.zero, Vector2.zero, false, "", UnityEngine.UI.Image.Type.Sliced, new Vector2(0, 1), Vector2.zero);
    }
    #endregion

    #region Public Checks
    public static bool isPartyScreenShowing() {
        return partyScreen.GetComponent<PartyScreenController>() != null;
    }

    public static bool isSummaryScreenShowing() {
        return partyScreen.GetComponent<SummaryScreenController>() != null;
    }

    public static bool isItemScreenShowing() {
        return itemBagScreen.GetComponent<ItemScreenController>() != null;
    }

    public static bool isTrainerCardScreenShowing()
    {
        return trainerCardScreen.GetComponent<TrainerCardScreenController>() != null;
    }

    public static bool isOptionsScreenShowing()
    {
        return optionsScreen.GetComponent<OptionsScreenController>() != null;
    }

    public static bool anyMenusShowing() {
        if (partyScreen != null && partyScreen.GetComponent<PartyScreenController>() != null) {
            return true;
        } else if (pauseMenu != null && pauseMenu.GetComponent<PauseMenuController>() != null) {
            return true;
        } else if (messageSystem != null && messageSystem.GetComponent<DisplayText>() != null) {
            return true;
        } else if (textOptionMenu != null && textOptionMenu.GetComponent<TextOptionMenuController>() != null) {
            return true;
        }//add any other accessible UIs here

        return false;
    }
    #endregion Public Checks

    public static void setUpUIManager() {
        uiManager = GameObject.FindGameObjectWithTag("UIManager");
        uiManager.GetComponent<Transform>().position = new Vector3(0, 0, 0);
    }

    #region Generic Object Creation
    public static GameObject createTopUIObject(string name, GameObject parent) {
        GameObject go = new GameObject(name);
        go.transform.parent = parent.transform;
        go.layer = SortingLayer.GetLayerValueFromName("UI");
        go.AddComponent<Canvas>();
        go.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        go.GetComponent<Canvas>().pixelPerfect = true;
        go.AddComponent<UnityEngine.UI.CanvasScaler>();
        go.GetComponent<UnityEngine.UI.CanvasScaler>().uiScaleMode = UnityEngine.UI.CanvasScaler.ScaleMode.ScaleWithScreenSize;
        go.GetComponent<UnityEngine.UI.CanvasScaler>().referenceResolution = new Vector2(1024, 768);
        go.GetComponent<UnityEngine.UI.CanvasScaler>().screenMatchMode = UnityEngine.UI.CanvasScaler.ScreenMatchMode.Expand;
        go.GetComponent<UnityEngine.UI.CanvasScaler>().referencePixelsPerUnit = 16;
        go.AddComponent<UnityEngine.UI.GraphicRaycaster>();
        go.AddComponent<CanvasRenderer>();
        return go;
    }

    public static GameObject createLowerUIImageObject(string name, GameObject parent, Vector3 position, Vector2 size, bool enableImage, string imageLocation, UnityEngine.UI.Image.Type imageSliced, Vector2 anchor, Vector2 pivot, bool setNativeSize = false) {
        GameObject go = new GameObject(name);
        go.transform.parent = parent.transform;
        go.layer = SortingLayer.GetLayerValueFromName("UI");
        go.AddComponent<RectTransform>();
        if (size != Vector2.zero) {
            go.GetComponent<RectTransform>().sizeDelta = size;
        }
        go.GetComponent<RectTransform>().anchorMin = anchor;
        go.GetComponent<RectTransform>().anchorMax = anchor;
        go.GetComponent<RectTransform>().pivot = pivot;
        if (position != Vector3.zero) {
            go.GetComponent<RectTransform>().position = position;
        }
        go.AddComponent<CanvasRenderer>();
        go.AddComponent<UnityEngine.UI.Image>();
        go.GetComponent<UnityEngine.UI.Image>().enabled = false;
        if (!imageLocation.Equals("")) {
            go.GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load(imageLocation, typeof(Sprite)) as Sprite;
        }
        go.GetComponent<UnityEngine.UI.Image>().type = imageSliced;
        go.GetComponent<UnityEngine.UI.Image>().fillCenter = true;
        go.AddComponent<Canvas>();
        go.GetComponent<Canvas>().pixelPerfect = true;
        if (setNativeSize) {
            go.GetComponent<UnityEngine.UI.Image>().SetNativeSize();
        }
        return go;
    }

    public static GameObject createLowerUIImageObject(string name, GameObject parent, Vector3 position, Vector2 size, bool enableImage, string imageLocation, UnityEngine.UI.Image.Type imageSliced, bool setNativeSize = false) {
        GameObject go = createLowerUIImageObject(name, parent, position, size, enableImage, imageLocation, imageSliced, Vector2.zero, Vector2.zero, setNativeSize);
        return go;
    }

    public static void createGenericListOfLowerUIImageObjects(System.Collections.Generic.List<string> list, GameObject parent, UnityEngine.UI.Image.Type imageSliced, Vector2 anchor, Vector2 pivot) {
        GameObject go;
        foreach (string s in list) {
            go = createLowerUIImageObject(s, parent, Vector2.zero, Vector2.zero, false, "", imageSliced, anchor, pivot, false);
        }
    }


    public static GameObject createLowerUITextObject(string name, GameObject parent, Vector3 position, Vector2 size, string fontName, int fontSize, float lineSpacing, Vector2 anchor, Vector2 pivot, Color color, TextAnchor alignment = TextAnchor.UpperLeft) {
        GameObject go = new GameObject(name);
        go.transform.parent = parent.transform;
        go.layer = SortingLayer.GetLayerValueFromName("UI");
        go.AddComponent<RectTransform>();
        go.GetComponent<RectTransform>().position = position;
        go.GetComponent<RectTransform>().sizeDelta = size;
        go.GetComponent<RectTransform>().anchorMin = anchor;
        go.GetComponent<RectTransform>().anchorMax = anchor;
        go.GetComponent<RectTransform>().pivot = pivot;
        go.AddComponent<CanvasRenderer>();
        go.AddComponent<UnityEngine.UI.Text>();
        if (!fontName.Equals("")) {
            go.GetComponent<UnityEngine.UI.Text>().font = FontManager.getFont(fontName);
            go.GetComponent<UnityEngine.UI.Text>().fontSize = fontSize;
            go.GetComponent<UnityEngine.UI.Text>().lineSpacing = lineSpacing;
        }
        go.GetComponent<UnityEngine.UI.Text>().supportRichText = true;
        go.GetComponent<UnityEngine.UI.Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
        go.GetComponent<UnityEngine.UI.Text>().verticalOverflow = VerticalWrapMode.Overflow;
        go.GetComponent<UnityEngine.UI.Text>().color = color;
        go.GetComponent<UnityEngine.UI.Text>().alignment = alignment;
        return go;
    }

    public static GameObject createLowerUITextObject(string name, GameObject parent, Vector3 position, Vector2 size, string fontName, int fontSize, float lineSpacing, Color color, TextAnchor alignment = TextAnchor.UpperLeft) {
        GameObject go = createLowerUITextObject(name, parent, position, size, fontName, fontSize, lineSpacing, Vector2.zero, Vector2.zero, color, alignment);
        return go;
    }

    public static GameObject createLowerUITextObject(string name, GameObject parent, Vector3 position, Vector2 size, Vector2 anchor, Vector2 pivot, Color color, TextAnchor alignment = TextAnchor.UpperLeft) {
        GameObject go = createLowerUITextObject(name, parent, position, size, "", 0, 0, anchor, pivot, color, alignment);
        return go;
    }

    public static void createGenericListOfLowerUITextObjects(System.Collections.Generic.List<string> list, GameObject parent, string fontName, int fontSize, float lineSpacing, Color color, Vector2 anchor, Vector2 pivot, TextAnchor alignment = TextAnchor.UpperLeft) {
        GameObject go;
        foreach (string s in list) {
            go = createLowerUITextObject(s, parent, Vector2.zero, Vector2.zero, fontName, fontSize, lineSpacing, anchor, pivot, color, alignment);
        }
    }
    #endregion  Generic Object Creation

}
