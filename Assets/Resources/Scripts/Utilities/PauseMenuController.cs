﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class PauseMenuController : ChoiceController {

    new void Start() {
        //Set desired objects/sprites to use (already created, and children of the game object this is attatched to)
        arrowLocation = "Graphics/Pictures/arrows";
        imageBoxName = "Pause Skin";
        imageArrowName = "Pause Selector";
        textObjectName = "Pause Text";
        //Set desired positioning
        xPos = 766;
        yPos = -355*(menuOptions.Count+1);
        setWidth = 250;
        arrowHeight = arrowWidth = 20;
        menuSpacing = 60;
        arrowSpacing = 52;
        arrowStartYPos = -58;
        updateMenuText();
        base.Start();     
    }

    void updateMenuText() {
        menuOptions.Clear();
        //Add each menu item
        menuOptions.Add("DEX", "Pokédex");
        if (GamePlayer.getTrainer().numInParty() > 0) {  //if player has pokemon
            menuOptions.Add("PARTY", "Pokémon");
        }
        menuOptions.Add("BAG", "Bag");
        menuOptions.Add("PLAYER", PokemonTrainer.TrainerName);
        menuOptions.Add("OPTIONS", "Options");
        menuOptions.Add("CANCEL", "Cancel");
    }

    protected override IEnumerator executeSelection(bool exit = false) {
        if (exit) {
            Destroy(this);
            yield break;
        }
        hasControl = false;
        switch (menuOptions.Keys.ElementAt(arrowPos)) {
            case "DEX":

                break;
            case "PARTY":
                UIManager.showPartyScreen();
                while (UIManager.isPartyScreenShowing()) {
                    yield return null;
                }
                break;
            case "BAG":
                UIManager.showItemBagScreen();
                while (UIManager.isItemScreenShowing()) {
                    yield return null;
                }
                break;
            case "PLAYER":
                UIManager.showTrainerCardScreen();
                while (UIManager.isTrainerCardScreenShowing())
                {
                    yield return null;
                }
                break;
            case "OPTIONS":
                UIManager.showOptionsScreen();
                while (UIManager.isOptionsScreenShowing())
                {
                    yield return null;
                }
                break;
            case "CANCEL":
                Destroy(this);
                break;
        }
        hasControl = true;
        yield break;
    }
    
}
