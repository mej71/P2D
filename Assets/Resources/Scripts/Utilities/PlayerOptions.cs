﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class PlayerOptions{
	//Steps for adding new options (Feel free to add your own option type class just make sure it extends PlayerOptions)
	//1. In getOptionsList(), instantiate each option item as a type of SliderOption, EnumOption, or NumOption with the necessary parameters. If using enums, make the enum in this file, and type the name of the enum as a string for the appropriate parameter.
	//2. In executeSelection() of OptionsMenuController.cs, add the case statement for your new option i.e case "OptionName":
	//			p_Options.<insert variable to change here> = <some value>;
	//			break;
	//(Remember to have a default value for your option like the examples below)
	//3. Your option should now function and be a part of the scrollable list.
	public static float BgmVolume = 100.0F; 
	public static float SfxVolume = 100.0F;
	public static int textSpeed = 0;
	public static BattleEffects battleEffects = BattleEffects.On;
	public static BattleStyle battleStyle = BattleStyle.Shift;
	public static RunningKey runningKey = RunningKey.Hold;
	public static FontStyle curFont = FontStyle.Em;
	public static string curSpeechFrame = "speech hgss 1";
	public static string curMenuFrame = "choice 1";
	// Use this for initialization
	// public PlayerOptions(float bgm = 100.0F, float sfx = SfxVolume){//Default option values or from save data
	// 	BgmVolume = bgm;
	// 	SfxVolume = sfx;
	// }
	public enum TextSpeed {
		Slow,
		Normal,
		Fast
	}
	public enum BattleEffects {
		On,
		Off
	}
	public enum BattleStyle {
		Shift,
		Set
	}
	public enum RunningKey {
		Hold,
		Toggle
	}
	public enum FontStyle {
		Em,
		RS,
		FRLG,
		DPPT
	}
	public virtual void next(){ }
	public virtual void prev(){ }
	public static List<PlayerOptions> getOptionsList(){
		List<PlayerOptions> tempList = new List<PlayerOptions>();
		tempList.Add(new SliderOption("BGM Volume", 0, 100, 5, PlayerOptions.BgmVolume));
		tempList.Add(new SliderOption("SE Volume", 0, 100, 5, PlayerOptions.SfxVolume));
		tempList.Add(new EnumOption("Text Speed", "TextSpeed", PlayerOptions.textSpeed));
		tempList.Add(new EnumOption("Battle Effects", "BattleEffects", (int)PlayerOptions.battleEffects));
		tempList.Add(new EnumOption("Battle Style", "BattleStyle", (int)PlayerOptions.battleStyle));
		tempList.Add(new EnumOption("Running Key", "RunningKey", (int)PlayerOptions.runningKey));
		tempList.Add(new NumberOption("Speech Frame", PlayerOptions.getSpeechOptions(), PlayerOptions.getSpeechOptions().FindIndex(a => a.Contains(PlayerOptions.curSpeechFrame))));
		tempList.Add(new NumberOption("Menu Frame", PlayerOptions.getChoiceOptions(), PlayerOptions.getChoiceOptions().FindIndex(a => a.Contains(PlayerOptions.curMenuFrame))));
		tempList.Add(new EnumOption("Font Style", "FontStyle", (int) PlayerOptions.curFont));
		return tempList;
	}
	public static List<string> getChoiceOptions(){
		List<string> frameList = new List<string>(){"choice 1", "choice 2", "choice 3", "choice 4", "choice 5", "choice 6", "choice 7", "choice 8", "choice 9", "choice 10", "choice 11", "choice 12", "choice 13", "choice 14", "choice 15", "choice 16", "choice 17", "choice 18", "choice 19", "choice 20", "choice 21", "choice 22", "choice 23", "choice 24", "choice 25", "choice 26", "choice 27", "choice 28", "choice dp", "choice ug"};
		return frameList;
	}
	public static List<string> getSpeechOptions(){
		List<string> frameList = new List<string>(){"speech hgss 1", "speech hgss 2", "speech hgss 3", "speech hgss 4", "speech hgss 5", "speech hgss 6", "speech hgss 7", "speech hgss 8", "speech hgss 9", "speech hgss 10", "speech hgss 11", "speech hgss 12", "speech hgss 13", "speech hgss 14", "speech hgss 15", "speech hgss 16", "speech hgss 17", "speech hgss 18", "speech hgss 19", "speech hgss 20"};
		return frameList;
	}
	public static string getFontStyle(){
		string font = "";
		switch(curFont){
			case FontStyle.Em:
			font = "Power Green";
				break;
			case FontStyle.RS:
			font = "Power Red and Blue";
				break;
			case FontStyle.FRLG:
			font = "Power Red and Green";
				break;
			case FontStyle.DPPT:
			font = "Power Clear";
				break;			
		} 
		return font;
	}
	

}

public class SliderOption : PlayerOptions {
	public string Name { get; set; }
	public float Min { get; set; }
	public float Max { get; set; }
	public float Range { get; set; }
	public float Modifier { get; set; }
	public float CurrentValue { get; set; }
    public SliderOption(string p_name, float optMin, float optMax, float optChange, float defaultValue){
		Name = p_name;
		Min = optMin;
		Max = optMax;
		Range = Max - Min;
		Modifier = optChange;
		CurrentValue = defaultValue;
    }
	public override void next(){
		CurrentValue = CurrentValue + Modifier <= Max ? CurrentValue + Modifier : Max;
	}
	public override void prev(){
		CurrentValue = CurrentValue - Modifier >= Min ? CurrentValue - Modifier : Min;
	}
}
public class EnumOption : PlayerOptions {
	public string Name { get; set; }
	public string EnumName { get; set; }
	public int CurrentValue { get; set; }
	public static List<int> enumValues;
    public EnumOption(string p_name, string enumName, int optStart = 0){
        Name = p_name;
		EnumName = enumName;
		CurrentValue = optStart;
    }
	public override void next(){
		CurrentValue = CurrentValue + 1 <= Enum.GetNames(typeof(PlayerOptions).GetNestedType(EnumName)).Length - 1 ? CurrentValue + 1 : 0;
	}
	public override void prev(){
		CurrentValue = CurrentValue - 1 >= 0 ? CurrentValue - 1 : Enum.GetNames(typeof(PlayerOptions).GetNestedType(EnumName)).Length - 1;
	}
}
public class NumberOption : PlayerOptions {
	public string Name { get; set; }
	public List<string> Values { get; set; }
	public int Index { get; set; }
    public NumberOption(string p_name, List<string> list, int optStart = 0){
        Name = p_name;
		Values = list;
		Index = optStart;
    }
	public override void next(){
		Index = Index + 1 <= Values.Count -1 ? Index + 1 : 0;
	}
	public override void prev(){
		Index = Index - 1 >= 0 ? Index - 1 : Values.Count - 1;
	}

}