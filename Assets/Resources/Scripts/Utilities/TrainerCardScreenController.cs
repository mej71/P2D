﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainerCardScreenController : UIScreen {

    const int defaultTextSize = 25;
    static Color32 defaultTextColor = new Color32(69, 69, 69, 255);

    UIImageDef cardBG = new UIImageDef();
    UIImageDef cardImage = new UIImageDef();
    UIImageDef trainerImage = new UIImageDef();

    //Define trainer card label text
    UITextDef trainerNameLabel = new UITextDef(new Vector3(33, -70), defaultTextSize, defaultTextColor, "Name");
    UITextDef trainerMoneyLabel = new UITextDef(new Vector3(33, -117), defaultTextSize, defaultTextColor, "Money");
    UITextDef trainerPokedexLabel = new UITextDef(new Vector3(33, -164), defaultTextSize, defaultTextColor, "Pokédex");
    UITextDef playTimeLabel = new UITextDef(new Vector3(33, -214), defaultTextSize, defaultTextColor, "Time");
    UITextDef trainerIDLabel = new UITextDef(new Vector3(333, -70), defaultTextSize, defaultTextColor, "ID No.");
    UITextDef trainerStartLabel = new UITextDef(new Vector3(33, -263), defaultTextSize, defaultTextColor, "Started");

    //Define trainer card value text
    UITextDef trainerNameText = new UITextDef(new Vector3(303, -70), defaultTextSize, defaultTextColor, "");
    UITextDef trainerMoneyText = new UITextDef(new Vector3(303, -117), defaultTextSize, defaultTextColor, "");
    UITextDef trainerPokedexText = new UITextDef(new Vector3(303, -164), defaultTextSize, defaultTextColor, "");
    UITextDef playTimeText = new UITextDef(new Vector3(303, -214), defaultTextSize, defaultTextColor, "");
    UITextDef trainerIDText = new UITextDef(new Vector3(468, -70), defaultTextSize, defaultTextColor, "");
  
    // Use this for initialization
    void Start()
    {
        InitializeAllTextAndImageFields(this);

        //Adjust image position and scale accordingly 
        cardBG.i.GetComponent<RectTransform>().localScale = new Vector3(128, 96, 10);
        cardImage.i.GetComponent<RectTransform>().localPosition = Vector3.zero;
        cardImage.i.GetComponent<RectTransform>().localScale = new Vector3(2 / cardBG.i.GetComponent<RectTransform>().localScale.x, 2 / cardBG.i.GetComponent<RectTransform>().localScale.y, 2);
        trainerImage.i.GetComponent<RectTransform>().localPosition = new Vector3(340, -110, 0);
        trainerImage.i.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 2);

        //Convert trainer attributes to be readable text on the trainer card.
        trainerNameText.t.text = GamePlayer.getTrainer().getName().ToString();
        trainerMoneyText.t.text = "$" + GamePlayer.getTrainer().getMoney().ToString();
        trainerPokedexText.t.text = GamePlayer.getTrainer().numInParty().ToString();
        playTimeText.t.text = TimeFunctions.getHour().ToString() + ":" + TimeFunctions.getMinute().ToString() + ":" + TimeFunctions.getSecond().ToString();
        trainerIDText.t.text = GamePlayer.getTrainer().getTrainerID().ToString();
        




    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.X))
        {
            Destroy(this);
        }
    }

    void OnDestroy()
    {
        ChangeAllImageAndTextEnableValue(this, false);
    }
}
