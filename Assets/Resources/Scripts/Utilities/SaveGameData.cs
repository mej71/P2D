﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;
using System.Reflection;
public sealed class VersionDeserializationBinder : SerializationBinder {
    public override Type BindToType(string assemblyName, string typeName) {
        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName)) {
            Type typeToDeserialize = null;
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            // The following line of code returns the type. 
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));
            return typeToDeserialize;
        }
        return null;
    }
}
public class SaveGameData {
    public static Dictionary<string, object> saveDataInfo;
    public static void setCurStateData() {
        saveDataInfo = new Dictionary<string, object>();
        saveDataInfo["Game Trainer"] = GamePlayer.getTrainer();
        saveDataInfo["Player X"] = GamePlayer.getPlayerX();
        saveDataInfo["Player Y"] = GamePlayer.getPlayerY();
    }
    public static void updateCurStateData() {
        GamePlayer.updateTrainer((PokemonTrainer)saveDataInfo["Game Trainer"]);
        Debug.Log(GamePlayer.getPlayerX());
        Debug.Log(saveDataInfo["Player X"]);
        GamePlayer.getPlayer().transform.position = new Vector3((float)saveDataInfo["Player X"], (float)saveDataInfo["Player Y"], 0);
    }
    public static void saveData() {
        setCurStateData();
        Stream stream = File.Open("MySavedGame.game", FileMode.Create);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        Debug.Log("Writing Information");
        bformatter.Serialize(stream, saveDataInfo);
        stream.Close();
    }
    public static void loadData() {
        if (!File.Exists("MySavedGame.game")) {
            return;
        }
        Stream stream = File.Open("MySavedGame.game", FileMode.Open);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        Debug.Log("Reading Data");
        saveDataInfo = (Dictionary<string, object>)bformatter.Deserialize(stream);
        stream.Close();
        updateCurStateData();
        GamePlayer.hasTransferred = true;
    }
}
