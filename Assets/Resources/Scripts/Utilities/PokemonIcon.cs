﻿using UnityEngine;
using System.Collections;

public class PokemonIconManager : MonoBehaviour {

    static System.Collections.Generic.List<string> pokemonIcons = new System.Collections.Generic.List<string>();

    public static Sprite[] getPokemonSprite(Pokemon poke) {
        if (pokemonIcons.Count == 0) {
            fetchIconSprites();
        }
        string iconName = poke.getSpeciesID().ToString() + ((poke.gender == Gender.FEMALE) ? "f" : "");
        if (pokemonIcons.Contains(iconName)) {
            return Resources.LoadAll<Sprite>("Graphics/Icons/Pokemon/" + iconName);
        } else {
            iconName = poke.getSpeciesID().ToString();
            return Resources.LoadAll<Sprite>("Graphics/Icons/Pokemon/" + iconName);
        }
    }

    public static Sprite[] getPokemonSprite(PBPokemon poke) {
        if (pokemonIcons.Count == 0) {
            fetchIconSprites();
        }
        return Resources.LoadAll<Sprite>("Graphics/Icons/Pokemon/" + poke.ToString().ToUpper());
    }

    public static void fetchIconSprites() {
        Object[] tempIcons = (Resources.LoadAll("Graphics/Icons/Pokemon", typeof(Sprite)));
        foreach (Object obj in tempIcons) {
            pokemonIcons.Add(obj.name);
        }
    }
}



public class PokemonIconSprite : MonoBehaviour {

    private Sprite[] iconSprites;
    private PBPokemon pokemon = PBPokemon.NONE;
    private int timeCount = 0;
    private int rectIndex = 0;
    private int animSpeed, baseSpeed;
    private const int iconHeight = 64;
    private const int iconWidth = 64;
    private const int defaultSpeed = 10;
    private bool selected = false;

    public void initPokemonIconSprite(Pokemon poke) {
        pokemon = poke.getSpeciesID();
        iconSprites = PokemonIconManager.getPokemonSprite(poke);
        if (poke.hp == 0) { //adjust animation speed based on hp
            changeAnimSpeed(0);
        } else if (poke.hp <= poke.TotalHP()/2) {
            baseSpeed = defaultSpeed * 2;
        } else if (poke.hp <= poke.TotalHP()/4) {
            baseSpeed = defaultSpeed * 3;
        } else {
            baseSpeed = defaultSpeed;
        }
        changeAnimSpeed((selected) ? baseSpeed / 2 : baseSpeed);
        changeSprite();
        InvokeRepeating("UpdateSprite", 0, 0.025f);
    }

    public void initPokemonIconSprite(PBPokemon poke) {
        pokemon = poke;
        iconSprites = PokemonIconManager.getPokemonSprite(poke);
        baseSpeed = defaultSpeed;
        changeAnimSpeed((selected) ? baseSpeed / 2 : baseSpeed);
        changeSprite();
        InvokeRepeating("UpdateSprite", 0, 0.025f);
    }

    private void changeAnimSpeed(int speed) {animSpeed = speed; }

    public void select() {
        selected = true;
        changeAnimSpeed(baseSpeed / 2);
    }

    public void unselect() {
        selected = false;
        changeAnimSpeed(baseSpeed);
    }

    public void signalExit() {
        this.CancelInvoke("UpdateSprite");
    }

    void OnDestroy() {signalExit();}

    void OnDisable() {signalExit();}

    private void UpdateSprite() {
        if (iconSprites == null || animSpeed <= 0) {
            return;
        }
        if (timeCount % animSpeed == 0) {
            changeSprite();
        }
        ++timeCount;
        if (timeCount == 60) {
            timeCount = 0;
        }
    }

    private void changeSprite() {
        if (iconSprites == null) {
            return;
        }
        if (animSpeed <= 0) {
            rectIndex = 0;
        } else {
            rectIndex = (rectIndex == 0) ? 1 : 0;
        }
        gameObject.GetComponent<UnityEngine.UI.Image>().sprite = iconSprites[rectIndex];
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(iconWidth, iconHeight);
    }

}
