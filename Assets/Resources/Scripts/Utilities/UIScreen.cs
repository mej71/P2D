﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;


//Structs for defining text and image components
public class UIImageDef {
    public Image i;
    public string imagePath;
    public bool enable;
    public UIImageDef (bool e = true, string p_path = "") {
        imagePath = p_path;
        enable = e;
    }
}

public class UITextDef {
    public Text t;
    public Vector3 pos;
    public int fontSize;
    public string fontName;
    public string text;
    public bool enable;
    public Color color;
    public UITextDef(Vector3 p, int fs, Color? c = null, string t = "", bool e = true, string fn = "") { 
        pos = p;
        fontSize = fs;
        color = c.GetValueOrDefault(Color.black);  //Default to black?
        fontName = fn;
        text = t;
        enable = e;
    }
}

public class UITextListDef {
    public List<Text> t;
    public List<Vector3> pos;
    public int fontSize;
    public Color color;
    public string fontName;
    public string text;
    public bool enable;
    public UITextListDef(List<Vector3> p, int fs, Color? c = null, string te = "", bool e = true, string fn = "") { //predefined vectors
        t = new List<Text>();
        ListExtensions<Text>.initNullList(ref t, p.Count);
        pos = p;
        fontSize = fs;
        color = c.GetValueOrDefault(Color.black);  //Default to black?
        fontName = fn;
        text = te;
        enable = e;
    }
    public UITextListDef(Vector3 p, int ls, int fs, Color? c = null, string te = "", bool e = true, string fn = "") { //single predefined vector
        t = new List<Text>();
        ListExtensions<Text>.initNullList(ref t, ls);
        pos = Enumerable.Repeat(p, ls).ToList();
        fontSize = fs;
        color = c.GetValueOrDefault(Color.black);  //Default to black?
        fontName = fn;
        text = te;
        enable = e;
    }
}

public class UIImageListDef {
    public List<Image> i;
    public string imagePath;
    public bool enable;
    public UIImageListDef(int ls, bool e = true, string p_path = "") {
        imagePath = p_path;
        i = new List<Image>();
        ListExtensions<Image>.initNullList(ref i, ls); ;
        enable = e;
    }
}


//Generic underlying class for many of the screens, holds shared init methods
public class UIScreen : MonoBehaviour {

    protected bool hasControl = true;

    protected void initTextComponent(ref List<Text> textList, int pos, Text textObj, Vector3 position, int fontSize, bool enable = true, Color? c = null) {
        textList[pos] = textObj;
        textList[pos].font = FontManager.getFont(PlayerOptions.getFontStyle());
        textList[pos].horizontalOverflow = HorizontalWrapMode.Overflow;
        //Reduce text scale to prevent blurriness
        textList[pos].fontSize = fontSize * 2;
        textList[pos].color = c.GetValueOrDefault(Color.black);
        textList[pos].transform.localScale = new Vector3(0.5f, 0.5f, 1);
        textList[pos].gameObject.GetComponent<RectTransform>().localPosition = position;
        textList[pos].enabled = enable;
    }


    //Individual text and image objects need to be passed in by reference (lists of them do not)
    protected void initTextComponent(ref Text newTextObj, Text textObj, Vector3 position, string text, int fontSize, bool enable = true, Color? c = null) {
        newTextObj = textObj;
        newTextObj.font = FontManager.getFont(PlayerOptions.getFontStyle());
        newTextObj.horizontalOverflow = HorizontalWrapMode.Overflow;
        //Reduce text scale to prevent blurriness
        newTextObj.fontSize = fontSize * 2;
        newTextObj.color = c.GetValueOrDefault(Color.black);
        newTextObj.transform.localScale = new Vector3(0.5f, 0.5f, 1);
        newTextObj.gameObject.GetComponent<RectTransform>().localPosition = position;
        newTextObj.text = text;
        newTextObj.enabled = enable;
    }


    protected void initImgComponent(ref List<Image> imgList, Image imgObj, int pos, bool enable = true) {
        imgList[pos] = imgObj;
        imgList[pos].enabled = enable;
    }


    protected void initImgComponent(ref Image newImg, Image imgObj, bool enable = true) {
        newImg = imgObj;
        newImg.enabled = enable;
    }


    //Use reflection to get all Image and Text components from any inherited class, and enable/disable them in one call (also works for lists of them)
    protected void ChangeAllImageAndTextEnableValue<T>(T t, bool replacement) where T : UIScreen {
        System.Reflection.FieldInfo[] fields = typeof(T).GetFields(System.Reflection.BindingFlags.NonPublic | 
                                                            System.Reflection.BindingFlags.Public | 
                                                            System.Reflection.BindingFlags.Instance);  //find all public and private non-static fields
        foreach (System.Reflection.FieldInfo f in fields) {
            if (f.FieldType == typeof(UIImageDef)) {
                UIImageDef i = (UIImageDef)f.GetValue(t);
                if (i.i != null) {
                    i.i.enabled = replacement;
                }
            } else if (f.FieldType == typeof(UITextDef)) {
                UITextDef te = (UITextDef)f.GetValue(t);
                if (te.t != null) {
                    te.t.enabled = replacement;
                } 
                
            } else if (f.FieldType == typeof(UIImageListDef)) {
                UIImageListDef list = (UIImageListDef)f.GetValue(t);
                foreach (Image im in list.i) {
                    if (im != null) {
                        im.enabled = replacement;
                    }
                }
            } else if (f.FieldType == typeof(UITextListDef)) {
                UITextListDef list = (UITextListDef)f.GetValue(t);
                foreach (Text te in list.t) {
                    if (te != null) {
                        te.enabled = replacement;
                    }
                }
            }
        }
    }

    protected void InitializeAllTextAndImageFields<T>(T t) where T :UIScreen {
        Image[] tempImages = t.GetComponentsInChildren<Image>(true);
        Text[] tempTexts = t.GetComponentsInChildren<Text>(true);
        System.Reflection.FieldInfo[] fields = typeof(T).GetFields(System.Reflection.BindingFlags.NonPublic |
                                                            System.Reflection.BindingFlags.Public |
                                                            System.Reflection.BindingFlags.Instance);  //find all public and private non-static fields
        Dictionary<UIImageDef, string> imageFields = new Dictionary<UIImageDef, string>();
        Dictionary<UITextDef, string> textFields = new Dictionary<UITextDef, string>();
        Dictionary<UIImageListDef, string> imageListFields = new Dictionary<UIImageListDef, string>();
        Dictionary<UITextListDef, string> textListFields = new Dictionary<UITextListDef, string>();
        foreach (System.Reflection.FieldInfo f in fields) {
            if (f.FieldType == typeof(UIImageDef)) {
                imageFields.Add((UIImageDef)f.GetValue(t), f.Name);
            } else if (f.FieldType == typeof(UITextDef)) {
                textFields.Add((UITextDef)f.GetValue(t), f.Name);
            } else if (f.FieldType == typeof(UITextListDef)) {
                textListFields.Add((UITextListDef)f.GetValue(t), f.Name);
            } else if (f.FieldType == typeof(UIImageListDef)) {
                imageListFields.Add((UIImageListDef)f.GetValue(t), f.Name);
            } //retry adding GameObjects later somehow.  Can't use SetValue becauase they start out as null.  Idk what else to do
        }
        KeyValuePair<UIImageDef, string> im;
        KeyValuePair<UITextDef, string> it;
        KeyValuePair<UIImageListDef, string> iml;
        KeyValuePair<UITextListDef, string> itl;
        List<string> splitName = new List<string>();
        foreach (Image i in tempImages) {
            im = new KeyValuePair<UIImageDef, string>();
            iml = new KeyValuePair<UIImageListDef, string>();
            im = imageFields.FirstOrDefault(x => x.Value==i.name);  //try to find exact name
            if (!im.Equals(default(KeyValuePair<UIImageDef, string>))) {
                initImgComponent(ref im.Key.i, i, im.Key.enable);
                if (!im.Key.imagePath.Equals("")) {
                    im.Key.i.sprite = Resources.Load(im.Key.imagePath, typeof(Sprite)) as Sprite;
                }
            } else {
                splitName.Clear();
                splitName = i.name.Split('_').ToList<string>();
                if (splitName.Count == 2) { //if it found a match, it will be split into (varName, index)
                    iml = imageListFields.FirstOrDefault(x => x.Value == splitName[0]);
                    if (!iml.Equals(default(KeyValuePair<UIImageListDef, string>))) {
                        initImgComponent(ref iml.Key.i, i, System.Int32.Parse(splitName[1]), iml.Key.enable);
                    } else {
                        Debug.Log("The image list " + i.name + " was not defined in your class, was named incorrectly, or you mispelled it.\nThis is allowed, but may be a bug");
                    }
                } else {
                    //Debug.Log("The image "+i.name +" was not defined in your class, was named incorrectly, or you mispelled it.\nThis is allowed, but may be a bug");
                }
            }
        }
        foreach (Text text in tempTexts) {
            it = new KeyValuePair<UITextDef, string>();
            itl = new KeyValuePair<UITextListDef, string>();
            it = textFields.FirstOrDefault(x => x.Value == text.name);  //try to find exact name
            if (!it.Equals(default(KeyValuePair<UITextDef, string>))) {
                it.Key.t = text;
                initTextComponent(ref it.Key.t, text, it.Key.pos, it.Key.text, it.Key.fontSize, it.Key.enable, it.Key.color);
            } else {
                splitName.Clear();
                splitName = text.name.Split('_').ToList<string>();
                if (splitName.Count == 2) { //if it found a match, it will be split into (varName, index)
                    itl = textListFields.FirstOrDefault(x => x.Value == splitName[0]);
                    if (!itl.Equals(default(KeyValuePair<UITextListDef, string>))) {
                        itl.Key.t[System.Int32.Parse(splitName[1])] = text;
                        initTextComponent(ref itl.Key.t, System.Int32.Parse(splitName[1]), text, itl.Key.pos[System.Int32.Parse(splitName[1])], itl.Key.fontSize, itl.Key.enable, itl.Key.color);
                    } else {
                        Debug.Log("The text list " + text.name + " was not defined in your class, was named incorrectly, or you mispelled it.\nThis is allowed, but may be a bug");
                    }
                } else {
                    //Debug.Log("The text " + text.name + " was not defined in your class, was named incorrectly, or you mispelled it.\nThis is allowed, but may be a bug");
                }
            }
        }
    }

}
