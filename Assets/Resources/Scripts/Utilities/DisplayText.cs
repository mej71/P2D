﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DisplayText : MonoBehaviour {

	public float letterPauseTime = 0.02f;  //Will use text speed option instead of this once options are established
	string words = "";
	int curChar = 0;
	string currentSentence = "";
	bool isTyping = false;
	bool finishedTyping = false;
	Image textBox;
	Text displayText;
	GameObject textObj;


	void Start() {
		Image[] tempImages = GetComponentsInChildren<Image>(true);
		foreach (Image image in tempImages) {
			if (image.name.Equals("Message Skin")) {
				textBox = image;
				textBox.enabled = true;
                break;
			}
		}
		foreach (Transform child in transform) {
			if (child.name.Equals("Message Text")) {
			 	textObj = child.gameObject;
				break;
			}
		}
		positionTextBox();
		InvokeRepeating ("updateText", 0, letterPauseTime);
	}

	//Positioning is different depending on screen size, reposition when drawn
	//Only 512x384 and 1024x786 hardcoded
	void positionTextBox() {
        textObj.GetComponent<Text>().font.material.mainTexture.filterMode = FilterMode.Point;
        if (Screen.width == 1024) {
            textBox.GetComponent<RectTransform>().anchoredPosition = new Vector3( -8, 0, 0);
            textObj.GetComponent<RectTransform>().anchoredPosition = new Vector3( 40, 55, 0);
        } else {
            textBox.GetComponent<RectTransform>().anchoredPosition = new Vector3(-25, 106, 0);
            textObj.GetComponent<RectTransform>().anchoredPosition = new Vector3(20, 155, 0);
        }
        textObj.GetComponent<Text>().rectTransform.sizeDelta = new Vector2(Screen.width * 0.839855f, 96);
		textBox.rectTransform.sizeDelta = new Vector2(Screen.width, 128);
    }

	void updateText() {
		if (!isTyping && !finishedTyping && !waitingForMoreLines) {
			TypeText();
		}
        if (waitingForMoreLines && Input.GetButton("Enter")) {
            waitingForMoreLines = false;
            currentSentence = "";
            TypeText();
        } else if (finishedTyping && Input.GetButton("Enter")) {
			textBox.enabled = false;
            displayText.text = "";
            displayText.enabled = false;
			CancelInvoke ("updateText");
			this.gameObject.SetActive(false);
			Destroy(this);
		}
	}

	public void SetText(string newText)
	{
        displayText = GetComponentInChildren<Text>();
		displayText.enabled = true;
		if (FontManager.hasFont("Power Green")) {
			displayText.font = FontManager.getFont("Power Green");
		}
		words = newText;
	}

	bool onNewWord = true;
	string curLine = "";
    int numLines = 1;
    bool waitingForMoreLines = false;
	void TypeText() {
		char[] tempWords = words.ToCharArray();
		string tempLine = curLine;
		int tempChar = curChar;
		bool newLine = false;
        //Determine if word can fit on current line in text box, add if so.  If not, add a line break
		if (onNewWord) {
			while (tempChar<tempWords.Length && tempWords [tempChar] != ' ') {
                if (displayText.rectTransform.sizeDelta.x > displayText.cachedTextGeneratorForLayout.GetPreferredWidth (tempLine, displayText.GetGenerationSettings (displayText.GetComponent<RectTransform> ().rect.size))) {             
					tempLine += tempWords [tempChar];
					tempChar++;
				} else {
					newLine = true;
					break;
				}
			}
			onNewWord = false;
		}
		if (newLine) {
            if (numLines > 1) {
                numLines = 1;
                curLine = "";
                waitingForMoreLines = true;
                return;
            } else {
                currentSentence += '\n';
                curLine = "";
                numLines += 1;
            }
        }
		currentSentence+= tempWords[curChar];
		curLine += tempWords [curChar];
		if (tempWords [curChar] == ' ') {
			onNewWord = true;
		}
		curChar++;
		if (curChar>=tempWords.Length) {
			isTyping = false;
			curChar = 0;
			finishedTyping = true;
		}
	}

	void OnGUI() {
		displayText.text = currentSentence;
	}
}
