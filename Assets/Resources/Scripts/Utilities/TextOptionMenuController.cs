﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TextOptionMenuController : ChoiceController {

    protected bool ScrollingEnabled;    // Is scrolling enabled for this list? (true if number of items in list > MaxLineHeight)
	protected int MaxLineHeight; 		// The max number of lines to display before scrolling.
	protected int MinOptionIndex;		// The first option currently being displayed.
	protected int MaxOptionIndex;		// The last option currently being displayed.
	protected List<string> Options;	    // List of Options in the menu.
	protected string SelectedOption;	// The option selected by the player.

	// Use this for initialization
	new void Start () {
		arrowLocation = "Graphics/Pictures/arrows";
		imageBoxName = "Text Option Skin";
        imageArrowName = "Text Option Selector";
        textObjectName = "Text Option Text";
		MaxLineHeight = 5;
		//Set desired positioning
		base.Start();
	}

	void Update() {
		if (hasControl && Options != null) {
            bool arrowPosChanged = false;
            if (Input.GetKeyDown(KeyCode.S)) { //Down
                ++arrowPos;
                if (arrowPos == Options.Count) {
                    arrowPos = 0;
                }
                arrowPosChanged = true;
            } else if (Input.GetKeyDown(KeyCode.W)) { //Up
                --arrowPos;
                if (arrowPos < 0) {
                    arrowPos = Options.Count - 1;
                }
                arrowPosChanged = true;
            } else if (Input.GetKeyDown(KeyCode.X)) { //Exit
                StartCoroutine(executeSelection(true));
            }
            if (arrowPosChanged) {       
				if (ScrollingEnabled) {  
					// Increment the list Options if the arrow position exceeds the indices
					// of the currently displayed Options.
					if (arrowPos < MinOptionIndex) {
						MinOptionIndex = arrowPos;
						// Make sure MaxOptionIndex does not exceed the length of the list.
						MaxOptionIndex = (arrowPos + (MaxLineHeight - 1) < Options.Count() ? arrowPos + (MaxLineHeight - 1) : Options.Count());
						updateMenuText();
					}
					else if (arrowPos > MaxOptionIndex) {
						MaxOptionIndex = arrowPos;
						// Make sure MinOptionIndex is not negative.
						MinOptionIndex = (MaxOptionIndex - (MaxLineHeight - 1) < 0 ? 0 : MaxOptionIndex - (MaxLineHeight - 1));
						updateMenuText();
					}
				}
                imageArrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(xPos+15, arrowStartYPos - ((arrowPos - MinOptionIndex) * arrowSpacing), 0);
            }
            if (Input.GetButtonDown("Enter")) {
                StartCoroutine(executeSelection());
            }
        }
	}

	// Call this when creating the menu to initialize the list.
	public void initializeMenuText (List<string> newOptions) {
		// Initialize object if it has not been initailized yet...?
		if (imageBox == null) {
			arrowLocation = "Graphics/Pictures/arrows";
			imageBoxName = "Text Option Skin";
			imageArrowName = "Text Option Selector";
			textObjectName = "Text Option Text";
			ScrollingEnabled = false;
			MaxLineHeight = 5;
			MinOptionIndex = 0;
			MaxOptionIndex = 1;
		}

		Options = newOptions;

		// Determine if scrolling needs to be enabled.
		if (Options.Count() > MaxLineHeight) {
			ScrollingEnabled = true;
		}

		menuOptions.Clear();
		MinOptionIndex = 0;
		MaxOptionIndex = (MaxLineHeight - 1 < Options.Count() ? MaxLineHeight - 1 : Options.Count());

		// Add each menu item.
		// Adds all strings in Options[] to menuOptions. They are given a key
		// equal to their index in the Options list.
		for (int i = MinOptionIndex; i < MaxOptionIndex; i++) {
			menuOptions.Add(i.ToString(), Options[i]);
		}

		base.Start();

		// Find the longest option string in the list.
		string longestOptionText = "";
		for (int i = 0; i < Options.Count; i++) {
			if (Options[i].Count() > longestOptionText.Count()) {
				longestOptionText = Options[i];
			}
		}

		// Determine the width of the longest option when it is displayed on the screen.
		Text textComponent = GetComponentInChildren<Text>();
		int textComponentWidth = (int)(textComponent.cachedTextGeneratorForLayout.GetPreferredWidth(longestOptionText, textComponent.GetGenerationSettings(textComponent.GetComponent<RectTransform> ().rect.size)) + 1);

        // Set desired positioning.
        arrowHeight = arrowWidth = 20; 
        menuSpacing = 60;
        arrowSpacing = 52;
        setWidth = textComponentWidth + 80;
        xPos = 1024 - setWidth - 5;
        yPos = -768;// + height of text box at bot of screen?
		// Need to check if the text area is higher / wider than the available screen space.
        arrowStartYPos = yPos + (menuOptions.Count() * menuSpacing) - arrowSpacing / 2 - 35;
	}

	// Call this from Update().
	protected void updateMenuText() {
		menuOptions.Clear();
		for (int i = MinOptionIndex; i <= MaxOptionIndex; i++) {
			menuOptions.Add(i.ToString(), Options[i]);
		}	
		positionChoiceBoxElements();
	}

	protected override IEnumerator executeSelection(bool exit = false) {
		if (exit) {
			Destroy(this);
			yield break;
		}
		hasControl = false;
		SelectedOption = menuOptions[menuOptions.Keys.ElementAt(arrowPos)];
		Debug.Log("You selected: " + SelectedOption);
		hasControl = true;
		Destroy(this);
		yield break;
	}
}
