﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class EventManager : MonoBehaviour {

	private static Dictionary<string, Dictionary<string, List<EventPage>>> eventList = new Dictionary<string, Dictionary<string, List<EventPage>>>(); 

	public static void AddEvent(string mapName, string idString, List<EventPage> evp) {
		if (!eventList.ContainsKey(mapName)) {
			eventList.Add(mapName, new Dictionary<string, List<EventPage>>());
		}
		eventList[mapName].Add(idString, evp);
	}

	public static void clearList() {
		eventList.Clear();
	}

  public static List<EventPage> GetEventPages(string mapName, string id) {
    if (eventList.Count==0) {
      loadDataFile();
    }
    if (eventList.ContainsKey(mapName)) {
      return eventList[mapName][id];
    }
    return null;
  }

	public static void saveDataFile() {
#if UNITY_EDITOR
        using (Stream stream = File.Open("Assets/Resources/Data/Events.txt", FileMode.Create)) {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            Dictionary<string, Dictionary<string, List<EventPage>>> newEventList = new Dictionary<string, Dictionary<string, List<EventPage>>>(); 
            string tempMapName;
            string tempIdString;
            foreach(KeyValuePair<string, Dictionary<string, List<EventPage>>> entry in eventList) {
              tempMapName = System.Convert.ToBase64String( System.Text.Encoding.UTF8.GetBytes(entry.Key));
			  newEventList.Add(tempMapName, new Dictionary<string, List<EventPage>>());
			  foreach (KeyValuePair<string, List<EventPage>> subentry in entry.Value) {
				tempIdString = System.Convert.ToBase64String( System.Text.Encoding.UTF8.GetBytes(subentry.Key));
				newEventList[tempMapName].Add(tempIdString, subentry.Value);
			  }
            }
            binaryFormatter.Serialize(stream, newEventList);
        }
        UnityEditor.AssetDatabase.Refresh();
    #endif
  }

  public static void loadDataFile() {
    Dictionary<string, Dictionary<string, List<EventPage>>> newEventList = new Dictionary<string, Dictionary<string, List<EventPage>>>(); 
    clearList();
    TextAsset asset = Resources.Load("Data/Events",typeof(TextAsset)) as TextAsset;
    byte[] bytes = asset.bytes;
    Stream s = new MemoryStream(bytes);
    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
    newEventList = (Dictionary<string, Dictionary<string, List<EventPage>>>)binaryFormatter.Deserialize(s);
    string tempMapName;
    string tempIdString;
    foreach(KeyValuePair<string, Dictionary<string, List<EventPage>>> entry in newEventList) {
      tempMapName = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(entry.Key));
	  eventList.Add(tempMapName, new Dictionary<string, List<EventPage>>());
	  foreach (KeyValuePair<string, List<EventPage>> subentry in entry.Value) {
      	tempIdString = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(subentry.Key));
      	eventList[tempMapName].Add(tempIdString, subentry.Value);
	  }
    }
  }
}
