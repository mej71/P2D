using UnityEngine;

[System.Serializable]
public struct LevelUpMoves {
    public int moveLevel;
    public string moveName;
    public LevelUpMoves(int p_level, string p_name) {
        moveLevel = p_level;
        moveName = p_name;
    }
}

[System.Serializable]
public struct EvolutionInfo {
    public string evoTarget;
    public string evoType;
    public string evoRequirement;
    public EvolutionInfo(string p_target, string p_type, string p_req) {
        evoTarget = p_target;
        evoType = p_type;
        evoRequirement = p_req;
    }
}

public enum Gender {
    MALE,
    FEMALE,
    GENDERLESS
    //TRIGGERED
}

//Likelihood of Pokémon to be a specific gender
public enum GenderRate {
    ALWAYSMALE, 
    FEMALEONEEIGHTH, 
    FEMALEONEFOURTH, 
    FEMALEONEHALF, 
    FEMALETHREEFOURTHS, 
    FEMALESEVENEIGHTS, 
    ALWAYSFEMALE, 
    GENDERLESS 
};

//Growth Rate of Pokémon, which determines how much Exp. will be needed to level up
public enum GrowthRate {
    FAST,
    MEDIUM,
    SLOW,
    PARABOLIC,
    ERRATIC,
    FLUCTUATING
};

//Egg groups the Pokémon belong to
public enum EggGroups {
    MONSTER,
    WATER1,
    BUG,
    FLYING,
    FIELD,
    FAIRY,
    GRASS,
    HUMANLIKE,
    WATER3,
    MINERAL,
    AMORPHOUS,
    WATER2,
    DITTO,
    DRAGON,
    UNDISCOVERED
}

//Available colors. 
//Descriptions are the displayed names of the colors (used for colors with spaces, others will use a default format)
//Most enums don't use descriptions, because the information is not displayed to the user
public enum PBColors {
    BLACK,
    BLUE,
    BROWN,
    GRAY,
    GREEN,
    PINK,
    PURPLE,
    RED,
    WHITE,
    YELLOW,
    [System.ComponentModel.Description("Forest Green")]
    FORESTGREEN
}

//Habitats Pokémon can be found in
public enum PokemonHabitats {
    NONE,
    CAVE,
    FOREST,
    GRASSLAND,
    MOUNTAIN,
    RARE,
    [System.ComponentModel.Description("Rough Terrain")]
    ROUGHTERRAIN,
    SEA,
    URBAN,
    [System.ComponentModel.Description("Water's Edge")]
    WATERSEDGE
}

//The types of evolutions Pokémon can have
public enum PokemonEvolutionTypes {
    HAPPINESS,
    HAPPINESSDAY,
    HAPPINESSNIGHT,
    LEVEL,
    TRADE,
    TRADEITEM,
    ITEM,
    ATTACKGREATER,
    ATKDEFEQUAL,
    DEFENSEGREATER,
    SILCOON,
    CASCOON,
    NINJASK,
    SHEDINJA,
    BEAUTY,
    ITEMMALE,
    ITEMFEMALE,
    DAYHOLDITEM,
    NIGHTHOLDITEM,
    HASMOVE,
    HASINPARTY,
    LEVELMALE,
    LEVELFEMALE,
    LOCATION,
    TRADESPECIES,
    LEVELDAY,
    LEVELNIGHT,
    LEVELDARKINPARTY,
    LEVELRAIN,
    HAPPINESSMOVETYPE
}

//Pokemon natures.  Do not change order, the int values are used for stat calculations
public enum PBNatures {
    HARDY ,
    LONELY,
    BRAVE,
    ADAMANT,
    NAUGHTY,
    BOLD,
    DOCILE,
    RELAXED,
    IMPISH,
    LAX,
    TIMID,
    HASTY,
    SERIOUS,
    JOLLY,
    NAIVE,
    MODEST,
    MILD,
    QUIET,
    BASHFUL,
    RASH,
    CALM,
    GENTLE,
    SASSY,
    CAREFUL,
    QUIRKY
}

//Pokemon Stats
public enum PBStats {
    HP,
    ATTACK,
    DEFENSE,
    SPATK,
    SPDEF,
    SPEED
}

//Status Types
public enum PBStatus {
    NONE,
    SLEEP,
    POISON,
    BURN,
    PARALYSIS,
    FREEZE
}

//Ribbon Types
public enum PBRibbons {
    HOENNCOOL,
    HOENNCOOLSUPER,
    HOENNCOOLHYPER,
    HOENNCOOLMASTER,
    HOENNBEAUTY,
    HOENNBEAUTYSUPER,
    HOENNBEAUTYHYPER,
    HOENNBEAUTYMASTER,
    HOENNCUTE,
    HOENNCUTESUPER,
    HOENNCUTEHYPER,
    HOENNCUTEMASTER,
    HOENNSMART,
    HOENNSMARTSUPER,
    HOENNSMARTHYPER,
    HOENNSMARTMASTER,
    HOENNTOUGH,
    HOENNTOUGHSUPER,
    HOENNTOUGHHYPER,
    HOENNTOUGHMASTER,
    SINNOHCOOL,
    SINNOHCOOLSUPER,
    SINNOHCOOLHYPER,
    SINNOHCOOLMASTER,
    SINNOHBEAUTY,
    SINNOHBEAUTYSUPER,
    SINNOHBEAUTYHYPER,
    SINNOHBEAUTYMASTER,
    SINNOHCUTE,
    SINNOHCUTESUPER,
    SINNOHCUTEHYPER,
    SINNOHCUTEMASTER,
    SINNOHSMART,
    SINNOHSMARTSUPER,
    SINNOHSMARTHYPER,
    SINNOHSMARTMASTER,
    SINNOHTOUGH,
    SINNOHTOUGHSUPER,
    SINNOHTOUGHHYPER,
    SINNOHTOUGHMASTER,
    WINNING,
    VICTORY,
    ABILITY,
    GREATABILITY,
    DOUBLEABILITY,
    MULTIABILITY,
    PAIRABILITY,
    WORLDABILITY,
    CHAMPION,
    SINNOHCHAMP,
    RECORD,
    EVENT,
    LEGEND,
    GORGEOUS,
    ROYAL,
    GORGEOUSROYAL,
    ALERT,
    SHOCK,
    DOWNCAST,
    CARELESS,
    RELAX,
    SNOOZE,
    SMILE,
    FOOTPRINT,
    ARTIST,
    EFFORT,
    BIRTHDAY,
    SPECIAL,
    CLASSIC,
    PREMIER,
    SOUVENIR,
    WISHING,
    NATIONAL,
    COUNTRY,
    BATTLECHAMPION,
    REGIONALCHAMPION,
    EARTH,
    WORLD,
    NATIONALCHAMPION,
    WORLDCHAMPION
}

//Obtain Text
public enum PBObtainType {
    NONE,
    MET,
    EGGRECIEVED,
    TRADED,
    FATEFULENCOUNTER
}

