﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataFileChecking : MonoBehaviour {

	public static System.DateTime UpdatePokemonDataDate() {
        return System.IO.File.GetLastWriteTimeUtc("Assets/Resources/Data/Pokemon");
    }

    public static System.DateTime UpdateAbilityDataDate() {
        return System.IO.File.GetLastWriteTimeUtc("Assets/Resources/Data/Abilities");
    }


}
