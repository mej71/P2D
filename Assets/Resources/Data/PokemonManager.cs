﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

[System.Serializable]
public struct PokemonData {
    public string monEnum;
    public string monName;
    public string monType1;
    public string monType2;
    public GenderRate monGenderRate;
    public GrowthRate monGrowthRate;
    public EggGroups monEggType1;
    public EggGroups monEggType2;
    public PBColors monColor;
    public string monKind;
    public PokemonHabitats monHabitat;
    public string monItemCommon;
    public string monItemUncommon;
    public string monItemRare;
    public string monIncense;
    public string monDexEntry;
    public int[] monBaseStats;
    public int[] monEffortValues;
    public int monBaseExp;
    public int monRareness;
    public int monBaseHappiness;
    public int monBattlePlayerYPos;
    public int monBattleEnemyYPos;
    public int monBattlerAltitude;
    public int monStepsToHatch;
    public float monHeight;
    public float monWeight;
    public List<LevelUpMoves> monLevelUpMoves;
    public Dictionary<string, int> monDexNumbers;
    public List<string> monTeachableMoves;
    public List<string> monEggMoves;
    public List<string> monNaturalAbilities;
    public List<string> monHiddenAbilities;
    public List<string> monFormNames;
    public List<EvolutionInfo> monEvolutions;
    public PokemonData(string p_internalName, string p_name, string p_type1, string p_type2, GenderRate p_gender,
            GrowthRate p_growthRare, EggGroups p_eggGroup1, EggGroups p_eggGroup2, PBColors p_color, string p_kind,
            PokemonHabitats p_habitat, string p_itemCommon, string p_itemUncommon, string p_itemRare, string p_incense,
            string p_dexEntry, int[] p_baseStats, int[] p_effortValues, int p_baseExp, int p_rareness, int p_happiness,
            int p_playerYPos, int p_enemyYPos, int p_altitude, int p_eggSteps, float p_height, float p_weight,
            List<LevelUpMoves> p_levelUpMoves, Dictionary<string, int> p_dexNumbers, List<string> p_teachableMoves,
            List<string> p_eggMoves, List<string> p_natAbilities, List<string> p_hidAbilities, List<string> p_formNames,
            List<EvolutionInfo> p_evoInfo) {
        monEnum = p_internalName;
        monName = p_name;
        monType1 = p_type1;
        monType2 = p_type2;
        monGenderRate = p_gender;
        monGrowthRate = p_growthRare;
        monEggType1 = p_eggGroup1;
        monEggType2 = p_eggGroup2;
        monColor = p_color;
        monKind = p_kind;
        monHabitat = p_habitat;
        monItemCommon = p_itemCommon;
        monItemUncommon = p_itemUncommon;
        monItemRare = p_itemRare;
        monIncense = p_incense;
        monDexEntry = p_dexEntry;
        monBaseStats = p_baseStats;
        monEffortValues = p_effortValues;
        monBaseExp = p_baseExp;
        monRareness = p_rareness;
        monBaseHappiness = p_happiness;
        monBattlePlayerYPos = p_playerYPos;
        monBattleEnemyYPos = p_enemyYPos;
        monBattlerAltitude = p_altitude;
        monStepsToHatch = p_eggSteps;
        monHeight = p_height;
        monWeight = p_weight;
        monLevelUpMoves = p_levelUpMoves;
        monDexNumbers = p_dexNumbers;
        monTeachableMoves = p_teachableMoves;
        monEggMoves = p_eggMoves;
        monNaturalAbilities = p_natAbilities;
        monHiddenAbilities = p_hidAbilities;
        monFormNames = p_formNames;
        monEvolutions = p_evoInfo;
    }
}

public class SpeciesManager : MonoBehaviour {

    private static List<PokemonData> speciesList = new List<PokemonData>();

    public static void addSpecies(string p_internalName, string p_name, string p_type1, string p_type2, GenderRate p_gender,
            GrowthRate p_growthRare, EggGroups p_eggGroup1, EggGroups p_eggGroup2, PBColors p_color, string p_kind,
            PokemonHabitats p_habitat, string p_itemCommon, string p_itemUncommon, string p_itemRare, string p_incense,
            string p_dexEntry, int[] p_baseStats, int[] p_effortValues, int p_baseExp, int p_rareness, int p_happiness,
            int p_playerYPos, int p_enemyYPos, int p_altitude, int p_eggSteps, float p_height, float p_weight,
            List<LevelUpMoves> p_levelUpMoves, Dictionary<string, int> p_dexNumbers, List<string> p_teachableMoves,
            List<string> p_eggMoves, List<string> p_natAbilities, List<string> p_hidAbilities, List<string> p_formNames,
            List<EvolutionInfo> p_evoInfo) {
        speciesList.Add(new PokemonData(p_internalName, p_name, p_type1, p_type2, p_gender,
            p_growthRare, p_eggGroup1, p_eggGroup2, p_color, p_kind,
            p_habitat, p_itemCommon, p_itemUncommon, p_itemRare, p_incense,
            p_dexEntry, p_baseStats, p_effortValues, p_baseExp, p_rareness, p_happiness,
            p_playerYPos, p_enemyYPos, p_altitude, p_eggSteps, p_height, p_weight,
            p_levelUpMoves, p_dexNumbers, p_teachableMoves,
            p_eggMoves, p_natAbilities, p_hidAbilities, p_formNames,
            p_evoInfo));
    }

    public static List<PokemonData> getSpeciesList() {
        if (speciesList.Count==0) {
            loadDataFile();
        }
        return speciesList;
    }

    public static void clearList() {
        speciesList.Clear();
    }

    public static void printEachItemName() {
        foreach (PokemonData mon in speciesList) {
            Debug.Log(mon.monEnum);
        }
    }

    public static int getNumSpecies() {
        return speciesList.Count;
    }

    public static void saveDataFile() {
        #if UNITY_EDITOR
            using (Stream stream = File.Open("Assets/Resources/Data/Pokemon.txt", FileMode.Create)) {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                List<PokemonData> newSpeciesList = new List<PokemonData>();
                string tempName, tempType1, tempType2, tempKind, tempItemCommon;
                string tempItemUncommon, tempItemRare, tempIncense, tempDexEntry;
                foreach (PokemonData mon in speciesList) {
                    tempName = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monName));
                    tempType1 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monType1));
                    tempType2 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monType2));
                    tempKind = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monKind));
                    tempItemCommon = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monItemCommon));
                    tempItemUncommon = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monItemUncommon));
                    tempItemRare = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monItemRare));
                    tempIncense = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monIncense));
                    tempDexEntry = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(mon.monDexEntry));
                    newSpeciesList.Add(new PokemonData(mon.monEnum, tempName, tempType1, tempType2, mon.monGenderRate,
                        mon.monGrowthRate, mon.monEggType1, mon.monEggType2, mon.monColor, tempKind,
                        mon.monHabitat, tempItemCommon, tempItemUncommon, tempItemRare, tempIncense,
                        tempDexEntry, mon.monBaseStats, mon.monEffortValues, mon.monBaseExp, mon.monRareness, mon.monBaseHappiness,
                        mon.monBattlePlayerYPos, mon.monBattleEnemyYPos, mon.monBattlerAltitude, mon.monStepsToHatch, mon.monHeight,
                        mon.monWeight, mon.monLevelUpMoves, mon.monDexNumbers, mon.monTeachableMoves,
                        mon.monEggMoves, mon.monNaturalAbilities, mon.monHiddenAbilities, mon.monFormNames, mon.monEvolutions));
                }
                binaryFormatter.Serialize(stream, newSpeciesList);
            }
            UnityEditor.AssetDatabase.Refresh();
        #endif
    }

    public static void loadDataFile() {
        List<PokemonData> tempSpeciesList;
        speciesList.Clear();
        TextAsset asset = Resources.Load("Data/Pokemon", typeof(TextAsset)) as TextAsset;
        byte[] bytes = asset.bytes;
        Stream s = new MemoryStream(bytes);
        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        tempSpeciesList = (List<PokemonData>)binaryFormatter.Deserialize(s);
        speciesList.Clear();
        string tempName, tempType1, tempType2, tempKind, tempItemCommon;
        string tempItemUncommon, tempItemRare, tempIncense, tempDexEntry;
        foreach (PokemonData mon in tempSpeciesList) {
            tempName = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monName));
            tempType1 = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monType1));
            tempType2 = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monType2));
            tempKind = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monKind));
            tempItemCommon = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monItemCommon));
            tempItemUncommon = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monItemUncommon));
            tempItemRare = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monItemRare));
            tempIncense = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monIncense));
            tempDexEntry = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(mon.monDexEntry));
            speciesList.Add(new PokemonData(mon.monEnum, tempName, tempType1, tempType2, mon.monGenderRate,
                    mon.monGrowthRate, mon.monEggType1, mon.monEggType2, mon.monColor, tempKind,
                    mon.monHabitat, tempItemCommon, tempItemUncommon, tempItemRare, tempIncense,
                    tempDexEntry, mon.monBaseStats, mon.monEffortValues, mon.monBaseExp, mon.monRareness, mon.monBaseHappiness,
                    mon.monBattlePlayerYPos, mon.monBattleEnemyYPos, mon.monBattlerAltitude, mon.monStepsToHatch, mon.monHeight,
                    mon.monWeight, mon.monLevelUpMoves, mon.monDexNumbers, mon.monTeachableMoves,
                    mon.monEggMoves, mon.monNaturalAbilities, mon.monHiddenAbilities, mon.monFormNames, mon.monEvolutions));
        }
    }
}
