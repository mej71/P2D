using UnityEngine;

public enum PBMoves {
	NONE	,
	MEGAHORN	,
	ATTACKORDER	,
	BUGBUZZ	,
	XSCISSOR	,
	SIGNALBEAM	,
	UTURN	,
	STEAMROLLER	,
	BUGBITE	,
	SILVERWIND	,
	STRUGGLEBUG	,
	TWINEEDLE	,
	FURYCUTTER	,
	LEECHLIFE	,
	PINMISSILE	,
	DEFENDORDER	,
	HEALORDER	,
	QUIVERDANCE	,
	RAGEPOWDER	,
	SPIDERWEB	,
	STRINGSHOT	,
	TAILGLOW	,
	FOULPLAY	,
	NIGHTDAZE	,
	CRUNCH	,
	DARKPULSE	,
	SUCKERPUNCH	,
	NIGHTSLASH	,
	BITE	,
	FEINTATTACK	,
	SNARL	,
	ASSURANCE	,
	PAYBACK	,
	PURSUIT	,
	THIEF	,
	KNOCKOFF	,
	BEATUP	,
	FLING	,
	PUNISHMENT	,
	DARKVOID	,
	EMBARGO	,
	FAKETEARS	,
	FLATTER	,
	HONECLAWS	,
	MEMENTO	,
	NASTYPLOT	,
	QUASH	,
	SNATCH	,
	SWITCHEROO	,
	TAUNT	,
	TORMENT	,
	ROAROFTIME	,
	DRACOMETEOR	,
	OUTRAGE	,
	DRAGONRUSH	,
	SPACIALREND	,
	DRAGONPULSE	,
	DRAGONCLAW	,
	DRAGONTAIL	,
	DRAGONBREATH	,
	DUALCHOP	,
	TWISTER	,
	DRAGONRAGE	,
	DRAGONDANCE	,
	BOLTSTRIKE	,
	THUNDER	,
	VOLTTACKLE	,
	ZAPCANNON	,
	FUSIONBOLT	,
	THUNDERBOLT	,
	WILDCHARGE	,
	DISCHARGE	,
	THUNDERPUNCH	,
	VOLTSWITCH	,
	SPARK	,
	THUNDERFANG	,
	SHOCKWAVE	,
	ELECTROWEB	,
	CHARGEBEAM	,
	THUNDERSHOCK	,
	ELECTROBALL	,
	CHARGE	,
	MAGNETRISE	,
	THUNDERWAVE	,
	FOCUSPUNCH	,
	HIGHJUMPKICK	,
	CLOSECOMBAT	,
	FOCUSBLAST	,
	SUPERPOWER	,
	CROSSCHOP	,
	DYNAMICPUNCH	,
	HAMMERARM	,
	JUMPKICK	,
	AURASPHERE	,
	SACREDSWORD	,
	SECRETSWORD	,
	SKYUPPERCUT	,
	SUBMISSION	,
	BRICKBREAK	,
	DRAINPUNCH	,
	VITALTHROW	,
	CIRCLETHROW	,
	FORCEPALM	,
	LOWSWEEP	,
	REVENGE	,
	ROLLINGKICK	,
	WAKEUPSLAP	,
	KARATECHOP	,
	MACHPUNCH	,
	ROCKSMASH	,
	STORMTHROW	,
	VACUUMWAVE	,
	DOUBLEKICK	,
	ARMTHRUST	,
	TRIPLEKICK	,
	COUNTER	,
	FINALGAMBIT	,
	LOWKICK	,
	REVERSAL	,
	SEISMICTOSS	,
	BULKUP	,
	DETECT	,
	QUICKGUARD	,
	VCREATE	,
	BLASTBURN	,
	ERUPTION	,
	OVERHEAT	,
	BLUEFLARE	,
	FIREBLAST	,
	FLAREBLITZ	,
	MAGMASTORM	,
	FUSIONFLARE	,
	HEATWAVE	,
	INFERNO	,
	SACREDFIRE	,
	SEARINGSHOT	,
	FLAMETHROWER	,
	BLAZEKICK	,
	FIERYDANCE	,
	LAVAPLUME	,
	FIREPUNCH	,
	FLAMEBURST	,
	FIREFANG	,
	FLAMEWHEEL	,
	FIREPLEDGE	,
	FLAMECHARGE	,
	EMBER	,
	FIRESPIN	,
	INCINERATE	,
	HEATCRASH	,
	SUNNYDAY	,
	WILLOWISP	,
	SKYATTACK	,
	BRAVEBIRD	,
	HURRICANE	,
	AEROBLAST	,
	FLY	,
	BOUNCE	,
	DRILLPECK	,
	AIRSLASH	,
	AERIALACE	,
	CHATTER	,
	PLUCK	,
	SKYDROP	,
	WINGATTACK	,
	ACROBATICS	,
	AIRCUTTER	,
	GUST	,
	PECK	,
	DEFOG	,
	FEATHERDANCE	,
	MIRRORMOVE	,
	ROOST	,
	TAILWIND	,
	SHADOWFORCE	,
	SHADOWBALL	,
	SHADOWCLAW	,
	OMINOUSWIND	,
	SHADOWPUNCH	,
	HEX	,
	SHADOWSNEAK	,
	ASTONISH	,
	LICK	,
	NIGHTSHADE	,
	CONFUSERAY	,
	CURSE	,
	DESTINYBOND	,
	GRUDGE	,
	NIGHTMARE	,
	SPITE	,
	FRENZYPLANT	,
	LEAFSTORM	,
	PETALDANCE	,
	POWERWHIP	,
	SEEDFLARE	,
	SOLARBEAM	,
	WOODHAMMER	,
	LEAFBLADE	,
	ENERGYBALL	,
	SEEDBOMB	,
	GIGADRAIN	,
	HORNLEECH	,
	LEAFTORNADO	,
	MAGICALLEAF	,
	NEEDLEARM	,
	RAZORLEAF	,
	GRASSPLEDGE	,
	MEGADRAIN	,
	VINEWHIP	,
	BULLETSEED	,
	ABSORB	,
	GRASSKNOT	,
	AROMATHERAPY	,
	COTTONGUARD	,
	COTTONSPORE	,
	GRASSWHISTLE	,
	INGRAIN	,
	LEECHSEED	,
	SLEEPPOWDER	,
	SPORE	,
	STUNSPORE	,
	SYNTHESIS	,
	WORRYSEED	,
	EARTHQUAKE	,
	EARTHPOWER	,
	DIG	,
	DRILLRUN	,
	BONECLUB	,
	MUDBOMB	,
	BULLDOZE	,
	MUDSHOT	,
	BONEMERANG	,
	SANDTOMB	,
	BONERUSH	,
	MUDSLAP	,
	FISSURE	,
	MAGNITUDE	,
	MUDSPORT	,
	SANDATTACK	,
	SPIKES	,
	FREEZESHOCK	,
	ICEBURN	,
	BLIZZARD	,
	ICEBEAM	,
	ICICLECRASH	,
	ICEPUNCH	,
	AURORABEAM	,
	GLACIATE	,
	ICEFANG	,
	AVALANCHE	,
	ICYWIND	,
	FROSTBREATH	,
	ICESHARD	,
	POWDERSNOW	,
	ICEBALL	,
	ICICLESPEAR	,
	SHEERCOLD	,
	HAIL	,
	HAZE	,
	MIST	,
	EXPLOSION	,
	SELFDESTRUCT	,
	GIGAIMPACT	,
	HYPERBEAM	,
	LASTRESORT	,
	DOUBLEEDGE	,
	HEADCHARGE	,
	MEGAKICK	,
	THRASH	,
	EGGBOMB	,
	JUDGMENT	,
	SKULLBASH	,
	HYPERVOICE	,
	ROCKCLIMB	,
	TAKEDOWN	,
	UPROAR	,
	BODYSLAM	,
	TECHNOBLAST	,
	EXTREMESPEED	,
	HYPERFANG	,
	MEGAPUNCH	,
	RAZORWIND	,
	SLAM	,
	STRENGTH	,
	TRIATTACK	,
	CRUSHCLAW	,
	RELICSONG	,
	CHIPAWAY	,
	DIZZYPUNCH	,
	FACADE	,
	HEADBUTT	,
	RETALIATE	,
	SECRETPOWER	,
	SLASH	,
	HORNATTACK	,
	STOMP	,
	COVET	,
	ROUND	,
	SMELLINGSALTS	,
	SWIFT	,
	VICEGRIP	,
	CUT	,
	STRUGGLE	,
	TACKLE	,
	WEATHERBALL	,
	ECHOEDVOICE	,
	FAKEOUT	,
	FALSESWIPE	,
	PAYDAY	,
	POUND	,
	QUICKATTACK	,
	SCRATCH	,
	SNORE	,
	DOUBLEHIT	,
	FEINT	,
	TAILSLAP	,
	RAGE	,
	RAPIDSPIN	,
	SPIKECANNON	,
	COMETPUNCH	,
	FURYSWIPES	,
	BARRAGE	,
	BIND	,
	DOUBLESLAP	,
	FURYATTACK	,
	WRAP	,
	CONSTRICT	,
	BIDE	,
	CRUSHGRIP	,
	ENDEAVOR	,
	FLAIL	,
	FRUSTRATION	,
	GUILLOTINE	,
	HIDDENPOWER	,
	HORNDRILL	,
	NATURALGIFT	,
	PRESENT	,
	RETURN	,
	SONICBOOM	,
	SPITUP	,
	SUPERFANG	,
	TRUMPCARD	,
	WRINGOUT	,
	ACUPRESSURE	,
	AFTERYOU	,
	ASSIST	,
	ATTRACT	,
	BATONPASS	,
	BELLYDRUM	,
	BESTOW	,
	BLOCK	,
	CAMOUFLAGE	,
	CAPTIVATE	,
	CHARM	,
	CONVERSION	,
	CONVERSION2	,
	COPYCAT	,
	DEFENSECURL	,
	DISABLE	,
	DOUBLETEAM	,
	ENCORE	,
	ENDURE	,
	ENTRAINMENT	,
	FLASH	,
	FOCUSENERGY	,
	FOLLOWME	,
	FORESIGHT	,
	GLARE	,
	GROWL	,
	GROWTH	,
	HARDEN	,
	HEALBELL	,
	HELPINGHAND	,
	HOWL	,
	LEER	,
	LOCKON	,
	LOVELYKISS	,
	LUCKYCHANT	,
	MEFIRST	,
	MEANLOOK	,
	METRONOME	,
	MILKDRINK	,
	MIMIC	,
	MINDREADER	,
	MINIMIZE	,
	MOONLIGHT	,
	MORNINGSUN	,
	NATUREPOWER	,
	ODORSLEUTH	,
	PAINSPLIT	,
	PERISHSONG	,
	PROTECT	,
	PSYCHUP	,
	RECOVER	,
	RECYCLE	,
	REFLECTTYPE	,
	REFRESH	,
	ROAR	,
	SAFEGUARD	,
	SCARYFACE	,
	SCREECH	,
	SHARPEN	,
	SHELLSMASH	,
	SIMPLEBEAM	,
	SING	,
	SKETCH	,
	SLACKOFF	,
	SLEEPTALK	,
	SMOKESCREEN	,
	SOFTBOILED	,
	SPLASH	,
	STOCKPILE	,
	SUBSTITUTE	,
	SUPERSONIC	,
	SWAGGER	,
	SWALLOW	,
	SWEETKISS	,
	SWEETSCENT	,
	SWORDSDANCE	,
	TAILWHIP	,
	TEETERDANCE	,
	TICKLE	,
	TRANSFORM	,
	WHIRLWIND	,
	WISH	,
	WORKUP	,
	YAWN	,
	GUNKSHOT	,
	SLUDGEWAVE	,
	SLUDGEBOMB	,
	POISONJAB	,
	CROSSPOISON	,
	SLUDGE	,
	VENOSHOCK	,
	CLEARSMOG	,
	POISONFANG	,
	POISONTAIL	,
	ACID	,
	ACIDSPRAY	,
	SMOG	,
	POISONSTING	,
	ACIDARMOR	,
	COIL	,
	GASTROACID	,
	POISONGAS	,
	POISONPOWDER	,
	TOXIC	,
	TOXICSPIKES	,
	PSYCHOBOOST	,
	DREAMEATER	,
	FUTURESIGHT	,
	PSYSTRIKE	,
	PSYCHIC	,
	EXTRASENSORY	,
	PSYSHOCK	,
	ZENHEADBUTT	,
	LUSTERPURGE	,
	MISTBALL	,
	PSYCHOCUT	,
	SYNCHRONOISE	,
	PSYBEAM	,
	HEARTSTAMP	,
	CONFUSION	,
	MIRRORCOAT	,
	PSYWAVE	,
	STOREDPOWER	,
	AGILITY	,
	ALLYSWITCH	,
	AMNESIA	,
	BARRIER	,
	CALMMIND	,
	COSMICPOWER	,
	GRAVITY	,
	GUARDSPLIT	,
	GUARDSWAP	,
	HEALBLOCK	,
	HEALPULSE	,
	HEALINGWISH	,
	HEARTSWAP	,
	HYPNOSIS	,
	IMPRISON	,
	KINESIS	,
	LIGHTSCREEN	,
	LUNARDANCE	,
	MAGICCOAT	,
	MAGICROOM	,
	MEDITATE	,
	MIRACLEEYE	,
	POWERSPLIT	,
	POWERSWAP	,
	POWERTRICK	,
	PSYCHOSHIFT	,
	REFLECT	,
	REST	,
	ROLEPLAY	,
	SKILLSWAP	,
	TELEKINESIS	,
	TELEPORT	,
	TRICK	,
	TRICKROOM	,
	WONDERROOM	,
	HEADSMASH	,
	ROCKWRECKER	,
	STONEEDGE	,
	ROCKSLIDE	,
	POWERGEM	,
	ANCIENTPOWER	,
	ROCKTHROW	,
	ROCKTOMB	,
	SMACKDOWN	,
	ROLLOUT	,
	ROCKBLAST	,
	ROCKPOLISH	,
	SANDSTORM	,
	STEALTHROCK	,
	WIDEGUARD	,
	DOOMDESIRE	,
	IRONTAIL	,
	METEORMASH	,
	FLASHCANNON	,
	IRONHEAD	,
	STEELWING	,
	MIRRORSHOT	,
	MAGNETBOMB	,
	GEARGRIND	,
	METALCLAW	,
	BULLETPUNCH	,
	GYROBALL	,
	HEAVYSLAM	,
	METALBURST	,
	AUTOTOMIZE	,
	IRONDEFENSE	,
	METALSOUND	,
	SHIFTGEAR	,
	HYDROCANNON	,
	WATERSPOUT	,
	HYDROPUMP	,
	MUDDYWATER	,
	SURF	,
	AQUATAIL	,
	CRABHAMMER	,
	DIVE	,
	SCALD	,
	WATERFALL	,
	RAZORSHELL	,
	BRINE	,
	BUBBLEBEAM	,
	OCTAZOOKA	,
	WATERPULSE	,
	WATERPLEDGE	,
	AQUAJET	,
	WATERGUN	,
	CLAMP	,
	WHIRLPOOL	,
	BUBBLE	,
	AQUARING	,
	RAINDANCE	,
	SOAK	,
	WATERSPORT	,
	WITHDRAW	
}
