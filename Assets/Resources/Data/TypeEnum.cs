using UnityEngine;

public enum PBTypes {
	NORMAL	,
	FIGHTING	,
	FLYING	,
	POISON	,
	GROUND	,
	ROCK	,
	BUG	,
	GHOST	,
	STEEL	,
	NULL	,
	FIRE	,
	WATER	,
	GRASS	,
	ELECTRIC	,
	PSYCHIC	,
	ICE	,
	DRAGON	,
	DARK	,
	FAIRY	
}
