using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

[System.Serializable]
public struct Moves {
    public string internalName;
    public string name;
    public string moveEffectType;
    public int baseDamage;
    public string moveType;
    public MoveDamageCategory moveCategory;
    public int moveAccuracy;
    public int basePP;
    public int additionalEffectChance;
    public MoveTargetType moveTargetType;
    public int priority;
    public bool makesContact;
    public bool blockedByProtect;
    public bool blockedByMagicBounce;
    public bool canBeSnatched;
    public bool canBeCopied;
    public bool affectedByKingsRock;
    public bool thawsUser;
    public bool highCritChance;
    public bool bitingMove;
    public bool punchingMove;
    public bool soundBasedMove;
    public bool powderBasedMove;
    public bool pulseBasedMove;
    public bool bombBasedMove;
    public string moveDesc;
    public string moveEffectTargetAbility;
    public string moveEffectTargetType;
    public List<string> moveAdditionalEffects;
    public List<string> moveDamageModifiers;
    public List<string> moveHitModifiers;
    public List<string> moveAccuracyModifiers;
    public List<string> moveSuccessModifiers;
    public List<string> moveTargetModifiers;
    public List<string> movePriorityModifiers;
    public List<string> moveTypeModifiers;
    public int[] moveStatChanges;
    public Moves(string p_internalName, string p_name, string p_moveEffectType, int p_baseDamage, string p_moveType, MoveDamageCategory p_moveCategory,
                int p_moveAccuracy, int p_basePP, int p_additionalEffectChance, MoveTargetType p_moveTargetType, int p_priority, bool p_makesContact,
                bool p_blockedByProtect, bool p_blockedByMagicBounce, bool p_canBeSnatched, bool p_canBeCopied, bool p_affectedByKingsRock,
                bool p_thawsUser, bool p_highCritChance, bool p_bitingMove, bool p_punchingMove, bool p_soundBasedMove, bool p_powderBasedMove,
                bool p_pulseBasedMove, bool p_bombBasedMove, string p_moveDesc, string p_moveEffectTargetAbility, string p_moveEffectTargetType,
                List<string> p_moveAdditionalEffects, List<string> p_moveDamageModifiers, List<string> p_moveHitModifiers, List<string> p_moveAccuracyModifiers,
                List<string> p_moveSuccessModifiers, List<string> p_moveTargetModifiers, List<string> p_movePriorityModifiers, List<string> p_moveTypeModifiers,
                int[] p_moveStatChanges) 
        {
        internalName = p_internalName; name = p_name;  moveEffectType = p_moveEffectType; baseDamage = p_baseDamage; moveType = p_moveType; moveCategory = p_moveCategory;
        moveAccuracy = p_moveAccuracy; basePP = p_basePP; additionalEffectChance = p_additionalEffectChance; moveTargetType = p_moveTargetType; priority = p_priority;
        makesContact = p_makesContact; blockedByProtect = p_blockedByProtect; blockedByMagicBounce = p_blockedByMagicBounce; canBeSnatched = p_canBeSnatched;
        canBeCopied = p_canBeCopied; affectedByKingsRock = p_affectedByKingsRock; thawsUser = p_thawsUser; highCritChance = p_highCritChance; bitingMove = p_bitingMove;
        punchingMove = p_punchingMove; soundBasedMove = p_soundBasedMove; powderBasedMove = p_powderBasedMove;  pulseBasedMove = p_pulseBasedMove; bombBasedMove = p_bombBasedMove;
        moveDesc = p_moveDesc; moveEffectTargetAbility = p_moveEffectTargetAbility; moveEffectTargetType = p_moveEffectTargetType; moveAdditionalEffects = p_moveAdditionalEffects;
        moveDamageModifiers = p_moveDamageModifiers; moveHitModifiers = p_moveHitModifiers; moveAccuracyModifiers = p_moveAccuracyModifiers;
        moveSuccessModifiers = p_moveSuccessModifiers; moveTargetModifiers = p_moveTargetModifiers; movePriorityModifiers = p_movePriorityModifiers;
        moveTypeModifiers = p_moveTypeModifiers; moveStatChanges = p_moveStatChanges;
  }
}

public class MoveManager : MonoBehaviour {
  public static List<Moves> moveList = new List<Moves>();
  public static void addMove(string p_internalName, string p_name, string p_moveEffectType, int p_baseDamage, string p_moveType, MoveDamageCategory p_moveCategory,
              int p_moveAccuracy, int p_basePP, int p_additionalEffectChance, MoveTargetType p_moveTargetType, int p_priority, bool p_makesContact,
              bool p_blockedByProtect, bool p_blockedByMagicBounce, bool p_canBeSnatched, bool p_canBeCopied, bool p_affectedByKingsRock,
              bool p_thawsUser, bool p_highCritChance, bool p_bitingMove, bool p_punchingMove, bool p_soundBasedMove, bool p_powderBasedMove,
              bool p_pulseBasedMove, bool p_bombBasedMove, string p_moveDesc, string p_moveEffectTargetAbility, string p_moveEffectTargetType,
                List<string> p_moveAdditionalEffects, List<string> p_moveDamageModifiers, List<string> p_moveHitModifiers, List<string> p_moveAccuracyModifiers,
                List<string> p_moveSuccessModifiers, List<string> p_moveTargetModifiers, List<string> p_movePriorityModifiers, List<string> p_moveTypeModifiers,
                int[] p_moveStatChanges)  {
    moveList.Add(new Moves(p_internalName, p_name, p_moveEffectType, p_baseDamage, p_moveType, p_moveCategory,
                p_moveAccuracy, p_basePP, p_additionalEffectChance, p_moveTargetType, p_priority, p_makesContact,
                p_blockedByProtect, p_blockedByMagicBounce, p_canBeSnatched, p_canBeCopied, p_affectedByKingsRock,
                p_thawsUser, p_highCritChance, p_bitingMove, p_punchingMove, p_soundBasedMove, p_powderBasedMove,
                p_pulseBasedMove, p_bombBasedMove, p_moveDesc, p_moveEffectTargetAbility, p_moveEffectTargetType,
                p_moveAdditionalEffects, p_moveDamageModifiers, p_moveHitModifiers, p_moveAccuracyModifiers,
                p_moveSuccessModifiers, p_moveTargetModifiers, p_movePriorityModifiers, p_moveTypeModifiers,
                p_moveStatChanges)) ;
  }

  public static void clearList() {
    moveList.Clear();
  }

  public static void printEachMoveName() {
    foreach(Moves move in moveList) {
      Debug.Log(move.name);
    }
  }

  public static List<Moves> getMoves() {
        if (moveList.Count==0) {
            loadDataFile();
        }
        return moveList;
    }

  public static int getNumMoves() {
    return moveList.Count;
  }

  public static void saveDataFile() {
    #if UNITY_EDITOR
        using (Stream stream = File.Open("Assets/Resources/Data/Moves.txt", FileMode.Create)) {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            List<Moves> newMoveList = new List<Moves>();
            string tempName;
            string tempMoveDesc;
            foreach(Moves move in moveList) {
                tempName = System.Convert.ToBase64String( System.Text.Encoding.UTF8.GetBytes(move.name));
                tempMoveDesc = System.Convert.ToBase64String( System.Text.Encoding.UTF8.GetBytes(move.moveDesc));
                newMoveList.Add(new Moves(move.internalName, tempName, move.moveEffectType, move.baseDamage, move.moveType,
                            move.moveCategory, move.moveAccuracy, move.basePP, move.additionalEffectChance, move.moveTargetType,
                            move.priority, move.makesContact, move.blockedByProtect, move.blockedByMagicBounce, move.canBeSnatched,
                            move.canBeCopied, move.affectedByKingsRock, move.thawsUser, move.highCritChance, move.bitingMove,
                            move.punchingMove, move.soundBasedMove, move.powderBasedMove, move.pulseBasedMove, move.bombBasedMove,
                            tempMoveDesc, move.moveEffectTargetAbility, move.moveEffectTargetType,
                            move.moveAdditionalEffects, move.moveDamageModifiers, move.moveHitModifiers, move.moveAccuracyModifiers,
                            move.moveSuccessModifiers, move.moveTargetModifiers, move.movePriorityModifiers, move.moveTypeModifiers,
                            move.moveStatChanges));
            }
            binaryFormatter.Serialize(stream, newMoveList);
        }
        UnityEditor.AssetDatabase.Refresh();
    #endif
  }

    public static void loadDataFile() {
        List<Moves> tempMoveList;
        TextAsset asset = Resources.Load("Data/Moves", typeof(TextAsset)) as TextAsset;
        Stream s = new MemoryStream(asset.bytes);
        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        tempMoveList = (List<Moves>)binaryFormatter.Deserialize(s);
        moveList.Clear();
        string tempName;
        string tempMoveDesc;
        foreach(Moves move in tempMoveList) {
            tempName = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(move.name));
            tempMoveDesc = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(move.moveDesc));
            moveList.Add(new Moves(move.internalName, tempName, move.moveEffectType, move.baseDamage, move.moveType,
                        move.moveCategory, move.moveAccuracy, move.basePP, move.additionalEffectChance, move.moveTargetType,
                        move.priority, move.makesContact, move.blockedByProtect, move.blockedByMagicBounce, move.canBeSnatched,
                        move.canBeCopied, move.affectedByKingsRock, move.thawsUser, move.highCritChance, move.bitingMove,
                        move.punchingMove, move.soundBasedMove, move.powderBasedMove, move.pulseBasedMove, move.bombBasedMove,
                        tempMoveDesc, move.moveEffectTargetAbility, move.moveEffectTargetType,
                        move.moveAdditionalEffects, move.moveDamageModifiers, move.moveHitModifiers, move.moveAccuracyModifiers,
                        move.moveSuccessModifiers, move.moveTargetModifiers, move.movePriorityModifiers, move.moveTypeModifiers,
                        move.moveStatChanges));
        }
    }
}
